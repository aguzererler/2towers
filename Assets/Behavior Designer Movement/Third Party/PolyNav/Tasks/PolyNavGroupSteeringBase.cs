using UnityEngine;
using System.Collections.Generic;

namespace BehaviorDesigner.Runtime.Tasks.Movement.PolyNav
{
    public abstract class PolyNavGroupSteeringBase : Action
    {
        [Tooltip("All of the agents")]
        public PolyNavAgent[] agents;

        protected Transform[] agentTransforms;

        public override void OnAwake()
        {
            agentTransforms = new Transform[agents.Length];
            for (int i = 0; i < agents.Length; ++i) {
                agents[i].enabled = true;
                agentTransforms[i] = agents[i].transform;
            }
        }

        public override void OnEnd()
        {
            for (int i = 0; i < agents.Length; ++i) {
                if (agents[i] != null) {
                    agents[i].enabled = false;
                }
            }
        }
    }
}