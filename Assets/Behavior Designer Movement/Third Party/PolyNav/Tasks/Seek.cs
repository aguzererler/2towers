using UnityEngine;

namespace BehaviorDesigner.Runtime.Tasks.Movement.PolyNav
{
    [TaskDescription("Seek the target specified using PolyNav.")]
    [TaskCategory("Movement/PolyNav")]
    [HelpURL("http://www.opsive.com/assets/BehaviorDesigner/Movement/documentation.php?id=3")]
    [TaskIcon("Assets/Behavior Designer Movement/Editor/Icons/{SkinColor}SeekIcon.png")]
    public class Seek : PolyNavSteeringBase
    {
        [Tooltip("The transform that the agent is moving towards")]
        public SharedTransform targetTransform;
        [Tooltip("If target is null then use the target position")]
        public SharedVector3 targetPosition;

        private Vector2 prevPosition;

        // Seek the destination. Return success once the agent has reached the destination.
        // Return running if the agent hasn't reached the destination yet
        public override TaskStatus OnUpdate()
        {
            if (DestinationReached) {
                return TaskStatus.Success;
            }

            if (prevPosition != Target()) {
                prevPosition = Target();
                UpdateDestination();
            }
            return TaskStatus.Running;
        }

        // Return targetPosition if targetTransform is null
        protected override Vector2 Target()
        {
            if (targetTransform.Value != null) {
                return targetTransform.Value.position;
            }
            return targetPosition.Value;
        }

        // Reset the public variables
        public override void OnReset()
        {
            targetTransform = null;
            targetPosition = Vector3.zero;
        }
    }
}