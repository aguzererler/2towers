using UnityEngine;
using System.Collections.Generic;

namespace BehaviorDesigner.Runtime.Tasks.Movement.PolyNav
{
    public abstract class PolyNavSteeringBase : Action
    {
        [Tooltip("The speed of the agent")]
        public SharedFloat speed = 5;
        [Tooltip("Angular speed of the agent")]
        public SharedFloat angularSpeed = 350;

        private bool destinationReached = false;
        protected bool DestinationReached { get { return destinationReached;} }

        // A cache of the PolyNavAgent
        protected PolyNavAgent polyNavAgent;
        
        public override void OnAwake()
        {
            polyNavAgent = gameObject.GetComponent<PolyNavAgent>();

        }

        public override void OnStart()
        {
            destinationReached = false;

            polyNavAgent.maxSpeed = speed.Value;
            polyNavAgent.rotateSpeed = angularSpeed.Value;
            polyNavAgent.enabled = true;
            polyNavAgent.SetDestination(Target(), OnDestinationReached);
        }

        // The destination has changed
        protected void UpdateDestination()
        {
            polyNavAgent.SetDestination(Target(), OnDestinationReached);
            //destinationReached = false;
        }

        private void OnDestinationReached(bool arrived)
        {
            destinationReached = arrived;
			//Debug.Log("destination arrived");
        }
        
        // Virtual method to determine the destination
        protected virtual Vector2 Target()
        {
            return Vector2.zero;
        }

        public override void OnEnd()
        {
            // Disable the poly nav
			//Debug.Log ("steeringbase end, disable navpoly");
            polyNavAgent.enabled = false;
        }

        // Reset the public variables
        public override void OnReset()
        {
            speed = 5;
            angularSpeed = 10;
        }

		public void changeSpeed (float nSpeed){
			polyNavAgent.maxSpeed = nSpeed;
		}
    }
}