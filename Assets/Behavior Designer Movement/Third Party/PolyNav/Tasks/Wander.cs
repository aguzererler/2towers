using UnityEngine;

namespace BehaviorDesigner.Runtime.Tasks.Movement.PolyNav
{
    [TaskDescription("Wander using PolyNav.")]
    [TaskCategory("Movement/PolyNav")]
    [HelpURL("http://www.opsive.com/assets/BehaviorDesigner/Movement/documentation.php?id=9")]
    [TaskIcon("Assets/Behavior Designer Movement/Editor/Icons/{SkinColor}WanderIcon.png")]
    public class Wander : PolyNavSteeringBase
    {
        [Tooltip("The agent has arrived when the square magnitude is less than this value")]
        public SharedFloat arriveDistance = 0.1f;
        [Tooltip("How far ahead of the current position to look ahead for a wander")]
        public SharedFloat wanderDistance = 10;
        [Tooltip("The amount that the agent rotates direction")]
        public SharedFloat wanderRate = 2;

        private Vector2 prevTarget;

        // There is no success or fail state with wander - the agent will just keep wandering
        public override TaskStatus OnUpdate()
        {
            Vector2 thisPosition = transform.position;
            if (DestinationReached || Vector3.SqrMagnitude(thisPosition - prevTarget) < arriveDistance.Value) {
                UpdateDestination();
            }

            return TaskStatus.Running;
        }

        // Return targetPosition if targetTransform is null
        protected override Vector2 Target()
        {
            // point in a new random direction and then multiply that by the wander distance
            var direction = transform.up + Random.insideUnitSphere * wanderRate.Value;
            prevTarget = transform.position + direction.normalized * wanderDistance.Value;
            return prevTarget;
        }

        // Reset the public variables
        public override void OnReset()
        {
            arriveDistance = 0.1f;
            wanderDistance = 20;
            wanderRate = 2;
        }
    }
}