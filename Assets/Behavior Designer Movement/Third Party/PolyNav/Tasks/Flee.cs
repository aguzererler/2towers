using UnityEngine;

namespace BehaviorDesigner.Runtime.Tasks.Movement.PolyNav
{
    [TaskDescription("Flee from the target specified using NavMesh 2D.")]
    [TaskCategory("Movement/PolyNav")]
    [HelpURL("http://www.opsive.com/assets/BehaviorDesigner/Movement/documentation.php?id=4")]
    [TaskIcon("Assets/Behavior Designer Movement/Editor/Icons/{SkinColor}FleeIcon.png")]
    public class Flee : PolyNavSteeringBase
    {
        [Tooltip("The agent has fleed when the square magnitude is greater than this value")]
        public SharedFloat fleedDistance = 20;
        [Tooltip("The distance to look ahead when fleeing")]
        public SharedFloat lookAheadDistance = 5;
        [Tooltip("The transform that the agent is moving towards")]
        public SharedTransform targetTransform;
        [Tooltip("If target is null then use the target position")]
        public SharedVector3 targetPosition;

        // True if the target is a transform
        private bool dynamicTarget;
        private Vector2 prevTargetPosition;

        public override void OnStart()
        {
            // the target is dynamic if the target transform is not null and has a valid
            dynamicTarget = (targetTransform != null && targetTransform.Value != null);

            base.OnStart();
        }

        // Seek the destination. Return success once the agent has reached the destination.
        // Return running if the agent hasn't reached the destination yet
        public override TaskStatus OnUpdate()
        {
            if (Vector3.SqrMagnitude((Vector2)transform.position - TargetPosition()) > fleedDistance.Value) {
                return TaskStatus.Success;
            }

            // Update the destination if the target is a transform because that agent could move
            if (dynamicTarget) {
                UpdateDestination();
            }
            
            return TaskStatus.Running;
        }

        private Vector2 TargetPosition()
        {
            if (dynamicTarget) {
                return targetTransform.Value.position;
            }
            return targetPosition.Value;
        }

        // Return targetPosition if targetTransform is null
        protected override Vector2 Target()
        {
            return transform.position + (transform.position - (Vector3)TargetPosition()).normalized * lookAheadDistance.Value;
        }
    }
}