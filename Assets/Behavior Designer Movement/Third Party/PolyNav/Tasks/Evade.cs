using UnityEngine;

namespace BehaviorDesigner.Runtime.Tasks.Movement.PolyNav
{
    [TaskDescription("Evade the target specified using the PolyNav.")]
    [TaskCategory("Movement/PolyNav")]
    [HelpURL("http://www.opsive.com/assets/BehaviorDesigner/Movement/documentation.php?id=6")]
    [TaskIcon("Assets/Behavior Designer Movement/Editor/Icons/{SkinColor}EvadeIcon.png")]
    public class Evade : PolyNavSteeringBase
    {
        [Tooltip("The agent has fleed when the square magnitude is greater than this value")]
        public SharedFloat fleedDistance = 10;
        [Tooltip("The distance to look ahead when fleeing")]
        public SharedFloat lookAheadDistance = 5;
        [Tooltip("How far to predict the distance ahead of the target. Lower values indicate less distance should be predicated")]
        public SharedFloat targetDistPrediction = 20;
        [Tooltip("Multiplier for predicting the look ahead distance")]
        public SharedFloat targetDistPredictionMult = 20;
        [Tooltip("The transform that the agent is evading")]
        public SharedTransform targetTransform;

        // The position of the target at the last frame
        private Vector2 prevPosition;
        private Vector3 targetPosition;
        private Vector2 prevTargetPosition;

        public override void OnStart()
        {
            targetPosition = targetTransform.Value.position;
            prevPosition = transform.position;

            base.OnStart();
        }

        // Seek the destination. Return success once the agent has reached the destination.
        // Return running if the agent hasn't reached the destination yet
        public override TaskStatus OnUpdate()
        {
            if (Vector3.SqrMagnitude(transform.position - targetTransform.Value.position) > fleedDistance.Value) {
                return TaskStatus.Success;
            }

            UpdateDestination();
            
            return TaskStatus.Running;
        }
        
        private Vector2 TargetPosition()
        {
            // Calculate the current distance to the target and the current speed
            var distance = (targetTransform.Value.position - transform.position).magnitude;
            var speed = ((Vector2)transform.position - prevPosition).magnitude;
            prevPosition = transform.position;

            float futurePrediction = 0;
            // Set the future prediction to max prediction if the speed is too small to give an accurate prediction
            if (speed <= distance / targetDistPrediction.Value) {
                futurePrediction = targetDistPrediction.Value;
            } else {
                futurePrediction = (distance / speed) * targetDistPredictionMult.Value; // the prediction should be accurate enough
            }

            // Predict the future by taking the velocity of the target and multiply it by the future prediction
            var prevTargetPosition = targetPosition;
            targetPosition = targetTransform.Value.position;
            return targetPosition + (targetPosition - prevTargetPosition) * futurePrediction;
        }

        // Return targetPosition if targetTransform is null
        protected override Vector2 Target()
        {
            return transform.position + (transform.position - (Vector3)TargetPosition()).normalized * lookAheadDistance.Value;
        }

        public override void OnReset()
        {
            targetTransform = null;
            targetPosition = Vector3.zero;
        }
    }
}