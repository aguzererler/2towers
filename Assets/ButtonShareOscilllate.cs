﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class ButtonShareOscilllate : MonoBehaviour {

	int oscDir = 1;
	Quaternion initRotation;
	bool doOscillate;

	void Awake(){
		initRotation = this.transform.localRotation;
		
	}
	
	void Start () {
		transform.localRotation = initRotation;
		doOscillate = false;
		StartCoroutine(WaitToOscillate(Random.Range(3,6)));

	}
	
	// Update is called once per frame
	void Update () {


	}

	IEnumerator WaitToOscillate(float t){
		yield return new WaitForSeconds(t);
		transform.DOShakeRotation(1.8f,new Vector3(0,0,-40),10,20).OnComplete(OnTweenCompelete);
		//transform.DOLocalRotate(new Vector3(0,0,Random.Range(0,-40)),Random.Range(0,1.4f)).SetLoops(2,LoopType.Yoyo).OnComplete(OnTweenCompelete);


	}

	void OnTweenCompelete(){
		StartCoroutine(WaitToOscillate(Random.Range(3,6)));
	}
}
