﻿#if UNITY_EDITOR && (UNITY_5_0 || UNITY_5_1) && UNITY_STANDALONE
using System;
using UnityEditor;
using System.IO;

namespace UnityEngine.Cloud.Analytics
{
	public static class UnityAnalyticsBuildHooks{
		public static void CreatePackageHook(){
			SetPluginImportSettings();
		}

		private static void SetPluginImportSettings(){
			var pluginImporters = PluginImporter.GetAllImporters ();

			foreach (var pluginImporter in pluginImporters) {
				string folderName = Path.GetFileName(Path.GetDirectoryName(pluginImporter.assetPath));
				string fileName = Path.GetFileName(pluginImporter.assetPath);

				if(folderName.Equals("Plugins"))
				{
					if(fileName.Equals("UnityEngine.Cloud.Analytics.dll")){
						pluginImporter.SetCompatibleWithAnyPlatform(false);

						foreach (Build buildEnum in System.Enum.GetValues(typeof(Build)))
						{
							pluginImporter.SetCompatibleWithPlatform(buildEnum, true);
						}
						pluginImporter.SetCompatibleWithEditor(true);

						pluginImporter.SetCompatibleWithPlatform(Build.WP8Player, false);
						pluginImporter.SetCompatibleWithPlatform(Build.WSAPlayer, false);

						pluginImporter.SaveAndReimport();
					}else if (fileName.Equals("UnityEngine.Cloud.Analytics.Util.dll")){
						pluginImporter.SetCompatibleWithAnyPlatform(false);

						foreach (Build buildEnum in System.Enum.GetValues(typeof(Build)))
						{
							pluginImporter.SetCompatibleWithPlatform(buildEnum, true);
						}
						pluginImporter.SetCompatibleWithEditor(true);

						pluginImporter.SetCompatibleWithPlatform(Build.WP8Player, true);
						pluginImporter.SetCompatibleWithPlatform(Build.WSAPlayer, false);

						pluginImporter.SaveAndReimport();
					}
				}
			}

		}
	}
}
#endif