﻿using UnityEngine;
using System.Collections;

namespace Assets.Code.Interfaces{

	public interface IBaseState {

		void Initialize();
		void StateUpdate();
		void Show ();
		void SwitchState();
	}
}
