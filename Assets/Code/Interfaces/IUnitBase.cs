﻿using UnityEngine;
using System.Collections;


public interface IUnitBase {

	void Init (GameObject go, int tr);
	void Update ();
	void Move ();	
	void Attack (GameObject go);
	void TakeDamage(float dmg);
	HealthProperties getHealthProperties();
	bool isAlive();
	int unitCode{ get; set;}
	void playAttackAnimation();
	void getAttackStand();
	void getNormalStand();
	void setUnitSprites(int typeTower);
	MovementProperties GetMovementProp();
	void setTier(int t);
	int getTier();
	//int getStand{get; set;}

}
