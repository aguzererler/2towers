﻿using UnityEngine;
using System.Collections;
using Assets.Code.Interfaces;



namespace Assets.Code.States{
	public class WelcomeState:IBaseState {


		private StateManager manager;
		
		
		public WelcomeState(StateManager managerRef){
			manager = managerRef;
			Initialize ();
			Debug.Log ("Constructing WelcomeState");
		}
		
		public void Initialize(){
			for(int i = 0; i < 10; i++){

			}
			
		}
		
		public void StateUpdate(){
			if (Input.GetKeyUp (KeyCode.Space))
			{
				manager.SwitchState (new PlayState (manager));
			}
		}
		public void Show (){
			
		}
		
		public void SwitchScene(){
			//Application.LoadLevel("s10");
			manager.SwitchState (new MainMenuState (manager));
		}

		public void SwitchState(){

		}

	}
}