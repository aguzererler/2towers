using UnityEngine;
using System.Collections;
using Assets.Code.Interfaces;



namespace Assets.Code.States{
	
	public class MainMenuState:IBaseState {

		const int PLAYSTATE = 1;


		
		public MainMenuState(StateManager managerRef){
			Initialize ();
			Debug.Log ("Constructing BeginState");
		}
		
		public void Initialize(){

		}

		public void StateUpdate(){

		}
		public void Show (){
			
		}

		public void SwitchScene(tk2dUIItem btnUI){
			if(btnUI.name == "PlayBTC" ){
				if(GameConstants.Instance.areParametersUpdated){
					Application.LoadLevel("4T_G020");
					StateManager.Instance.SwitchState (new PlayState(StateManager.Instance));
				}else{
					Debug.Log("Waiting for parameter download and try again");
				}
			}
		}

		public void SwitchState(){

		}


	}
}