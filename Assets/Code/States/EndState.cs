using UnityEngine;
using System.Collections;
using Assets.Code.Interfaces;


namespace Assets.Code.States{
	
	public class EndState: IBaseState{
		
		private StateManager manager;
		
		public EndState(StateManager managerRef){
			manager = managerRef;
			
			Debug.Log ("Constructing EndState");
		}
		
		public void Initialize(){

		}
		public void StateUpdate(){
			if (Input.GetKeyUp (KeyCode.Space))
			{
				manager.SwitchState (new MainMenuState (manager));
			}
		}
		public void Show (){
			
		}

		public void SwitchState(){

		}
		
	}
}