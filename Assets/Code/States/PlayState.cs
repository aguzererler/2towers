﻿using UnityEngine;
using System.Collections;
using Assets.Code.Interfaces;

namespace Assets.Code.States{
	
	public class PlayState: IBaseState{
		
		private StateManager manager;

		
		public PlayState(StateManager managerRef){
			manager = managerRef;
			
			Debug.Log ("Constructing PlayState");
		}
		
		
		public void Initialize(){
			Debug.Log("PlayState initialize");
		}
		public void StateUpdate(){

		}
		public void Show (){
			
		}

		public void SwitchState(){
			Application.LoadLevel("4T_Main");
			manager.SwitchState (new MainMenuState(manager));
		}

	

		
	}
}