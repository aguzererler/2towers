using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class InitFieldUnitTokens : Action
{
	public override void OnStart ()
	{
		ComputerAI.Instance.newUnitOnField = false;
		ComputerAI.Instance.newPlayerUnitOnField = false;
		ComputerAI.Instance.deadComputerUnit = false;
		ComputerAI.Instance.deadPlayerUnit = false;

	}
	public override TaskStatus OnUpdate()
	{
		return TaskStatus.Success;
	}
}