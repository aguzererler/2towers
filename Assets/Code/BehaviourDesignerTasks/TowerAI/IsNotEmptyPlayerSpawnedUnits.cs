using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class IsNotEmptyPlayerSpawnedUnits : Conditional
{
	public override TaskStatus OnUpdate()
	{
		if(ComputerAI.Instance.playerSpawnedUnits.Count > 0)
			return TaskStatus.Success;
		else
			return TaskStatus.Failure;
	}
}