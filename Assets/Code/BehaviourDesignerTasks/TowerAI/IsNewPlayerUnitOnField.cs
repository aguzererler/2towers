using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;


public class IsNewPlayerUnitOnField: Conditional
{
	public override TaskStatus OnUpdate()
	{
		if(ComputerAI.Instance.newPlayerUnitOnField)
			return TaskStatus.Success;
		else
			return TaskStatus.Failure;
	}
}