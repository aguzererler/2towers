using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class CheckTimePointsToAttack : Conditional
{
	public SharedFloat aC;
	public override TaskStatus OnUpdate()
	{
		if (aC.Value < 1) {
			if (ComputerAI.Instance.timeKeeper.CalculateCurrentBudget () > ComputerAI.Instance.minTimePointsToAttack)
				return TaskStatus.Success;
			else
				return TaskStatus.Failure;
			} else
				return TaskStatus.Success;
	}
}