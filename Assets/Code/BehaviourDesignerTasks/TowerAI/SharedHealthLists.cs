using UnityEngine;
using BehaviorDesigner.Runtime;



[System.Serializable]
public class SharedHealthPropertyLists : SharedVariable
{
	public HealthPropertyLists Value { get { return mValue; } set { mValue = value; } }
	[SerializeField]
	private HealthPropertyLists mValue;

	public override object GetValue() { return mValue; }
	public override void SetValue(object value) { mValue = (HealthPropertyLists)value; }

	public override string ToString() { return mValue == null ? "null" : mValue.ToString(); }
	public static implicit operator SharedHealthPropertyLists(HealthPropertyLists value) { var sharedVariable = new SharedHealthPropertyLists(); sharedVariable.SetValue(value); return sharedVariable; }
}