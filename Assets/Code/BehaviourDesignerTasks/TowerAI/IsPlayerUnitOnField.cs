using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using System.Collections.Generic;

public class IsPlayerUnitOnField: Conditional
{

	public override TaskStatus OnUpdate()
	{

		if(ComputerAI.Instance.playerUnitsOnField.Count == 0)
			return TaskStatus.Failure;
		else
			return TaskStatus.Success;
	}
}