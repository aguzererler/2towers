using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using System.Collections.Generic;
using System.Linq;

public class SimulateBattleLists : Conditional
{
	//simulation results
	//inputs
	//public SharedUnitTable uToBeSpawned;
	public SharedResultBattleSimList resultBattleSim;	
	public PrepareSimLists simListPrepTask;
	float computerTowerHealth;
	float playerTowerHealth;
	
	private int nOUnitTypes;
	public bool isAttackTower;

	public override void OnStart()
	{
		nOUnitTypes = GameConstants.noUnitTypes;
		if (resultBattleSim == null){
			resultBattleSim = new SharedResultBattleSimList ();
			resultBattleSim.Value = new ResultBattleSimList();
		}
		else{
			if(resultBattleSim.Value == null)
				resultBattleSim.Value = new ResultBattleSimList();
			else
				resultBattleSim.Init ();
		}
	}

	public override TaskStatus OnUpdate()
	{	

		float simBattleDuration;

		computerTowerHealth = simListPrepTask.GetComputerTowerHealth();
		playerTowerHealth = simListPrepTask.GetPlayerTowerHealth();

		List<SimUnit> computerSimUnits;
		List<SimUnit> playerSimUnits;
		List<HealthProperties>[] computerUnitHealths;
		List<HealthProperties>[] playerUnitHealths;

		playerSimUnits = simListPrepTask.simUnitLists.playerSimUnits;
		computerSimUnits = simListPrepTask.simUnitLists.computerSimUnits;

		computerUnitHealths = new List<HealthProperties>[nOUnitTypes]; 
		playerUnitHealths = new List<HealthProperties>[nOUnitTypes];


		for (int i = 0; i < GameConstants.noUnitTypes; i ++) {
			computerUnitHealths[i] = new List<HealthProperties>();
			playerUnitHealths[i] = new List<HealthProperties>();
		}

		foreach(SimUnit s in playerSimUnits){
			HealthProperties h = s.healthP;
			playerUnitHealths[s.unitCode].Add(h);
		}

		foreach(SimUnit s in computerSimUnits){
			HealthProperties h = s.healthP;
			computerUnitHealths[s.unitCode].Add(h);
		}
	
		simBattleDuration = SimulateBattle(ref computerSimUnits, ref playerSimUnits, ref computerUnitHealths, ref playerUnitHealths);

		if(isAttackTower){
			for (int i = 0; i < nOUnitTypes; i++) {
				foreach(HealthProperties h in computerUnitHealths[i]){
					float coeffU = h.health * GameConstants.unitTowerDamageCoeff;
					playerTowerHealth -= coeffU ;
				}		
				
				foreach(HealthProperties h in playerUnitHealths[i]){
					float coeffU = h.health * GameConstants.unitTowerDamageCoeff;
					computerTowerHealth -= coeffU ;
				}	
			}
		}

		float unitSpeed = 60;
		float etoaP = ComputerAI.Instance.closestPlayerUnitToComputerTw / unitSpeed;
		etoaP += simBattleDuration;

		float etoaC = ComputerAI.Instance.closestComputerUnitToPlayerTw / unitSpeed;
		etoaC += simBattleDuration;
		
		resultBattleSim.SetAll (computerSimUnits, playerSimUnits, computerTowerHealth, playerTowerHealth, simBattleDuration, etoaC, etoaP);


		if (computerSimUnits.Count > 0){

			if(simListPrepTask.isFieldLists.Value && !simListPrepTask.isStackLists.Value){
				ComputerAI.Instance.newUnitOnField = false;
				ComputerAI.Instance.newPlayerUnitOnField = false;
			}
			//Debug.Log("sim sucess");
			return TaskStatus.Success;
		}else {
			//Debug.Log("sim failure");
			return TaskStatus.Failure;
			
		}
	}
	
	private float SimulateBattle(ref List<SimUnit> eSU, ref List<SimUnit> pSU,
	                            ref List<HealthProperties>[] computerForcesHealth, ref List<HealthProperties>[] playerForcesHealth){
		
		//List<GameObject> attackedComputerUnits;
		//List<GameObject> attackedPlayerUnits;

		float simBattleDuration = 0;

		while(eSU.Count != 0 && pSU.Count != 0){
			int noAttackTurns = (eSU.Count > pSU.Count) ? eSU.Count : pSU.Count; 
			for(int i= 0; i < noAttackTurns; i++){ 
				simBattleDuration++;
				if (Random.value < 0.5f) {
					if(pSU.Count>i){
						//Debug.Log("> i");
						if(pSU[i].healthP.health > 0){
							//Debug.Log("B Attack");
							UnitAttack(pSU[i].unitCode, pSU[i].attackP, ref computerForcesHealth);
						}
					}
					if(eSU.Count>i){
						//Debug.Log("> i");
						if(eSU[i].healthP.health > 0){
							//Debug.Log("B Attack");
							UnitAttack(eSU[i].unitCode, eSU[i].attackP, ref playerForcesHealth);
						}
					}
				}else {
					if(eSU.Count > i){
						if(eSU[i].healthP.health > 0){
							UnitAttack(eSU[i].unitCode, eSU[i].attackP, ref playerForcesHealth);
						}
					}
					if(pSU.Count > i){
						//Debug.Log("> i");
						if(pSU[i].healthP.health > 0){
							//Debug.Log("B Attack");
							UnitAttack(pSU[i].unitCode, pSU[i].attackP, ref computerForcesHealth);
						}
					}
				}
				
				List<int> dPList =  new List<int>();
				
				for(int j = 0; j < pSU.Count; j++){
					SimUnit u = pSU[j];
					if(u.healthP.health<=0)
						dPList.Add(j);
				}
				
				if(dPList.Count>0){
					foreach(int d in dPList){
						pSU.RemoveAt(d);
					}
				}
				
				List<int> dEList =  new List<int>();
				for(int j = 0; j < eSU.Count; j++){
					SimUnit u = eSU[j];
					if(u.healthP.health<=0)
						dEList.Add(j);
				}
				if(dEList.Count>0){
					foreach(int d in dEList){
						eSU.RemoveAt(d);
					}
				}
				
				noAttackTurns = (eSU.Count > pSU.Count) ? eSU.Count : pSU.Count;
				
			}
		}

		return simBattleDuration;
	}
	
	private bool UnitAttack(int attackerType, AttackProperties attackerAttackP, ref List<HealthProperties>[] unitsToBeAttacked){
		bool attackComplete = false;
		
		for(int i = 1; i > -2; i--){
			int k = (attackerType+3+i)% nOUnitTypes;
			//Debug.Log("attacker type:" + attackerType + "k: " + k);
			if(unitsToBeAttacked[k].Count >0){
				unitsToBeAttacked[k][0].health -= attackerAttackP.GetDamage(k);//GameConstants.GetAvgDamage(attackerType,k);//add bonus
				attackComplete = true;
				if(unitsToBeAttacked[k][0].health <0)
					unitsToBeAttacked[k].RemoveAt(0);
			}
			
			if(attackComplete)
				break;
		}
		
		return attackComplete;
	}

	public float GetPlayerTowerHealth(){
		return playerTowerHealth;
	}

	public float GetComputerTowerHealth(){
		return computerTowerHealth;
	}
	
}
/*
[System.Serializable]
public class SimUnit //Not a MonoBehaviour!
{
	public int unitCode;
	public AttackProperties attackP;
	public HealthProperties healthP;
	
	public SimUnit(){
		
	}
	
}
*/

