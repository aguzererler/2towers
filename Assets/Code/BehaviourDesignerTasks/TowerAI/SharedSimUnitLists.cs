using UnityEngine;
using BehaviorDesigner.Runtime;
using System.Collections.Generic;

[System.Serializable]
public class SimUnitLists
{
	public List<SimUnit> playerSimUnits;
	public List<SimUnit> computerSimUnits;

	public List<HealthProperties>[] computerUnitsHealthProp;
	public List<HealthProperties>[] playerUnitsHealthProp;

	public SimUnitLists (){
		/*
		bool isNull = true;
		if (computerSimUnits != null || playerSimUnits != null)
			isNull = false;
		Debug.Log ("SimulationResultlist Initialized, null: " + isNull);
		*/
		playerSimUnits = new List<SimUnit> ();
		computerSimUnits = new List<SimUnit> ();

		computerUnitsHealthProp = new List<HealthProperties>[GameConstants.noUnitTypes];
		playerUnitsHealthProp = new List<HealthProperties>[GameConstants.noUnitTypes];
		for (int i = 0; i < GameConstants.noUnitTypes; i ++) {
			computerUnitsHealthProp[i] = new List<HealthProperties>();
			playerUnitsHealthProp[i] = new List<HealthProperties>();
		}

	}

	public void ClearLists(){
		playerSimUnits.Clear ();
		computerSimUnits.Clear ();

		for (int i = 0; i < GameConstants.noUnitTypes; i ++) {
			computerUnitsHealthProp[i].Clear();
			playerUnitsHealthProp[i].Clear();
		}
	}
}

[System.Serializable]
public class SharedSimUnitLists : SharedVariable
{
	[SerializeField]
	private SimUnitLists mValue;

	public SimUnitLists Value { get { 
			if(mValue == null)
				mValue = new SimUnitLists();
			return mValue; } set { mValue = value; } }

	public override object GetValue() {
		if(mValue == null)
			mValue = new SimUnitLists();
		return mValue; }
	public override void SetValue(object value) { 
		if(mValue == null)
			mValue = new SimUnitLists();
		mValue = (SimUnitLists)value; 
	}

	public override string ToString() { return mValue == null ? "null" : mValue.ToString(); }
	public static implicit operator SharedSimUnitLists(SimUnitLists value) { var sharedVariable = new SharedSimUnitLists(); sharedVariable.SetValue(value); return sharedVariable; }
}