using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class NextWaveHordeCheck : Conditional
{
	public SharedUnitTable uTBS;
	public SharedUnitTable nWUTBS;

	public SharedInt lDSUC;
	public SharedInt nWLDSUC;

	public SharedFloat playerUnitsESTOA;

	public SharedFloat WaitDurationForAttack;

	public SharedFloat playerTowerHealthAfterSim;
	public SharedFloat computerTowerHealthAfterSim;


	public SharedFloat nWPayerTowerHealthAfterSim;
	public SharedFloat nWComputerTowerHealthAfterSim;

	public SharedFloat nWttackChance; 

	
	public override TaskStatus OnUpdate()
	{
		uTBS.Value.AddByCode (lDSUC.Value, 1);
		nWUTBS.Value.AddByCode (nWLDSUC.Value, 1);

		int[] uTBSA = uTBS.Value.ToIntArray ();
		int[] nWTBSA = nWUTBS.Value.ToIntArray ();

		float nWPlayerTowerHealth = nWPayerTowerHealthAfterSim.Value;
		float nWComputerTowerHealth = nWComputerTowerHealthAfterSim.Value;

		float hordeCost = ComputerAI.Instance.timeKeeper.CalculateHordeCostWithError (uTBSA);
		float nWhordeCost = ComputerAI.Instance.timeKeeper.CalculateHordeCostWithError (nWTBSA);

		hordeCost += nWhordeCost;


		float wT = (playerUnitsESTOA.Value + ComputerAI.Instance.closestPlayerUnitToComputerTw / 60);
		nWhordeCost -= (ComputerAI.Instance.computerSpawnDeviationFromPlayer * ComputerAI.Instance.GetPlayerCurrentSpawnDeviation()) * WaitDurationForAttack.Value;
		WaitDurationForAttack.Value = wT * 1.25f;

		//Debug.Log ("Simulation Cost: " + hordeCost + " TimePoints: " + ComputerAI.Instance.timeKeeper.CalculateCurrentBudget());

		while (ComputerAI.Instance.timeKeeper.CheckSimulationCost (hordeCost)) {
			int uc = Random.Range(0,3);
			hordeCost += ComputerAI.Instance.timeKeeper.calculateSingleWithError(uc);
			nWPlayerTowerHealth -= GameConstants.GetUnitHealthMax(uc, ComputerAI.Instance.GetTierComputerSpawner(uc))/GameConstants.unitTowerDamageCoeff; 
		}

		float deltaPlayerTowerHealth = playerTowerHealthAfterSim.Value - nWPlayerTowerHealth;
		float deltaComputerTowerHealth = computerTowerHealthAfterSim.Value - nWComputerTowerHealth;

		if(deltaPlayerTowerHealth > deltaComputerTowerHealth*Random.Range(0.9f,1.5f)){
			nWttackChance = 1;
			return TaskStatus.Success;
		}
		else
			return TaskStatus.Failure;
	}
}