using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class UnitSpawnController : Action
{
	public SharedUnitTable uSimResults;
	public SharedUnitTable uTBS;
	public SharedInt lastDroppedUC;

	public override void OnStart()
	{
		int uD = uTBS.Value.GetMostPopulatedUnitType ();
		uTBS.Value.SubstractByCode (uD, 1);
		lastDroppedUC.Value = uD; 
	}

	public override TaskStatus OnUpdate()
	{
		return TaskStatus.Success;
	}
}