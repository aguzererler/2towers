using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class SetEstimatePlayerStack : Action
{	
	public SharedBool ePS;
	public SharedBool setTo;
	public override void OnStart()
	{
		
	}

	public override TaskStatus OnUpdate()
	{
		ePS.Value = setTo.Value;
		return TaskStatus.Success;
	}
}