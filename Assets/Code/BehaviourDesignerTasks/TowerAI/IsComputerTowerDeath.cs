using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class WillComputerTowerSurvive : Conditional
{
	public SharedFloat computerTowerHealthAfterSim;

	public override TaskStatus OnUpdate()
	{
		if (computerTowerHealthAfterSim.Value >= GameConstants.criticalTowerHealth)
				return TaskStatus.Success;
		else
				return TaskStatus.Failure;
	}
}