using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using System.Collections.Generic;
using System.Linq;

public class PrepareSimLists : Action
{


	//input
	public SharedBool isFieldLists;
	public SharedBool isStackLists;
	public SharedBool estimatePlayerStack;

	public SharedUnitTable estimatedPlayerStackTable;

	public SharedCalculationResults resultSimCalc;
	public SharedResultBattleSimList resultBattleSim;

	public SpawnSimulationList refSpawnSimTask;

	//output
	public SimUnitLists simUnitLists;

	int[] estimatedPlayerStack;

	
	private int[] pUT; 
	private int[] eUT; 
	private int nOUnitTypes;
	float playerTowerHealth;
	float computerTowerHealth;

	//results/outputs

	//public HealthPropertyLists healthPropertyLists;
	//public SharedFloat asComputerTowerHealth;
	//public SharedFloat asPlayerTowerHealth;
	//public SharedUnitTable uToBeSpawned;

	
	public override void OnAwake ()
	{
		nOUnitTypes = GameConstants.noUnitTypes;
		base.OnAwake ();
		//healthPropertyLists = new HealthPropertyLists ();
		simUnitLists = new SimUnitLists ();
	}
	
	public override void OnStart()
	{
		simUnitLists.ClearLists ();
		//List<SimUnit> playerSimUnits = simUnitLists.playerSimUnits;
		List<SimUnit> playerSimUnits;
		List<SimUnit> computerSimUnits;
		UnitTable uTBSpawned;

		if (resultBattleSim.Value != null && !(resultBattleSim.Value.computerTowerHealth < -98 
		                                      && resultBattleSim.Value.playerTowerHealth < -98)) {
			playerSimUnits = resultBattleSim.GetPlayerSimResult();
			computerSimUnits  = resultBattleSim.GetComputerSimResult();
			computerTowerHealth = resultBattleSim.Value.computerTowerHealth;
			playerTowerHealth = resultBattleSim.Value.playerTowerHealth;
		}else{
			playerSimUnits = new List<SimUnit>();
			computerSimUnits = new List<SimUnit>();
			computerTowerHealth = ComputerAI.Instance.GetComputerTowerHealth();
			playerTowerHealth = ComputerAI.Instance.GetPlayerTowerHealth();
		} 

		
		pUT = new int[nOUnitTypes];
		eUT = new int[nOUnitTypes];

		if (resultSimCalc.Value != null && resultSimCalc.Value.lastDroppedUnitCode != -1) {
			uTBSpawned = resultSimCalc.Value.cSpawnUT;
		}
		else
			uTBSpawned = null;
		
		for (int i = 0; i<pUT.Length; i++) {
			pUT[i] = 0;
		}
		for (int i = 0; i<eUT.Length; i++){
			eUT[i] = 0;
		}
		
		if(isFieldLists.Value){
			foreach (GameObject pqgo in ComputerAI.Instance.computerUnitsInOutQue) {
				Unit u = pqgo.GetComponent<Unit>() as Unit;
				SimUnit su = new SimUnit();
				su.unitCode = u.unitCode;
				su.healthP = ObjectCopier.Clone(u.getHealthProperties());
				su.attackP = ObjectCopier.Clone(u.getAttackProperties());
				computerSimUnits.Add(su);
				eUT[u.unitCode]++;
			}
			
			foreach (GameObject ego in ComputerAI.Instance.computerUnitsOnField) {
				Unit u = ego.GetComponent<Unit>() as Unit;
				SimUnit su = new SimUnit();
				su.unitCode = u.unitCode;
				su.healthP = ObjectCopier.Clone(u.getHealthProperties());
				su.attackP = ObjectCopier.Clone(u.getAttackProperties());
				computerSimUnits.Add(su);
				eUT[u.unitCode]++;
			}
			
			foreach (GameObject pgo in ComputerAI.Instance.playerUnitsOnField) {
				Unit u = pgo.GetComponent<Unit>() as Unit;
				SimUnit su = new SimUnit();
				su.unitCode = u.unitCode;
				su.healthP = ObjectCopier.Clone(u.getHealthProperties());
				su.attackP = ObjectCopier.Clone(u.getAttackProperties());
				playerSimUnits.Add(su);
				pUT[u.unitCode]++;
			}
		}
		
		if(isStackLists.Value){
			if(estimatePlayerStack.Value){
				if(estimatedPlayerStackTable.Value == null){
					estimatedPlayerStackTable.Value = new UnitTable();
				}else
					estimatedPlayerStackTable.Value.Init();

				estimatedPlayerStack = ComputerAI.Instance.EstimatePlayerUnitsInCastle();
				
				for(int i = 0; i< estimatedPlayerStack.Length; i++){
					estimatedPlayerStackTable.Value.AddByCode(i,estimatedPlayerStack[i]);
					for(int j = 0; j < estimatedPlayerStack[i]; j++){
						SimUnit tSU = GenerateNewSimUnit(i, true);
						playerSimUnits.Add(tSU);
						pUT[tSU.unitCode]++;

					}	
				}
				estimatePlayerStack.Value = false;
			}else{

				for(int i = 0; i< nOUnitTypes; i++){
					for(int j = 0; j < estimatedPlayerStackTable.Value.GetNOByCode(i) ; j++){
						SimUnit tSU = GenerateNewSimUnit(i, true);
						playerSimUnits.Add(tSU); 
						pUT[tSU.unitCode]++;
						
					}	
				}

			}
		}
		
		if (uTBSpawned != null && uTBSpawned.notEmpty ()) {
			for(int i = 0; i < uTBSpawned.nOSpearman; i++){
				SimUnit tSU = GenerateNewSimUnit(0, false);
				computerSimUnits.Add(tSU);
				eUT[tSU.unitCode]++;;
			}		
			for(int i = 0; i < uTBSpawned.nOHorseman; i++){
				SimUnit tSU = GenerateNewSimUnit(1, false);
				computerSimUnits.Add(tSU);;
				eUT[tSU.unitCode]++;
			}		
			for(int i = 0; i < uTBSpawned.nOArcher; i++){
				SimUnit tSU = GenerateNewSimUnit(2, false);
				computerSimUnits.Add(tSU);
				eUT[tSU.unitCode]++;
			}
		}

		if (refSpawnSimTask != null) {
			if(refSpawnSimTask.simSpawnedUnits.notEmpty()){
				for(int i = 0; i < refSpawnSimTask.simSpawnedUnits.nOSpearman; i++){
					SimUnit tSU = GenerateNewSimUnit(0, false);
					computerSimUnits.Add(tSU);
					eUT[tSU.unitCode]++;;
				}		
				for(int i = 0; i < refSpawnSimTask.simSpawnedUnits.nOHorseman; i++){
					SimUnit tSU = GenerateNewSimUnit(1, false);
					computerSimUnits.Add(tSU);
					eUT[tSU.unitCode]++;
				}		
				for(int i = 0; i < refSpawnSimTask.simSpawnedUnits.nOArcher; i++){
					SimUnit tSU = GenerateNewSimUnit(2, false);
					computerSimUnits.Add(tSU);
					eUT[tSU.unitCode]++;
				}
			}		
		}

		simUnitLists.playerSimUnits = playerSimUnits;
		simUnitLists.computerSimUnits = computerSimUnits;

	}
	
	public override TaskStatus OnUpdate()
	{
		return TaskStatus.Success;
	}
	
	private SimUnit GenerateNewSimUnit(int uCode, bool isPlayerUnit){
		SimUnit su = new SimUnit();
		su.unitCode = uCode;
		int uTier;
		if(isPlayerUnit){
			uTier = ComputerAI.Instance.GetTierPlayerSpawner(uCode);
		}else{
			uTier = ComputerAI.Instance.GetTierComputerSpawner(uCode);
		}
		
		su.healthP = GameConstants.GetUnitHealthProperties(uCode, uTier);
		su.attackP = GameConstants.GetUnitAttackProperties(uCode, uTier);
		return su;
	}

	public float GetPlayerTowerHealth(){
		return playerTowerHealth;
	}
	
	public float GetComputerTowerHealth(){
		return computerTowerHealth;
	}
}

[System.Serializable]
public class SimUnit //Not a MonoBehaviour!
{
	public int unitCode{ get; set;}
	public float distanceToTargetTower{ get; set;}
	public AttackProperties attackP{ get; set;}
	public HealthProperties healthP{ get; set;}

	public SimUnit(){
		
	}
	
}

[System.Serializable]
public class HealthPropertyLists
{
	public List<HealthProperties>[] computerForcesHealth;
	public List<HealthProperties>[] playerForcesHealth;
	int nOUnitTypes;
	
	public HealthPropertyLists(){
		nOUnitTypes = GameConstants.noUnitTypes;
		computerForcesHealth = new List<HealthProperties>[nOUnitTypes]; 
		playerForcesHealth = new List<HealthProperties>[nOUnitTypes];
	}
	
	public void ClearHealthproperties(){
		for (int i = 0; i < nOUnitTypes ; i++) {
			computerForcesHealth[i] = new List<HealthProperties>();
			playerForcesHealth[i] = new List<HealthProperties>();
		}
	}
}

