using UnityEngine;
using BehaviorDesigner.Runtime;

[System.Serializable]
public class SharedAttackProperties : SharedVariable
{
	public AttackProperties Value { get { return mValue; } set { mValue = value; } }
	[SerializeField]
	private AttackProperties mValue;

	public override object GetValue() { return mValue; }
	public override void SetValue(object value) { mValue = (AttackProperties)value; }

	public override string ToString() { return mValue == null ? "null" : mValue.ToString(); }
	public static implicit operator SharedAttackProperties(AttackProperties value) { var sharedVariable = new SharedAttackProperties(); sharedVariable.SetValue(value); return sharedVariable; }
}