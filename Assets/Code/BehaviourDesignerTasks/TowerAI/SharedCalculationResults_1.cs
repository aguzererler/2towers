using UnityEngine;
using BehaviorDesigner.Runtime;

[System.Serializable]
public class CalculationResults
{
	public UnitTable pSpawnUT;
	public UnitTable cSpawnUT;
	public int lastDroppedUnitCode;


	public CalculationResults(){
		pSpawnUT = new UnitTable ();
		cSpawnUT = new UnitTable ();
		lastDroppedUnitCode = -1;
	}
}

[System.Serializable]
public class SharedCalculationResults : SharedVariable
{
	public CalculationResults Value { get { return mValue; } set { mValue = value; } }
	[SerializeField]
	private CalculationResults mValue;

	public override object GetValue() { return mValue; }
	public override void SetValue(object value) { mValue = (CalculationResults)value; }
	public void Init(){
		mValue.pSpawnUT.Init ();
		mValue.cSpawnUT.Init ();
		mValue.lastDroppedUnitCode = -1;
	}
	public override string ToString() { return mValue == null ? "null" : mValue.ToString(); }
	public static implicit operator SharedCalculationResults(CalculationResults value) { var sharedVariable = new SharedCalculationResults(); sharedVariable.SetValue(value); return sharedVariable; }
}