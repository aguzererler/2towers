using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class InitSpawnUnitTable : Action
{
	public SharedUnitTable sUT;

	public override void OnStart()
	{
		sUT.Value.Init ();
	}

	public override TaskStatus OnUpdate()
	{
		//Debug.Log ("CounterAttack Calc Restarted");
		return TaskStatus.Success;
	}
}