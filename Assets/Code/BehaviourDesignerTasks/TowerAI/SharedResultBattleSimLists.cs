using UnityEngine;
using BehaviorDesigner.Runtime;
using System.Collections.Generic;

[System.Serializable]
public class ResultBattleSimList
{
	public List<SimUnit> computerSimUnits;
	public List<SimUnit> playerSimUnits;

	public float playerTowerHealth { get; set;}
	public float computerTowerHealth { get; set;}
	public float battleDuration{ get; set;}
	public float etoaPlayerU { get; set;}
	public float etoaComputerU { get; set;}

	public ResultBattleSimList(){
		bool isNull = true;
		if (computerSimUnits != null || playerSimUnits != null)
						isNull = false;
		//Debug.Log ("SimulationResultlist Initialized, null: " + isNull);
		computerSimUnits = new List<SimUnit>();
		playerSimUnits = new List<SimUnit>();
		playerTowerHealth = -99;
		computerTowerHealth = -99;
	}

	public void SetAll(List<SimUnit> cSU, List<SimUnit> pSU, float cTH, float pTH, float bD, float eP, float eC){
		computerSimUnits = cSU;
		playerSimUnits = pSU;
		computerTowerHealth = cTH;
		playerTowerHealth = pTH;
		battleDuration = bD;
		etoaComputerU = eC;
		etoaPlayerU = eP;
	}



}

[System.Serializable]
public class SharedResultBattleSimList : SharedVariable
{
	public ResultBattleSimList Value { 
		get{
			if (mValue == null) {
				mValue = new ResultBattleSimList();		
			}
			return mValue;
		}
		
		set{
			if (mValue == null) {
				mValue = new ResultBattleSimList();		
			}
			mValue = value;
		}
	}
	[SerializeField]
	private ResultBattleSimList mValue;

	public override object GetValue() { return mValue; }
	public override void SetValue(object value) { 
		if (mValue == null) {
			mValue = new ResultBattleSimList();		
		}
		mValue = (ResultBattleSimList)value; 
	}

	public void SetComputerSimResult(List<SimUnit> cSU){
		if (mValue == null) {
			mValue = new ResultBattleSimList();		
		}
		mValue.computerSimUnits = cSU;
	}

	public void SetPlayerSimResult(List<SimUnit> pSU){
		if (mValue == null) {
			mValue = new ResultBattleSimList();		
		}
		mValue.playerSimUnits = pSU;
	}

	public List<SimUnit> GetPlayerSimResult(){
		//if (mValue == null) {
		//	mValue = new SimulationResultList();		
		//}
		return mValue.playerSimUnits;
	}

	public List<SimUnit> GetComputerSimResult(){
		if (mValue == null) {
			mValue = new ResultBattleSimList();		
		}
		return mValue.computerSimUnits;
	}

	public void SetAll(List<SimUnit> cSU, List<SimUnit> pSU, float cTH, float pTH, float bD, float eC, float eP){
		mValue.computerSimUnits = cSU;
		mValue.playerSimUnits = pSU;
		mValue.computerTowerHealth = cTH;
		mValue.playerTowerHealth = pTH;
		mValue.battleDuration = bD;
		mValue.etoaComputerU = eC;
		mValue.etoaPlayerU = eP;
	}

	public void Init(){
		mValue.computerSimUnits.Clear();
		mValue.playerSimUnits.Clear ();
		mValue.computerTowerHealth = 0;
		mValue.playerTowerHealth = 0;
		mValue.battleDuration = 0;
		mValue.etoaComputerU = 0;
		mValue.etoaPlayerU = 0;
	}

	public float GetPlayerTowerHealth(){
		return mValue.playerTowerHealth;
	}

	public float GetComputerTowerHealth(){
		return mValue.computerTowerHealth;
	}

	public float GetETOAPlayerU(){
		return mValue.etoaPlayerU;
	}

	public override string ToString() { return mValue == null ? "null" : mValue.ToString(); }
	public static implicit operator SharedResultBattleSimList(ResultBattleSimList value) { 
		var sharedVariable = new SharedResultBattleSimList(); 
		sharedVariable.SetValue(value); 
		return sharedVariable; 
	}
}