using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using System.Collections.Generic;

public class ResultBattleSimDublicator : Action
{
	public SharedResultBattleSimList toBeCopied;
	public SharedResultBattleSimList dublicant;

	public override void OnStart()
	{
		dublicant.Value.battleDuration = toBeCopied.Value.battleDuration;
		dublicant.Value.computerTowerHealth = toBeCopied.Value.computerTowerHealth;
		dublicant.Value.playerTowerHealth = toBeCopied.Value.playerTowerHealth;
		dublicant.Value.etoaComputerU = toBeCopied.Value.etoaComputerU;
		dublicant.Value.etoaPlayerU = toBeCopied.Value.etoaPlayerU;

		dublicant.Value.computerSimUnits = new List<SimUnit> ();
		dublicant.Value.playerSimUnits = new List<SimUnit> ();

		foreach (SimUnit s in toBeCopied.Value.computerSimUnits)
						dublicant.Value.computerSimUnits.Add (ObjectCopier.Clone (s));

		foreach (SimUnit s in toBeCopied.Value.playerSimUnits)
			dublicant.Value.playerSimUnits.Add (ObjectCopier.Clone (s));
	}

	public override TaskStatus OnUpdate()
	{
		return TaskStatus.Success;
	}
}