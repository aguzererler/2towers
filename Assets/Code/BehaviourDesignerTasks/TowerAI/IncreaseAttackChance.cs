using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class IncreaseAttackChance : Action
{
	public SharedFloat aC;
	
	public override TaskStatus OnUpdate()
	{
		aC.Value += Random.Range (0.05f, 0.15f); 
		return TaskStatus.Success;
	}
}