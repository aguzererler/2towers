using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class InitComputerAttackProp : Action
{
	public SharedFloat attackProp;

	public override void OnStart()
	{
		
	}

	public override TaskStatus OnUpdate()
	{
		attackProp.Value = 0.4f;
		return TaskStatus.Success;
	}
}