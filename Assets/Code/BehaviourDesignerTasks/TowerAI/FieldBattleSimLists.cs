using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using System.Collections.Generic;
using System.Linq;

public class FieldBattleSimLists : Action
{
	//results/outputs
	public SimUnitLists simUnitLists;
	public HealthPropertyLists healthPropertyLists;
	
	
	//after simulation
	public SharedFloat ComputerTowerHealthAfterSimulation;
	public SharedFloat PlayerTowerHealthAfterSimulation;
	
	//inputs
	public SharedUnitTable uToBeSpawned;
	public float playerTowerHealth;
	public float computerTowerHealth;
	
	
	//inputs
	
	public SharedBool isFieldLists;
	
	private int[] pUT; 
	private int[] eUT; 
	
	private int nOUnitTypes;
	
	public override void OnAwake ()
	{
		nOUnitTypes = GameConstants.noUnitTypes;
		base.OnAwake ();
		healthPropertyLists = new HealthPropertyLists ();
		simUnitLists = new SimUnitLists ();
	}
	
	public override void OnStart()
	{
		simUnitLists.ClearLists ();
		List<SimUnit> playerSimUnits = simUnitLists.playerSimUnits;
		List<SimUnit> computerSimUnits = simUnitLists.computerSimUnits;
		
		List<HealthProperties>[] computerForcesHealth = new List<HealthProperties>[nOUnitTypes]; 
		List<HealthProperties>[] playerForcesHealth = new List<HealthProperties>[nOUnitTypes];
		
		healthPropertyLists.computerForcesHealth = computerForcesHealth;
		healthPropertyLists.playerForcesHealth = playerForcesHealth;
		for (int i = 0; i < nOUnitTypes; i++) {
			computerForcesHealth[i] = new List<HealthProperties>();
			playerForcesHealth[i] = new List<HealthProperties>();
		}
		
		
		pUT = new int[nOUnitTypes];
		eUT = new int[nOUnitTypes];
		
		UnitTable uTBSpawned = uToBeSpawned.Value;
		
		for (int i = 0; i<pUT.Length; i++) {
			pUT[i] = 0;
		}
		for (int i = 0; i<eUT.Length; i++){
			eUT[i] = 0;
		}
		
		if(isFieldLists.Value){
			computerTowerHealth = ComputerAI.Instance.GetComputerTowerHealth();
			playerTowerHealth = ComputerAI.Instance.GetPlayerTowerHealth();
			
			foreach (GameObject pqgo in ComputerAI.Instance.computerUnitsInOutQue) {
				Unit u = pqgo.GetComponent<Unit>() as Unit;
				SimUnit su = new SimUnit();
				su.unitCode = u.unitCode;
				su.healthP = ObjectCopier.Clone(u.getHealthProperties());
				su.attackP = ObjectCopier.Clone(u.getAttackProperties());
				computerSimUnits.Add(su);
				HealthProperties hp = computerSimUnits.Last().healthP;
				computerForcesHealth[u.unitCode].Add(hp);
				eUT[u.unitCode]++;
			}
			
			foreach (GameObject ego in ComputerAI.Instance.computerUnitsOnField) {
				Unit u = ego.GetComponent<Unit>() as Unit;
				SimUnit su = new SimUnit();
				su.unitCode = u.unitCode;
				su.healthP = ObjectCopier.Clone(u.getHealthProperties());
				su.attackP = ObjectCopier.Clone(u.getAttackProperties());
				computerSimUnits.Add(su);
				computerForcesHealth[u.unitCode].Add(computerSimUnits.Last().healthP);
				eUT[u.unitCode]++;
			}
			
			if (uTBSpawned.notEmpty ()) {
				for(int i = 0; i < uTBSpawned.nOSpearman; i++){
					SimUnit tSU = GenerateNewSimUnit(0, false);
					computerSimUnits.Add(tSU);
					HealthProperties hp = computerSimUnits.Last().healthP;
					computerForcesHealth[tSU.unitCode].Add(hp);
					eUT[tSU.unitCode]++;
				}		
				for(int i = 0; i < uTBSpawned.nOHorseman; i++){
					SimUnit tSU = GenerateNewSimUnit(1, false);
					computerSimUnits.Add(tSU);
					HealthProperties hp = computerSimUnits.Last().healthP;
					computerForcesHealth[tSU.unitCode].Add(hp);
					eUT[tSU.unitCode]++;
				}		
				for(int i = 0; i < uTBSpawned.nOArcher; i++){
					SimUnit tSU = GenerateNewSimUnit(2, false);
					computerSimUnits.Add(tSU);
					HealthProperties hp = computerSimUnits.Last().healthP;
					computerForcesHealth[tSU.unitCode].Add(hp);
					eUT[tSU.unitCode]++;
				}
			}
			
			foreach (GameObject pgo in ComputerAI.Instance.playerUnitsOnField) {
				Unit u = pgo.GetComponent<Unit>() as Unit;
				SimUnit su = new SimUnit();
				su.unitCode = u.unitCode;
				su.healthP = ObjectCopier.Clone(u.getHealthProperties());
				su.attackP = ObjectCopier.Clone(u.getAttackProperties());
				playerSimUnits.Add(su);
				HealthProperties hp = playerSimUnits.Last().healthP;
				playerForcesHealth[u.unitCode].Add(hp);
				pUT[u.unitCode]++;
			}
			
		}else{
			computerTowerHealth = ComputerTowerHealthAfterSimulation.Value;
			playerTowerHealth 	= PlayerTowerHealthAfterSimulation.Value;
			
			int[] ePUC = ComputerAI.Instance.EstimatePlayerUnitsInCastle();
			
			for(int i = 0; i< ePUC.Length; i++){
				for(int j = 0; j < ePUC[i]; j++){
					SimUnit tSU = GenerateNewSimUnit(i, true);
					playerSimUnits.Add(tSU);
					HealthProperties hp = playerSimUnits.Last().healthP;
					playerForcesHealth[tSU.unitCode].Add(hp);
					pUT[tSU.unitCode]++;
				}	
			}
			
			if (uTBSpawned.notEmpty ()) {
				for(int i = 0; i < uTBSpawned.nOSpearman; i++){
					SimUnit tSU = GenerateNewSimUnit(0, false);
					computerSimUnits.Add(tSU);
					HealthProperties hp = computerSimUnits.Last().healthP;
					computerForcesHealth[tSU.unitCode].Add(hp);
					eUT[tSU.unitCode]++;;
				}		
				for(int i = 0; i < uTBSpawned.nOHorseman; i++){
					SimUnit tSU = GenerateNewSimUnit(1, false);
					computerSimUnits.Add(tSU);
					HealthProperties hp = computerSimUnits.Last().healthP;
					computerForcesHealth[tSU.unitCode].Add(hp);
					eUT[tSU.unitCode]++;
				}		
				for(int i = 0; i < uTBSpawned.nOArcher; i++){
					SimUnit tSU = GenerateNewSimUnit(2, false);
					computerSimUnits.Add(tSU);
					HealthProperties hp = computerSimUnits.Last().healthP;
					computerForcesHealth[tSU.unitCode].Add(hp);
					eUT[tSU.unitCode]++;
				}
			}
			
		}
		
	}
	
	public override TaskStatus OnUpdate()
	{
		return TaskStatus.Success;
	}
	
	private SimUnit GenerateNewSimUnit(int uCode, bool isPlayerUnit){
		SimUnit su = new SimUnit();
		su.unitCode = uCode;
		int uTier;
		if(isPlayerUnit){
			uTier = ComputerAI.Instance.GetTierPlayerSpawner(uCode);
		}else{
			uTier = ComputerAI.Instance.GetTierComputerSpawner(uCode);
		}

		su.healthP = GameConstants.GetUnitHealthProperties(uCode, uTier);
		su.attackP = GameConstants.GetUnitAttackProperties(uCode, uTier);
		return su;
	}
	
}

/*
[System.Serializable]
public class SimUnit //Not a MonoBehaviour!
{
	public int unitCode{ get; set;}
	public float distanceToTargetTower{ get; set;}
	public AttackProperties attackP{ get; set;}
	public HealthProperties healthP{ get; set;}
	
	public SimUnit(){
		
	}
	
}

[System.Serializable]
public class HealthPropertyLists
{
	public List<HealthProperties>[] computerForcesHealth;
	public List<HealthProperties>[] playerForcesHealth;
	int nOUnitTypes;
	
	public HealthPropertyLists(){
		nOUnitTypes = GameConstants.noUnitTypes;
		computerForcesHealth = new List<HealthProperties>[nOUnitTypes]; 
		playerForcesHealth = new List<HealthProperties>[nOUnitTypes];
	}
	
	public void ClearHealthproperties(){
		for (int i = 0; i < nOUnitTypes ; i++) {
			computerForcesHealth[i] = new List<HealthProperties>();
			playerForcesHealth[i] = new List<HealthProperties>();
		}
	}
}
 */