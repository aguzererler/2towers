﻿using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using System.Collections.Generic;

public class HordeSpawnBudgetCut : Conditional
{
	public SharedCalculationResults uTBS;
	
	public override TaskStatus OnUpdate()
	{
		if(uTBS.Value != null){
			List<int> uTBSL = uTBS.Value.cSpawnUT.ToUnitCodeList();
			
			
			int[] uTBSA = uTBS.Value.cSpawnUT.ToIntArray();
			//Debug.Log("HordeSpawn Units To Be Spawned: S" + uTBSA[0]+ " H: " + uTBSA[1]+ " A: "+ uTBSA[2]);
			float hordeCost = ComputerAI.Instance.timeKeeper.CalculateHordeCostWithError (uTBSA);
			//Debug.Log ("Simulation Cost: " + hordeCost + " TimePoints: " + ComputerAI.Instance.timeKeeper.CalculateCurrentBudget());
			

			foreach(int i in uTBSL){
				if(ComputerAI.Instance.timeKeeper.SpawnSingleWithError(i)){
					uTBS.Value.cSpawnUT.SubstractByCode(i,1f);
				}else
					break;
			}
				return TaskStatus.Success;
		}else{
			return TaskStatus.Failure;
		}
	}
}