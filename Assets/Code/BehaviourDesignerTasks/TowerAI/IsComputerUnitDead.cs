using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class IsComputerUnitDead : Conditional
{
	public override TaskStatus OnUpdate()
	{
		if(ComputerAI.Instance.deadComputerUnit)
			return TaskStatus.Success;
		else
			return TaskStatus.Failure;
	}
}