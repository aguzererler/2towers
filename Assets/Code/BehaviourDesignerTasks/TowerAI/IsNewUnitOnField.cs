using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using System.Collections.Generic;
using System.Linq;

public class IsNewUnitOnField : Conditional
{

	public override TaskStatus OnUpdate()
	{
		if (ComputerAI.Instance.newUnitOnField) {

			Debug.Log("New Unit on Field");
			return TaskStatus.Success;
		} else{
			//Debug.Log("No new Unit on Field");
			return TaskStatus.Failure;
		}
	}
}