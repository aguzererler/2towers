using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class CompareTowerHealths : Conditional
{
	//public SharedCalculationResults calcFieldCounterBattle;
	//public SharedCalculationResults calcNWBattle;

	public SharedResultBattleSimList sNW;
	public SharedResultBattleSimList sCH;

	public SharedResultBattleSimList resultFieldSim;
	public SharedFloat ETOA;

	public override TaskStatus OnUpdate()
	{
		float dTHNW = sNW.GetComputerTowerHealth() - sNW.GetPlayerTowerHealth();
		float dTHCB = sCH.GetComputerTowerHealth() - sCH.GetPlayerTowerHealth();
	
		ETOA.Value = resultFieldSim.GetETOAPlayerU();

		if (dTHNW > dTHCB)
			return TaskStatus.Success;
		else
			return TaskStatus.Failure;
	}
}