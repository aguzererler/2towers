using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using System.Collections.Generic;

public class SpawnSimulationList : Action
{
	public SharedCalculationResults calcFieldBattle;
	public SharedCalculationResults calcNWBattle;

	//public SharedUnitTable uTBS;
	//public SharedUnitTable nWUTBS;
		
	//public SharedInt lDSUC;
	//public SharedInt nWLDSUC;
		
	//public SharedFloat playerUnitsESTOA;
	//public SharedFloat preSimPlayerTowerHealth;
	//public SharedFloat preSimComputerTowerHealth;
		
	public SharedFloat nWAttackProb; 
	public bool includeCounterAttack;
	public UnitTable simSpawnedUnits;

	public override TaskStatus OnUpdate()
	{
		simSpawnedUnits = new UnitTable ();

		//uTBS.Value.AddByCode (lDSUC.Value, 1);
		//nWUTBS.Value.AddByCode (nWLDSUC.Value, 1);

		int[] uTBSA = calcFieldBattle.Value.cSpawnUT.ToIntArray ();
		int[] nWTBSA = calcNWBattle.Value.cSpawnUT.ToIntArray ();

		List<int> ucs;

		float nWPlayerTowerHealth = ComputerAI.Instance.GetPlayerTowerHealth();
		float nWComputerTowerHealth = ComputerAI.Instance.GetComputerTowerHealth();

		float hordeCost = ComputerAI.Instance.timeKeeper.CalculateHordeCostWithError (uTBSA);
		float nWhordeCost = ComputerAI.Instance.timeKeeper.CalculateHordeCostWithError (nWTBSA);
		float currentTimePoints = ComputerAI.Instance.timeKeeper.CalculateCurrentBudget();

		if (includeCounterAttack) {
			ucs = calcFieldBattle.Value.cSpawnUT.ToUnitCodeList ();
			ucs.AddRange(calcNWBattle.Value.cSpawnUT.ToUnitCodeList());
		}else{
			ucs = calcNWBattle.Value.cSpawnUT.ToUnitCodeList();
			//int uspeed = 60;
			//currentTimePoints += ComputerAI.Instance.closestPlayerUnitToComputerTw / uspeed;
		}

		while(ucs.Count != 0 && currentTimePoints >= 0 ){
			int i = Random.Range(0, ucs.Count);
			int u = ucs[i];
			currentTimePoints -= ComputerAI.Instance.timeKeeper.calculateSingleWithError(u);
			if(currentTimePoints >= 0){
				simSpawnedUnits.AddByCode(u,1f);
				ucs.RemoveAt(i);
			}
			else{
				break;
			}
		}
		//over units
		while (currentTimePoints > 0) {
			int u = Random.Range(0, ucs.Count);
			currentTimePoints -= ComputerAI.Instance.timeKeeper.calculateSingleWithError(u);
			if(currentTimePoints >= 0){
				simSpawnedUnits.AddByCode(u,1f);
			}
			else{
				break;
			}	
		}

		return TaskStatus.Success;
	}
}