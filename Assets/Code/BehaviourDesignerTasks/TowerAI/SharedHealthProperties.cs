using UnityEngine;
using BehaviorDesigner.Runtime;

[System.Serializable]
public class SharedHealthProperties : SharedVariable
{
	public HealthProperties Value { get { return mValue; } set { mValue = value; } }
	[SerializeField]
	private HealthProperties mValue;

	public override object GetValue() { return mValue; }
	public override void SetValue(object value) { mValue = (HealthProperties)value; }

	public override string ToString() { return mValue == null ? "null" : mValue.ToString(); }
	public static implicit operator SharedHealthProperties(HealthProperties value) { var sharedVariable = new SharedHealthProperties(); sharedVariable.SetValue(value); return sharedVariable; }
}