using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using System.Collections.Generic;
using System.Linq;

public class SimulateBattleOnField : Conditional
{
	//public SharedGameObjectList attackForce;
	//private List<HealthProperties>[] computerForcesHealth;
	//private List<HealthProperties>[] playerForcesHealth;
	public SharedSimUnitLists fieldSimResult;
	public SharedUnitTable playerUnitResults;
	public SharedUnitTable computerUnitResults;
	public SharedUnitTable uToBeSpawned;

	private int[] pUT; 
	private int[] eUT; 
	private int noEUiF;
	private int noPUiF;

	//private float[,] avgDamageTable;

	private int noUT;

	public override void OnAwake()
	{
		Debug.Log("sim " +fieldSimResult.GetHashCode() + " " + playerUnitResults.GetHashCode());
		//avgDamageTable = GameConstants.avgDamageTable;
	}


	public override void OnStart()
	{
		noUT = 3;
		noEUiF = ComputerAI.Instance.computerUnitsOnField.Count;
		noPUiF = ComputerAI.Instance.playerUnitsOnField.Count;
		pUT = new int[3];
		eUT = new int[3];

		//fieldSimResult = new SharedSimRUnitLists ();
	}

	private SimUnit GenerateNewSimUnit(int uCode, bool isPlayerUnit){
		SimUnit su = new SimUnit();
		su.unitCode = uCode;
		int uTier;
		if(isPlayerUnit){
			uTier = ComputerAI.Instance.GetTierPlayerSpawner(uCode);
		}else{
			uTier = ComputerAI.Instance.GetTierComputerSpawner(uCode);
		}
		
		su.healthP = GameConstants.GetUnitHealthProperties(uCode, uTier);
		su.attackP = GameConstants.GetUnitAttackProperties(uCode, uTier);
		return su;
	}

	public override TaskStatus OnUpdate()
	{	
		List<SimUnit> playerSimUnits = new List<SimUnit>();
		List<SimUnit> computerSimUnits = new List<SimUnit> ();
		UnitTable uTBSpawned = uToBeSpawned.Value;

		int nUnitTypes = 3;
		List<HealthProperties>[] computerForcesHealth = new List<HealthProperties>[nUnitTypes]; 
		List<HealthProperties>[] playerForcesHealth = new List<HealthProperties>[nUnitTypes];

		for (int i = 0; i < nUnitTypes; i++) {
			computerForcesHealth[i] = new List<HealthProperties>();
			playerForcesHealth[i] = new List<HealthProperties>();
		}

		for (int i = 0; i<pUT.Length; i++) {
			pUT[i] = 0;
		}
		for (int i = 0; i<eUT.Length; i++){
			eUT[i] = 0;
		}

		foreach (GameObject pqgo in ComputerAI.Instance.computerUnitsInOutQue) {
			Unit u = pqgo.GetComponent<Unit>() as Unit;
			SimUnit su = new SimUnit();
			su.unitCode = u.unitCode;
			su.healthP = ObjectCopier.Clone(u.getHealthProperties());
			su.attackP = ObjectCopier.Clone(u.getAttackProperties());
			computerSimUnits.Add(su);
			HealthProperties hp = computerSimUnits.Last().healthP;
			computerForcesHealth[u.unitCode].Add(hp);
			eUT[u.unitCode]++;
		}

		foreach (GameObject ego in ComputerAI.Instance.computerUnitsOnField) {
			Unit u = ego.GetComponent<Unit>() as Unit;
			SimUnit su = new SimUnit();
			su.unitCode = u.unitCode;
			su.healthP = ObjectCopier.Clone(u.getHealthProperties());
			su.attackP = ObjectCopier.Clone(u.getAttackProperties());
			computerSimUnits.Add(su);
			computerForcesHealth[u.unitCode].Add(computerSimUnits.Last().healthP);
			eUT[u.unitCode]++;
		}

		if (uTBSpawned.notEmpty ()) {
			for(int i = 0; i < uTBSpawned.nOSpearman; i++){
				SimUnit tSU = GenerateNewSimUnit(0, false);
				computerSimUnits.Add(tSU);
				HealthProperties hp = computerSimUnits.Last().healthP;
				computerForcesHealth[tSU.unitCode].Add(hp);
				eUT[tSU.unitCode]++;
			}		
			for(int i = 0; i < uTBSpawned.nOHorseman; i++){
				SimUnit tSU = GenerateNewSimUnit(1, false);
				computerSimUnits.Add(tSU);
				HealthProperties hp = computerSimUnits.Last().healthP;
				computerForcesHealth[tSU.unitCode].Add(hp);
				eUT[tSU.unitCode]++;
			}		
			for(int i = 0; i < uTBSpawned.nOArcher; i++){
				SimUnit tSU = GenerateNewSimUnit(2, false);
				computerSimUnits.Add(tSU);
				HealthProperties hp = computerSimUnits.Last().healthP;
				computerForcesHealth[tSU.unitCode].Add(hp);
				eUT[tSU.unitCode]++;
			}		
		}

		foreach (GameObject pgo in ComputerAI.Instance.playerUnitsOnField) {
			Unit u = pgo.GetComponent<Unit>() as Unit;
			SimUnit su = new SimUnit();
			su.unitCode = u.unitCode;
			su.healthP = ObjectCopier.Clone(u.getHealthProperties());
			su.attackP = ObjectCopier.Clone(u.getAttackProperties());
			playerSimUnits.Add(su);
			HealthProperties hp = playerSimUnits.Last().healthP;
			playerForcesHealth[u.unitCode].Add(hp);
			pUT[u.unitCode]++;
		}
	
		SimulateBattle(ref computerSimUnits, ref playerSimUnits, ref computerForcesHealth, ref playerForcesHealth);
		SimUnitLists sR = new SimUnitLists ();
		sR.playerSimUnits = playerSimUnits;
		sR.computerSimUnits = computerSimUnits;

		fieldSimResult.SetValue(sR);

		//Debug.Log ("SimResultList set in SimulateBattleOnField");
		//uSimResults.VInit ();
		UnitTable pUR = new UnitTable ();
		foreach (SimUnit su in playerSimUnits) {
			float coeffU = su.healthP.health/su.healthP.healthMax;
			if(su.unitCode == 0)
				pUR.nOSpearman+=coeffU;
			else if(su.unitCode == 1)
				pUR.nOHorseman+=coeffU;
			else if(su.unitCode == 2)
				pUR.nOHorseman+=coeffU;
		}
 		playerUnitResults.SetValue (pUR);

		UnitTable cUR = new UnitTable ();
		foreach (SimUnit su in computerSimUnits) {
			float coeffU = su.healthP.health/su.healthP.healthMax;
			if(su.unitCode == 0)
				cUR.nOSpearman+=coeffU;
			else if(su.unitCode == 1)
				cUR.nOHorseman+=coeffU;
			else if(su.unitCode == 2)
				cUR.nOHorseman+=coeffU;
		}
		computerUnitResults.SetValue (cUR);


		if (computerSimUnits.Count > 0){
			if(!uToBeSpawned.Value.notEmpty()){
				ComputerAI.Instance.newUnitOnField = false;
				ComputerAI.Instance.newPlayerUnitOnField = false;
			}
			//Debug.Log("sim sucess");
			return TaskStatus.Success;
		}else {
			//Debug.Log("sim failure");
			return TaskStatus.Failure;

		}
	}

	private void SimulateBattle(ref List<SimUnit> eSU, ref List<SimUnit> pSU,
	                            ref List<HealthProperties>[] computerForcesHealth, ref List<HealthProperties>[] playerForcesHealth){

		List<GameObject> attackedComputerUnits;
		List<GameObject> attackedPlayerUnits;

		while(eSU.Count != 0 && pSU.Count != 0){
			int noAttackTurns = (eSU.Count > pSU.Count) ? eSU.Count : pSU.Count; 
			for(int i= 0; i < noAttackTurns; i++){
				if (Random.value < 0.5f) {
					if(pSU.Count>i){
						//Debug.Log("> i");
						if(pSU[i].healthP.health > 0){
							//Debug.Log("B Attack");
							UnitAttack(pSU[i].unitCode, pSU[i].attackP, ref computerForcesHealth);
						}
					}
					if(eSU.Count>i){
						//Debug.Log("> i");
						if(eSU[i].healthP.health > 0){
							//Debug.Log("B Attack");
							UnitAttack(eSU[i].unitCode, eSU[i].attackP, ref playerForcesHealth);
						}
					}
				}else {
					if(eSU.Count > i){
						if(eSU[i].healthP.health > 0){
							UnitAttack(eSU[i].unitCode, eSU[i].attackP, ref playerForcesHealth);
						}
					}
					if(pSU.Count > i){
						//Debug.Log("> i");
						if(pSU[i].healthP.health > 0){
							//Debug.Log("B Attack");
							UnitAttack(pSU[i].unitCode, eSU[i].attackP, ref computerForcesHealth);
						}
					}
				}

				List<int> dPList =  new List<int>();

				for(int j = 0; j < pSU.Count; j++){
					SimUnit u = pSU[j];
					if(u.healthP.health<=0)
						dPList.Add(j);
				}

				if(dPList.Count>0){
					foreach(int d in dPList){
						pSU.RemoveAt(d);
					}
				}

				List<int> dEList =  new List<int>();
				for(int j = 0; j < eSU.Count; j++){
					SimUnit u = eSU[j];
					if(u.healthP.health<=0)
						dEList.Add(j);
				}
				if(dEList.Count>0){
					foreach(int d in dEList){
						eSU.RemoveAt(d);
					}
				}

				noAttackTurns = (eSU.Count > pSU.Count) ? eSU.Count : pSU.Count;

			}
		}
	}

	private bool UnitAttack(int attackerType, AttackProperties attackersAttackP, ref List<HealthProperties>[] unitsToBeAttacked){
		bool attackComplete = false;

		for(int i = 1; i > -2; i--){
			int k = (attackerType+3+i)% noUT;
			//Debug.Log("attacker type:" + attackerType + "k: " + k);
			if(unitsToBeAttacked[k].Count >0){
				unitsToBeAttacked[k][0].health -= attackersAttackP.GetDamage(k);//GameConstants.GetAvgDamage(attackerType,k);//add bonus
				attackComplete = true;
				if(unitsToBeAttacked[k][0].health <0)
					unitsToBeAttacked[k].RemoveAt(0);
			}

			if(attackComplete)
				break;
		}

		return attackComplete;
	}
	
}

/*
[System.Serializable]
public class SimUnit //Not a MonoBehaviour!
{
	public int unitCode;
	public AttackProperties attackP;
	public HealthProperties healthP;
	
	public SimUnit(){
		
	}

}
*/

