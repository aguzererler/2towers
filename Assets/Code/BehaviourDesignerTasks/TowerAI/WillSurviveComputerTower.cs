using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class WillSurviveComputerTower : Conditional
{
	public SharedResultBattleSimList comTowerHealthAsim;
	public override TaskStatus OnUpdate()
	{
		if(comTowerHealthAsim.Value.computerTowerHealth >= (GameConstants.criticalTowerHealth * Random.Range(0.9f,1.5f)))
			return TaskStatus.Success;
		else
			return TaskStatus.Failure;
	}
}