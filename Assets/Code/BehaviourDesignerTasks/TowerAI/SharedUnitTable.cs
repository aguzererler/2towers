using UnityEngine;
using BehaviorDesigner.Runtime;
using System.Collections.Generic;

[System.Serializable]
public class UnitTable
{
	public float nOSpearman { get; set;}
	public float nOHorseman { get; set;}
	public float nOArcher { get; set;}

	struct UnitStruct{
		int uc;
		int tier;
	}

	public UnitTable(){

		nOSpearman = 0;
		nOHorseman = 0;
		nOArcher = 0;
	}

	public bool notEmpty(){
		return (nOSpearman != 0 || nOHorseman != 0 || nOArcher != 0);
	}

	public void Init(){
		nOSpearman = 0;
		nOHorseman = 0;
		nOArcher = 0;
	}

	public float GetNOByCode(int uCode){
		switch (uCode) {
		case 0:
			return nOSpearman;
			break;
		case 1:
			return nOHorseman;
			break;
		case 2:
			return nOArcher;
			break;
		default:
			return 0;
			break;
		}

	}

	public void SetNOByCode(int uCode, float n){
		switch (uCode) {
		case 0:
			nOSpearman = n;
			break;
		case 1:
			nOHorseman= n;
			break;
		case 2:
			nOArcher= n;
			break;
		default:
			Debug.Log("no such unit code");
			break;
		}
		
	}

	public void AddByCode(int uCode, float n){
		switch (uCode) {
		case 0:
			nOSpearman += n;
			break;
		case 1:
			nOHorseman += n;
			break;
		case 2:
			nOArcher= +n;
			break;
		default:
			Debug.Log("no such unit code");
			break;
		}
		
	}

	public void SubstractByCode(int uCode, float n){
		switch (uCode) {
		case 0:
			nOSpearman -= n;
			break;
		case 1:
			nOHorseman -= n;
			break;
		case 2:
			nOArcher -= n;
			break;
		default:
			Debug.Log("no such unit code");
			break;
		}
		
	}

	public int GetMostPopulatedUnitType(){
		int uc = -1;
		if (nOSpearman > nOHorseman)
			uc = 0;
		else
			uc = 1;
		if (uc == 0) {
			if(nOArcher > nOSpearman)
				uc = 2;
		}else if(uc == 1){
			if(nOArcher > nOHorseman)
				uc = 2;
		}
		return uc;
	}

	public int GetRandomUnit(){
		if(notEmpty()){
			int u = Random.Range (0, 3);
			while(GetNOByCode(u) == 0){
				u = Random.Range (0, 3);
			}
			SubstractByCode(u,1);
			return u;
		}else{
			return -1;
		}

			
	}

	public int[] ToIntArray(){
		return new int[]{(int)nOSpearman, (int)nOHorseman, (int)nOArcher};
	}

	//computer unit spawn order.
	public List<int> ToUnitCodeList(){
		List<int> l = new List<int>();

		int fNOSpearman = (int)nOSpearman;
		int fNOArcher = (int)nOArcher;
		int fNOHorseman =  (int)nOHorseman;

		while(fNOSpearman != 0 || fNOArcher != 0 || fNOHorseman != 0){
			int r = UnityEngine.Random.Range(0,3);

			if(r == 0 && fNOSpearman != 0){
					l.Add (0);
					fNOSpearman--;
			}else if(r == 1 && fNOHorseman != 0){
					l.Add (1);
					fNOHorseman--;
			}else if(r == 2 && fNOArcher != 0){
					l.Add (2);
					fNOArcher--;
			}
		}
		return l;
	}
}


[System.Serializable]
public class SharedUnitTable : SharedVariable
{
	public UnitTable Value { get { return mValue; } set { mValue = value; } }
	[SerializeField]
	private UnitTable mValue;

	public override object GetValue() { return mValue; }
	public override void SetValue(object value) { mValue = (UnitTable)value; }

	public override string ToString() { return mValue == null ? "null" : mValue.ToString(); }



	public static implicit operator SharedUnitTable(UnitTable value) { var sharedVariable = new SharedUnitTable(); sharedVariable.SetValue(value); return sharedVariable; }
}