using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using System.Collections.Generic;

public class CalculateCounterHorde : Action
{
	//input
	public SharedResultBattleSimList battleSim;
	//output
	public SharedCalculationResults calcRes;

	private SharedInt lastDroppedUnitCode;

	private float[] unitCounterTable;  
	private UnitTable spawnUnitTable;
	private UnitTable cUT;
	private UnitTable pUT;

	public override void OnAwake ()
	{
		unitCounterTable = new float[]{1.4f, 0.8f, 0.8f};
		cUT = new UnitTable ();
		pUT = new UnitTable ();

	}

	public override void OnStart()
	{
		/*
		if(calcRes.Value.cSpawnUT == null)
			calcRes.Value.cSpawnUT = new UnitTable();
		else
			calcRes.Value*/
			spawnUnitTable = new UnitTable();
	}

	public override TaskStatus OnUpdate()
	{
		updateUnitTablesNTowerHealths(battleSim.Value);
		counterHorde ();
		return TaskStatus.Success;
	}

	private void counterHorde ()
	{
		int lDUC = -1;

		if(pUT.notEmpty()){
			int noUnitTypes = GameConstants.noUnitTypes;
			for (int i = 0; i < noUnitTypes; i ++) {
				int j = (i+2)%3;
				int uAdd = Mathf.CeilToInt(pUT.GetNOByCode(i) * unitCounterTable[j]*0.9f);
				spawnUnitTable.AddByCode(j,uAdd);
				}
		}else{
			int uD = cUT.GetMostPopulatedUnitType ();
			//spawnUnitTable.SubstractByCode (uD, 1);
			lDUC = uD; 
		}

		calcRes.Value.pSpawnUT = pUT;
		calcRes.Value.cSpawnUT = spawnUnitTable;
		calcRes.Value.lastDroppedUnitCode = lDUC;
	}

	void updateUnitTablesNTowerHealths (ResultBattleSimList simRes)
	{

		List<SimUnit> computerSimUnits = simRes.computerSimUnits;
		List<SimUnit> playerSimUnits = simRes.playerSimUnits;

		//float playerTowerHealth = simRes.playerTowerHealth;
		//float computerTowerHealth = simRes.computerTowerHealth;
		//float etoa;
		//float battleDuration = simRes.battleDuration;

		pUT.Init();
		foreach (SimUnit su in playerSimUnits) {
			float coeffU = su.healthP.health/su.healthP.healthMax;
			if(su.unitCode == 0)
				pUT.nOSpearman+=coeffU;
			else if(su.unitCode == 1)
				pUT.nOHorseman+=coeffU;
			else if(su.unitCode == 2)
				pUT.nOArcher+=coeffU;
		}
		
		cUT.Init();
		foreach (SimUnit su in computerSimUnits) {
			float coeffU = su.healthP.health/su.healthP.healthMax;
			if(su.unitCode == 0)
				cUT.nOSpearman +=coeffU;
			else if(su.unitCode == 1)
				cUT.nOHorseman +=coeffU;
			else if(su.unitCode == 2)
				cUT.nOArcher +=coeffU;
		}

		calcRes.Value.cSpawnUT = cUT;
		calcRes.Value.pSpawnUT = pUT;
		/*
		calcRes.Value.pTowerHealth = playerTowerHealth;
		calcRes.Value.cTowerHealth = computerTowerHealth;
		calcRes.Value.ETOAPUnits = battleSim.Value.etoaPlayerU;
		calcRes.Value.ETOACUnits = battleSim.Value.etoaComputerU;
		 */

	}
	
}