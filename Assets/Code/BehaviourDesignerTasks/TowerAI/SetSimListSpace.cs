using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class SetSimListSpace : Action
{
	public bool isField;
	public bool isNW;
	public SharedBool isFieldShared;
	public SharedBool isNWShared;

	public override void OnStart()
	{
		isFieldShared.SetValue (isField);	
		isNWShared.SetValue (isNW);
	}

	public override TaskStatus OnUpdate()
	{
		return TaskStatus.Success;
	}
}