using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class WithinFakeAttackDistanceUnit : Conditional
{
	public SharedTransform targetTransform;
	public SharedVector3 fakePointLocal;
	public SharedFloat attackDistance;

	public override TaskStatus OnUpdate()
	{
		Vector3 fakePoint = (Vector3)targetTransform.Value.TransformPoint (fakePointLocal.Value);
		Vector3 dist = transform.position - fakePoint;
		if (dist.magnitude < attackDistance.Value)
			return TaskStatus.Success;
		else{
			return TaskStatus.Failure;
		}
	}
}