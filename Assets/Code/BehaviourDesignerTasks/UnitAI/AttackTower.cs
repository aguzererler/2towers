using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class AttackTower : Action
{
	public override void OnStart()
	{
		Unit au = gameObject.GetComponent<Unit>();
		HealthProperties hp = au.getHealthProperties();
		float dmg = hp.health * GameConstants.unitTowerDamageCoeff;

		au.enemyTower.SendMessage ("TowerDamage", dmg);
		au.isAttackingTower = true;
		au.TakeDamage (100f);

		if (ComputerAI.Instance.playerUnitsOnField.Count == 0) {
			ComputerAI.Instance.newUnitOnField = false;
			ComputerAI.Instance.newPlayerUnitOnField = false;
			ComputerAI.Instance.deadComputerUnit = false;
		}

	}

	public override TaskStatus OnUpdate()
	{

		return TaskStatus.Success;
	}
}