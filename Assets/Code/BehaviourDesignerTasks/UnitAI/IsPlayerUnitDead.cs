﻿using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class IsPlayerUnitDead : Conditional
{
	public override TaskStatus OnUpdate()
	{
		if(ComputerAI.Instance.deadPlayerUnit)
			return TaskStatus.Success;
		else
			return TaskStatus.Failure;
	}
}