﻿using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class WithinAttackDistanceUnit : Conditional
{
	public SharedTransform targetTransform;
	public SharedFloat attackDistance;
	
	public override TaskStatus OnUpdate()
	{
		if(targetTransform.Value != null){
			Vector3 dist = transform.position - targetTransform.Value.position;

			if (dist.sqrMagnitude < attackDistance.Value){//&& gameObject.GetComponent<PolyNavAgent>().polyNav.CheckLOS(transform.position,targetTransform.Value.position))
				return TaskStatus.Success;}
			else{
				return TaskStatus.Failure;
			}
		}else
			return TaskStatus.Failure;
	}
}