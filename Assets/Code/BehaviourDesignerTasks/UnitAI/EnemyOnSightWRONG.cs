using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using System.Collections.Generic;

public class EnemyOnSightWRONG: Conditional
{
	// How wide of an angle the object can see
	public float fieldOfViewAngle;
	// Set the target variable when a target has been found so the subsequent tasks know which object is the target
	public SharedTransform target;
	// Sight range 
	public int sightDistance;
	// A cache of all of the possible targets
	public List<GameObject> possibleTargets;

	public SharedGameObject targetObject;

	public bool isTK2D;
	
	private bool isPlayer = false;
	private bool isEnemy = false;
	private bool isPreTargetGO;

	public SharedBool waitToAttack;

	public override void OnAwake()
	{
		if (Owner.tag.Equals ("Player")) {
			isPlayer = true;
		} else if (Owner.tag.Equals ("Computer")) {
			isEnemy = true;
		}

		isPreTargetGO = false;
	}
	
	public override TaskStatus OnUpdate()
	{
		if (isPlayer) {
			possibleTargets = ComputerAI.Instance.computerUnitsOnField;
		} else if (isEnemy) {
			possibleTargets = ComputerAI.Instance.playerUnitsOnField;
		}
		//Debug.Log ("posTargets: " + possibleTargets.Count);
						// Return success if a target is within sight
		if (targetObject.Value != null) {
			//return TaskStatus.Success;
			//return TaskStatus.Running;
			return TaskStatus.Failure;
		}else{
			foreach (GameObject go in possibleTargets) {
				if(go.transform != null){
					if (withinSight (go.transform, fieldOfViewAngle)) {
					// Set the target so other tasks will know which transform is within sight
					Unit u = go.GetComponent<Unit>();	
						if(u.getNOAttackers() < 3){
							target.Value = go.transform;
							targetObject.Value = go;
							u.AddtoAttackedBy(gameObject);
							Unit aU = gameObject.GetComponent<Unit>();
							aU.attackingTo = go;
							break;
							//return TaskStatus.Success;
							//return TaskStatus.Running;
						}
						waitToAttack = true;
					}
				}
			}

			if(targetObject.Value != null){
				if(isPreTargetGO){
					return TaskStatus.Failure;
				}
				else{
					isPreTargetGO = true;
					return TaskStatus.Success;
				}
			}else{
				if(isPreTargetGO){
					isPreTargetGO = false;
					return TaskStatus.Success;
				}
				else{
					return TaskStatus.Failure;
				}
			}

			//return TaskStatus.Failure;
			//return TaskStatus.Running;
		}
	}
	
	// Returns true if targetTransform is within sight of current transform
	public bool withinSight(Transform targetTransform, float fieldOfViewAngle)
	{
		Vector3 direction = targetTransform.position - transform.position;

		if(isTK2D){
			// An object is within sight if the angle is less than field of view
			//Debug.Log(Vector3.Angle(direction, transform.up) < fieldOfViewAngle);
			return (Vector3.Angle(direction, transform.up) < fieldOfViewAngle && sightDistance > direction.magnitude);

		}else{
			return (Vector3.Angle(direction, transform.forward) < fieldOfViewAngle && sightDistance > direction.magnitude);
		}
	}

	public bool withinDistance(Transform targetTransform)
	{
		Vector3 direction = targetTransform.position - transform.position;
		
		if(isTK2D){
			// An object is within sight if the angle is less than field of view
			//Debug.Log(Vector3.Angle(direction, transform.up) < fieldOfViewAngle);
			return (sightDistance > direction.magnitude);
			
		}else{
			return (sightDistance > direction.magnitude);
		}
	}
}