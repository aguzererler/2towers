using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;

namespace BehaviorDesigner.Runtime.Tasks.Movement.PolyNav
{
	[TaskDescription("Seek the target specified using PolyNav.")]
	[TaskCategory("Movement/PolyNav")]
	[HelpURL("http://www.opsive.com/assets/BehaviorDesigner/Movement/documentation.php?id=3")]
	[TaskIcon("Assets/Behavior Designer Movement/Editor/Icons/{SkinColor}SeekIcon.png")]
	public class DetermineTargetMiniatureSeek : PolyNavSteeringBase
	{
		[Tooltip("The transform that the agent is moving towards")]
		public SharedTransform targetTransform;
		[Tooltip("If target is null then use the target position")]
		public SharedVector3 targetPosition;
	
		// How wide of an angle the object can see
		public float fieldOfViewAngle;

		// Sight range 
		public int sightDistance;
		// A cache of all of the possible targets
		public List<GameObject> possibleTargets;

		// True if the target is a transform
		private bool dynamicTarget;

		public SharedGameObject targetObject;

		public SharedBool waitToAttack;
		
		public SharedBool isMiniature;
		public bool isTK2D;

		public SharedFloat attackingDistance;

		private bool isPlayer = false;
		private bool isEnemy = false;

		private int oscRotate= 0;
		private int oscDir = 1;
		private Unit aU;
		private Dictionary<float, GameObject>[] attackableObjects; 

		private int preOscDir =1;
		private float oscSpeed = 400;
		private float speedBase = 80;

		public override void OnAwake ()
		{
			if (Owner.tag.Equals ("Player")) {
				isPlayer = true;
			} else if (Owner.tag.Equals ("Computer")) {
				isEnemy = true;
			}
			base.OnAwake ();

			attackableObjects = new Dictionary<float, GameObject>[3];
			for(int i = 0; i<3; i++){
				attackableObjects[i] = new Dictionary<float, GameObject>();
			}

			aU = gameObject.GetComponent<Unit>();
		}

		public override void OnStart()
		{
			// the target is dynamic if the target transform is not null and has a valid
			if(transform.localRotation.eulerAngles.z != 0){
				transform.DOLocalRotate(new Vector3(0,0,0),0.4f);
			}
			//dynamicTarget = (targetTransform != null && targetTransform.Value != null);

			if(targetTransform != null && targetTransform.Value != null){
				IUnitBase ub = targetTransform.Value.gameObject.GetComponent<IUnitBase>() as IUnitBase;
				if( ub.isAlive()){
					dynamicTarget = true;
				}else{
					dynamicTarget = false;
					targetObject.Value = null;
					targetTransform.Value = null;
				}

			}else{
				dynamicTarget = false;
			}

			base.OnStart();
		}
		

		// Seek the destination. Return success once the agent has reached the destination.
		// Return running if the agent hasn't reached the destination yet
		public override TaskStatus OnUpdate()
		{

			if (DestinationReached) {
				return TaskStatus.Success;
			}

			if (isPlayer) {
				possibleTargets = ComputerAI.Instance.computerUnitsOnField;
			} else if (isEnemy) {
				possibleTargets = ComputerAI.Instance.playerUnitsOnField;
			}
			//Debug.Log ("posTargets: " + possibleTargets.Count);
			// Return success if a target is within sight

			bool anyAttackableObject = false;

			if (targetObject.Value == null || (targetObject.Value != null )) {//&& !dynamicTarget)) {

				for(int i = 0; i<3; i++){
					attackableObjects[i].Clear();
				}

				foreach (GameObject go in possibleTargets) {
					if(go != null){
						Vector3 direction = go.transform.position - transform.position;
						float dist = direction.sqrMagnitude;
						if (sightDistance > dist && polyNavAgent.polyNav.CheckLOS(transform.position,go.transform.position)) {

							Unit u = go.GetComponent<Unit>();	
							if(u.getNOAttackers() < 3){
								anyAttackableObject = true;

									attackableObjects[u.unitCode].Add(dist+Random.value,go);

							}

						}
					}
				}

				if(anyAttackableObject){
					waitToAttack.Value = true;
					int uC = aU.unitCode;	
					for(int ii = 4; ii > 1 ; ii--){
						if(attackableObjects[(uC+ii)%3].Count > 0){
							List<float> keys;
							keys = attackableObjects[(uC+ii)%3].Keys.ToList();
							keys.Sort();
							//attackableObjects[(uC+ii)%3] = attackableObjects[(uC+ii)%3].OrderBy(k=>k.Key).ToDictionary(k=>k.Key,k=>k.Value);
							//GameObject go = attackableObjects[(uC+ii)%3].Values.ElementAt(0);
							if((aU.attackedBy.Count <= 1 && keys[0] < attackingDistance.Value * 1.2f) || keys[0] < attackingDistance.Value || aU.attackedBy.Count <= 0){

								if(aU.attackingTo != null){
									aU.attackingTo.GetComponent<Unit>().RemoveFromAttackedBy(gameObject);
								}
								GameObject go = attackableObjects[(uC+ii)%3][keys[0]];
								targetObject.Value = go;
								targetTransform.Value = go.transform;
								go.GetComponent<Unit>().AddtoAttackedBy(gameObject);
								aU.attackingTo = go;
							}
							break;
						}

					}
				}else if(aU.attackedBy.Count > 0){
					for(int i = 0; i< aU.attackedBy.Count; i++){
						if(aU.attackedBy[i] != null){
							aU.attackedBy[i].GetComponent<Unit>().AddtoAttackedBy(gameObject);
							GameObject aGO =  aU.attackedBy[0];
							if(aGO!= null){
								aU.attackingTo = aGO;
								targetObject.Value = aGO;
								targetTransform.Value = aGO.transform;
							}
						}
					}
				}

			}

			bool preDynamicTarget = dynamicTarget;
			dynamicTarget = (targetTransform != null && targetTransform.Value != null);
			
			if(preDynamicTarget != dynamicTarget){
				base.OnStart();
			}

			// Update the destination if the target is a transform because that agent could move
			if (dynamicTarget) {
				UpdateDestination();
			}

			if(isMiniature.Value == true){
				
				float newZ = (float) (Mathf.Atan2(polyNavAgent.movingDirection.y,polyNavAgent.movingDirection.x)*Mathf.Rad2Deg);
				int scale = 5;
				float localZ = transform.localRotation.eulerAngles.z;
				
				if(newZ > -90 && newZ < 90){
					transform.localScale = new Vector3(-1,1,1);
					scale = -1;
				}
				else{
					transform.localScale = new Vector3(1,1,1);
				}
				
				if(newZ > 45 && newZ < 135){
					transform.rotation = Quaternion.Euler(new Vector3(0,0,-45*scale));
				}else if(newZ < -45 && newZ > -135){
					transform.rotation = Quaternion.Euler(new Vector3(0,0,45*scale));
				}else
					transform.rotation = Quaternion.Euler(new Vector3(0,0,0));
				
				if(localZ > 10 && localZ <180){
					oscDir = -1;
				}
				else if(localZ < 350 && localZ > 180){
					oscDir = 1;
				}

				if(preOscDir != oscDir){
					float randomOsc = Random.Range(1f,3f);
					oscSpeed = randomOsc * oscDir * 100;
					changeSpeed(randomOsc*speedBase);
					preOscDir = oscDir;
				}

				transform.localRotation = Quaternion.Euler( new Vector3(0,0,localZ+oscSpeed*Time.deltaTime) );
			}

			return TaskStatus.Running;
		}

		// Return targetPosition if targetTransform is null
		protected override Vector2 Target()
		{
			if (dynamicTarget) {
				return targetTransform.Value.position;
			}
			return targetPosition.Value;
		}
		
		// Reset the public variables
		public override void OnReset()
		{
			targetTransform = null;
			targetPosition = Vector3.zero;
		}

		// Returns true if targetTransform is within sight of current transform
		public bool withinSight(Transform targetTransform, float fieldOfViewAngle)
		{
			Vector3 direction = targetTransform.position - transform.position;
			
			if(isTK2D){
				// An object is within sight if the angle is less than field of view
				//Debug.Log(Vector3.Angle(direction, transform.up) < fieldOfViewAngle);
				return (Vector3.Angle(direction, transform.up) < fieldOfViewAngle && sightDistance > direction.magnitude);
				
			}else{
				return (Vector3.Angle(direction, transform.forward) < fieldOfViewAngle && sightDistance > direction.magnitude);
			}
		}
		
		public bool withinDistance(Transform targetTransform)
		{
			Vector3 direction = targetTransform.position - transform.position;
			
			if(isTK2D){
				// An object is within sight if the angle is less than field of view
				//Debug.Log(Vector3.Angle(direction, transform.up) < fieldOfViewAngle);
				return (sightDistance > direction.magnitude);
				
			}else{
				return (sightDistance > direction.magnitude);
			}
		}
	}
}