﻿using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class IsSelfUnitAlive : Conditional
{
	IUnitBase uB;
	
	public override void OnStart ()
	{
		base.OnStart ();
		uB = Owner.GetComponent(typeof(IUnitBase)) as IUnitBase;
	}
	public override TaskStatus OnUpdate()
	{
		if(uB.isAlive())
			return TaskStatus.Success;
		else
			return TaskStatus.Failure;
	}
}