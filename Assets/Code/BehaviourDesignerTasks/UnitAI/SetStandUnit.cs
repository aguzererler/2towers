using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class SetStandUnit : Action
{
	private IUnitBase selfUnit;
	public SharedInt standNo;

	public override void OnAwake()
	{
		selfUnit = Owner.GetComponent(typeof(IUnitBase)) as IUnitBase;
	}

	public override void OnStart()
	{
		
	}

	public override TaskStatus OnUpdate()
	{
		if(standNo.Value == 0){
			//Debug.Log("get normal stand");
			selfUnit.getNormalStand();
		}else if(standNo.Value == 1){
			//Debug.Log("get attack stand");
			selfUnit.getAttackStand();
		}else{
			//Debug.Log("no stand");
		}

		return TaskStatus.Success;
	}
}