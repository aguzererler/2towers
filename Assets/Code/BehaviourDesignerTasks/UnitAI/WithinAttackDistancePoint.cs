using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class WithinAttackDistancePosition: Conditional
{
	public SharedVector3 targetPosition;
	public SharedFloat attackDistance;
	
	public override TaskStatus OnUpdate()
	{
		Vector3 dist = transform.position - targetPosition.Value;
		if (dist.sqrMagnitude < attackDistance.Value)
			return TaskStatus.Success;
		else{
			return TaskStatus.Failure;
		}
	}
}
