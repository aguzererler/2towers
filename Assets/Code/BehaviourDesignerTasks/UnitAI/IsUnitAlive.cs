using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class IsUnitAlive : Conditional
{
	public SharedTransform tt;

	public override TaskStatus OnUpdate()
	{
		if(tt.Value != null){
			IUnitBase uT = tt.Value.gameObject.GetComponent(typeof(IUnitBase)) as IUnitBase;

			if(uT.isAlive())
				return TaskStatus.Success;
			else
				return TaskStatus.Failure;
		}else{
			return TaskStatus.Failure;
		}
	}	
}