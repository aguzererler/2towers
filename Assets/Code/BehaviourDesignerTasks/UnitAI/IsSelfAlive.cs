using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class IsSelfAlive : Conditional
{
	IUnitBase uB;
	public override void OnAwake ()
	{
		base.OnAwake ();
		uB = gameObject.GetComponent(typeof(IUnitBase)) as IUnitBase;
	}
	public override TaskStatus OnUpdate()
	{
		if(uB.isAlive())
			return TaskStatus.Success;
		else
			return TaskStatus.Failure;
	}
}