﻿using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;


public class AttackUnit:Action {

	public SharedTransform targetTransform;
	public SharedAttackProperties attack;

	private IUnitBase selfUnit;
	private IUnitBase tU;

	private GameObject preGO;
	private Unit au;
	private int attackedUnitCode;
	private float damage;

	public override void OnAwake(){
		selfUnit = Owner.GetComponent(typeof(IUnitBase)) as IUnitBase;
	}

	public override TaskStatus OnUpdate(){
		//Debug.Log ("BT attacked");
		if(preGO == null){
			au = targetTransform.Value.gameObject.GetComponent<Unit>();
			attackedUnitCode = au.unitCode;
			damage = attack.Value.GetDamage(attackedUnitCode);
			preGO = targetTransform.Value.gameObject;
		}else{
			if(preGO.GetHashCode() != targetTransform.Value.gameObject.GetHashCode()){
				au = targetTransform.Value.gameObject.GetComponent<Unit>();
				attackedUnitCode = au.unitCode;
				damage = attack.Value.GetDamage(attackedUnitCode);
				preGO = targetTransform.Value.gameObject;
			}
		}

		Vector3 direction = Vector3.Normalize(targetTransform.Value.position - gameObject.transform.position);
		float ang = (float)(Mathf.Atan2(direction.y,direction.x)*Mathf.Rad2Deg);


			if(gameObject.transform.localScale.x >= 0){
				gameObject.transform.eulerAngles = new Vector3 (0.0f,0.0f,ang-180f);
			}else {
				gameObject.transform.eulerAngles = new Vector3 (0.0f,0.0f,ang);
			}


		//selfUnit.GetMovementProp().LookAtTargetX(transform, targetTransform.Value.gameObject);

		selfUnit.playAttackAnimation();
		//Debug.Log ("uCode " + au.unitCode + " " + gameObject.GetHashCode() + " attacking " + targetTransform.Value.gameObject.GetHashCode());
		au.TakeDamage (damage);
		//float damage= new Vector2 (attack.Value.GetDamage (), attack.Value.damageType);
		//Vector2 damageVec= new Vector2 (attack.Value.GetDamage (), attack.Value.damageType);
		//targetTransform.Value.gameObject.SendMessage("TakeDamage", damageVec);
		//selfUnit.Attack (targetTransform.Value.gameObject);
		return TaskStatus.Success;
	}
}
