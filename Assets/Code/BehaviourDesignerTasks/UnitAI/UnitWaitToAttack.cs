using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class UnitWaitToAttack : Action
{
	// The time to wait
	private float waitDuration;
	// The time that the task started to wait.
	private float startTime;
	// Remember the time that the task is paused so the time paused doesn't contribute to the wait time.
	private float pauseTime;

	public SharedBool wTA;
	public SharedFloat wTime;

	public override void OnStart()
	{
		// Remember the start time.
		startTime = Time.time;
		waitDuration = wTime.Value;
	}
	
	public override TaskStatus OnUpdate()
	{
		if (wTA.Value) {
			// The task is done waiting if the time waitDuration has elapsed since the task was started.
			if (startTime + waitDuration < Time.time) {
				wTA.Value = false;
			}
			return TaskStatus.Running;
		}
		return TaskStatus.Failure;
	}
}