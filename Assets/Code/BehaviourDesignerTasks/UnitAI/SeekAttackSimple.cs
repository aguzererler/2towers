using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class SeekAttackSimple : Action
{
	public SharedTransform targetTransform;
	
	public SharedFloat Speed;
	
	private Vector3 direction;
	private Vector3 fakePointLocal;
	public SharedVector3 sFakePointLocal; 
	public SharedFloat attackDistance;
	
	public override void OnStart()
	{
		fakePointLocal = new Vector3 (Random.Range (-20f, 20f), Random.Range (0, 20f), 0);
		sFakePointLocal.Value = fakePointLocal;
	}
	
	public override TaskStatus OnUpdate()
	{	if(targetTransform.Value != null){
			Vector3 fakePoint = (Vector3)targetTransform.Value.TransformPoint (fakePointLocal);
			Vector3 dist = transform.position - fakePoint;
			if (dist.magnitude < attackDistance.Value)
				return TaskStatus.Success;
			else{
				MoveTarget(fakePoint);
				return TaskStatus.Running;
			}
		}else{
			return TaskStatus.Failure;
		}
	
	}

	public void SetDirectionOnPoint(Transform u, Vector3 p){
		Vector3 tDirection = Vector3.Normalize( p - u.position);
		direction = tDirection;
	}
	
	public void LookAtTargetPoint(Transform u, Vector3 p){
		SetDirectionOnPoint (u, p);
		float oldDirectionAngleZ = u.eulerAngles.z; 
		float nextDirectionAngleZ = (float) (Mathf.Atan2(direction.y,direction.x)*Mathf.Rad2Deg)-90f;
		u.eulerAngles = new Vector3 (0.0f,0.0f,Mathf.LerpAngle(oldDirectionAngleZ,nextDirectionAngleZ, 2 * Time.deltaTime));
		//u.eulerAngles = new Vector3 (0.0f,0.0f,nextDirectionAngleZ);
	}
	
	public void MoveTarget(Vector3 p){

		LookAtTargetPoint (transform, p);	
		transform.position = transform.position + direction * Time.deltaTime * Speed.Value;
	}
}