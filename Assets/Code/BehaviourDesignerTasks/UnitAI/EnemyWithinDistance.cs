using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using System.Collections.Generic;
using System.Linq;

public class EnemyWithinDistance : Conditional
{

	// Sight range 
	public SharedFloat sharedSightDistance;
	private float sightDistance;
	// A cache of all of the possible targets
	public List<GameObject> possibleTargets;
	
	private bool isPlayer = false;
	private bool isEnemy = false;

	private Unit aU;
	private Dictionary<float, GameObject>[] attackableObjects; 
	bool preEnemywithinDistance;
	bool isFirstRun;
	
	public override void OnAwake()
	{
		if (Owner.tag.Equals ("Player")) {
			isPlayer = true;
		} else if (Owner.tag.Equals ("Computer")) {
			isEnemy = true;
		}
		preEnemywithinDistance = false;
		attackableObjects = new Dictionary<float, GameObject>[3];

		for(int i = 0; i<3; i++){
			attackableObjects[i] = new Dictionary<float, GameObject>();
		}
		
		aU = gameObject.GetComponent<Unit>();
		sightDistance = sharedSightDistance.Value;
		isFirstRun = true;
	}
	
	public override TaskStatus OnUpdate()
	{
		if (isPlayer) {
			possibleTargets = ComputerAI.Instance.computerUnitsOnField;
		} else if (isEnemy) {
			possibleTargets = ComputerAI.Instance.playerUnitsOnField;
		}

		bool anyAttackableObject = false;

		/*if (targetObject.Value != null) {
			return TaskStatus.Success;
		}else{*/

			for(int i = 0; i<3; i++){
				attackableObjects[i].Clear();
			}
			
			foreach (GameObject go in possibleTargets) {
				if(go != null){
					Vector3 direction = go.transform.position - transform.position;
					float dist = direction.magnitude;
					if (dist<sightDistance) {

					Unit u = go.GetComponent<Unit>();

						if(u.getNOAttackers() < 3){
							attackableObjects[u.unitCode].Add(dist,go);
						anyAttackableObject = true;
						}
					}
				}
			}

		if(isFirstRun){
			isFirstRun = false;
			preEnemywithinDistance = anyAttackableObject;
			return TaskStatus.Failure;
			//return TaskStatus.Success;
		}else{
			if(anyAttackableObject == preEnemywithinDistance){
				preEnemywithinDistance = anyAttackableObject;
				return TaskStatus.Success;
				//return TaskStatus.Failure;
			}else{
				preEnemywithinDistance = anyAttackableObject;
				return TaskStatus.Failure;
				//return TaskStatus.Success;
			}
		}

	}
		
}