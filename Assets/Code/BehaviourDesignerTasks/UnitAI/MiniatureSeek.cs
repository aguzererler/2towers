using UnityEngine;

namespace BehaviorDesigner.Runtime.Tasks.Movement.PolyNav
{
    [TaskDescription("Seek the target specified using PolyNav.")]
    [TaskCategory("Movement/PolyNav")]
    [HelpURL("http://www.opsive.com/assets/BehaviorDesigner/Movement/documentation.php?id=3")]
    [TaskIcon("Assets/Behavior Designer Movement/Editor/Icons/{SkinColor}SeekIcon.png")]
    public class MiniatureSeek : PolyNavSteeringBase
    {
        [Tooltip("The transform that the agent is moving towards")]
        public SharedTransform targetTransform;
        [Tooltip("If target is null then use the target position")]
        public SharedVector3 targetPosition;

		/*
		// How wide of an angle the object can see
		public float fieldOfViewAngle;
		// Set the target variable when a target has been found so the subsequent tasks know which object is the target
		public SharedTransform target;
		// Sight range 
		public int sightDistance;
		// A cache of all of the possible targets
		public List<GameObject> possibleTargets;
		*/
        // True if the target is a transform
        private bool dynamicTarget;


		public SharedBool isMiniature;
		public bool isTK2D;
		private bool isPlayer = false;
		private bool isEnemy = false;

        public override void OnStart()
        {
            // the target is dynamic if the target transform is not null and has a valid
            dynamicTarget = (targetTransform != null && targetTransform.Value != null);
            base.OnStart();
        }

		int oscRotate= 0;
		int oscDir = 1;
        // Seek the destination. Return success once the agent has reached the destination.
        // Return running if the agent hasn't reached the destination yet
        public override TaskStatus OnUpdate()
        {
			bool preDynamicTarget = dynamicTarget;
			dynamicTarget = (targetTransform != null && targetTransform.Value != null);

			if(preDynamicTarget != dynamicTarget){
				base.OnStart();
			}

            if (DestinationReached) {
                return TaskStatus.Success;
            }

			if(isMiniature.Value == true){

				float newZ = (float) (Mathf.Atan2(polyNavAgent.movingDirection.y,polyNavAgent.movingDirection.x)*Mathf.Rad2Deg);
				int scale = 1;
				float localZ = transform.localRotation.eulerAngles.z;

				if(newZ > -90 && newZ < 90){
					transform.localScale = new Vector3(-1,1,1);
					scale = -1;
				}
				else{
					transform.localScale = new Vector3(1,1,1);
				}

				if(newZ > 45 && newZ < 135){
					transform.localRotation = Quaternion.Euler(new Vector3(0,0,-45*scale));
				}else if(newZ < -45 && newZ > -135){
					transform.localRotation = Quaternion.Euler(new Vector3(0,0,45*scale));
				}else
					transform.localRotation = Quaternion.Euler(new Vector3(0,0,0));

				if(localZ > 10 && localZ <180)
					oscDir = -1;
				else if(localZ < 350 && localZ > 180)
					oscDir = 1;

				float osc = Random.Range(1,4) * oscDir;
				transform.localRotation = Quaternion.Euler( new Vector3(0,0,localZ+osc) );
			}

            // Update the destination if the target is a transform because that agent could move
            if (dynamicTarget) {
                UpdateDestination();
            }

            return TaskStatus.Running;
        }

        // Return targetPosition if targetTransform is null
        protected override Vector2 Target()
        {
            if (dynamicTarget) {
                return targetTransform.Value.position;
            }
            return targetPosition.Value;
        }

        // Reset the public variables
        public override void OnReset()
        {
            targetTransform = null;
            targetPosition = Vector3.zero;
        }
    }
}