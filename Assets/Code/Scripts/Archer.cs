﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Archer : Unit {
	const int COMPUTER = 2;
	const int  PLAYER = 1;

	void Awake(){
		
		typeUnit = "Archer";
		unitCode = 2;	
		movement.isMoving = true;
		movement.speed = 12;
		movement.turnSpeed = 1;
	}

	public override void setUnitSprites(int typeTower){
		//Debug.Log("archer SetSprites");
		uSprite = gameObject.GetComponent<tk2dSprite> ();
		tk2dSprite ts;

		if(typeTower == COMPUTER){
			uSprite.SetSprite(GameConstants.unitSpriteNamesComputer[unitCode]);
			ts = sprites[0].GetComponent<tk2dSprite> ();
			ts.SetSprite(GameConstants.accSpriteNamesComputer[unitCode]);
		}else if(typeTower == PLAYER){
			uSprite.SetSprite(GameConstants.unitSpriteNamesPlayer[unitCode]);
			ts = sprites[0].GetComponent<tk2dSprite> ();
			ts.SetSprite(GameConstants.accSpriteNamesPlayer[unitCode]);
		}else{
			Debug.Log("wrong tower type");
		}
	}

	public override void playAttackAnimation(){
		//Debug.Log("play atack ani. arch");
		sprites[1].transform.localRotation = Quaternion.Euler(new Vector3(20,0,0));
		sprites[1].transform.DOLocalMove(new Vector3 (-64f,40f,-1.1f),0.3f, false).OnComplete(AttackAniComplete);

	}

	public void AttackAniComplete(){
		sprites[1].transform.localPosition = new Vector3(-16,-4,-1.1f);
	}

	public override void getAttackStand(){
		sprites[0].transform.DOLocalRotate(new Vector3(0,0,-40),0.2f).OnComplete(AttackStandComplete);
		sprites[1].transform.DOLocalRotate(new Vector3(0,0,-40),0.2f);
		//Debug.Log("arch get attack attack stand.");
	}
	
	public override void getNormalStand(){
		if(currentStand == 1){
		sprites[0].transform.DOLocalRotate(new Vector3(0,0,0),0.2f).OnComplete(NormalStandComplete);
		sprites[1].transform.DOLocalRotate(new Vector3(0,0,0),0.2f);
		//Debug.Log("arch get normal stand.");
		}
	}

	void NormalStandComplete(){
		currentStand = 0;
	}

	void AttackStandComplete(){
		currentStand = 1;
	}
}
