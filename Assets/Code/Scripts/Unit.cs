using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using BehaviorDesigner.Runtime;


public class Unit : MonoBehaviour, IUnitBase {

	public string typeUnit;
	public int unitCode { get; set;}
	public int tier {get;set;}
	
	public GameObject target;
	public GameObject enemyTower{get;set;}
	public float distanceToEnemyTower;
	public bool isTargetRedZone = false;

	public Vector3 targetTowerPosition;
	public List<GameObject> possibleTargetObjects;

	public List<GameObject> sprites;

	protected HealthProperties health;
	public MovementProperties movement;
	protected AttackProperties attack;

	public List<GameObject> attackedBy;
	public GameObject attackingTo;

	public BehaviorTree behaviourTree;

	public int currentWayPoint;

	public bool isMiniatureStyle = true;

	public GameObject coinAni;
	public tk2dSprite uSprite;
	bool isCoinAniPlaying = false;
	public bool isAttackingTower;

	public Unit(){
		/*

		movement = new MovementProperties ();
		attack = new AttackProperties ();
		health = GameConstants.GetUnitHealthProperties(unitCode,tier);
		*/


		attackedBy = new List<GameObject> ();


	}
	
	// Use this for initialization

	public virtual void Start () {

		/*
		uSprite = GetComponent<tk2dSprite>();
		Color c = uSprite.color;
		c.a = 1.0f;
		uSprite.color =  c;
		*/
		gameObject.transform.SetPositionZ(0);
		isCoinAniPlaying = false;
		isAttackingTower = false;
		if(enemyTower != null){
			Init(enemyTower, 0);
		}
	}

	void OnEnable(){

	}

	public void Init( GameObject go, int tr){

		health = GameConstants.GetUnitHealthProperties(unitCode,tier);
		movement = new MovementProperties ();
		attack = GameConstants.GetUnitAttackProperties (unitCode, tier);

		behaviourTree = GetComponent<BehaviorTree> ();
		targetTowerPosition = go.transform.position;
		possibleTargetObjects = go.GetComponent<Tower> ().unitsOnField;
		enemyTower = go;

		SharedVector3 targetTowerP = (SharedVector3)behaviourTree.GetVariable ("TargetTowerPosition");
		targetTowerP.Value = targetTowerPosition;

		SharedAttackProperties attackProperties = (SharedAttackProperties)behaviourTree.GetVariable ("AttackProperties");
		attackProperties.Value = attack;

		SharedHealthProperties healthProperties = (SharedHealthProperties)behaviourTree.GetVariable ("HealthProperties");
		healthProperties.Value = health;

		tier = tr;

		//movement.LookAtTargetPoint (transform, targetTowerPosition);
	}

	//Update is called once per frame
	public void Update () {



	}

	public void FixedUpdate(){
		//regenrateHealth();
	}

	public void Move (){

	}

	public void TakeDamage (float dmg){
		//Debug.Log(this.GetHashCode + );
		health.health -= dmg;

		float alpha = health.health / health.healthMax * 0.6f + 0.4f;
		Color uc = uSprite.color;
		uc.a = alpha;
		uSprite.color = uc;
		foreach(GameObject so in sprites){
			tk2dSprite s = so.GetComponent<tk2dSprite>();
			Color c = s.color;
			c.a = alpha;
			s.color = c;
		}

		if (health.health < 0 && !isCoinAniPlaying ) {


			if(attackingTo != null)
				attackingTo.SendMessage("RemoveFromAttackedBy", gameObject);


			if(gameObject.tag.Equals("Computer")){
				gameObject.transform.SetPositionZ(3);
				gameObject.transform.localScale = new Vector3(1,1,1);
				ComputerAI.Instance.computerUnitsOnField.Remove(gameObject);
				ComputerAI.Instance.computerSpawnedUnits.Remove(gameObject);
				ComputerAI.Instance.deadComputerUnit = true;
				/*
				coinAni.SendMessage("PlayCoinAni");
				isCoinAniPlaying = true;


				Color c = uSprite.color;
				c.a = 0f;
				uSprite.color =  c;
				*/

				int coinBoost;
				if(ComputerAI.Instance.isBoostMode)
					coinBoost = 2;
				else
					coinBoost =1;
					

				if(GameConstants.unitCoinPTable[unitCode][1]*coinBoost*1.5 > UnityEngine.Random.value && !isAttackingTower){
					int gc = Mathf.RoundToInt( GameConstants.unitCoinPTable[unitCode][0]*coinBoost);
					coinAni.SendMessage("SetCoinAniText", gc);
					coinAni.SendMessage("PlayCoinAni");
					ComputerAI.Instance.playerCoins += gc;
					ComputerAI.Instance.PlayerCoinsText.text = ComputerAI.Instance.playerCoins.ToString();
					isCoinAniPlaying = true;
				}else{
					DestroyUnit();
				}


			
			}
			else if(gameObject.tag.Equals("Player")){
				ComputerAI.Instance.playerUnitsOnField.Remove(gameObject);
				ComputerAI.Instance.playerSpawnedUnits.Remove(gameObject);
				ComputerAI.Instance.deadPlayerUnit= true;
				DestroyUnit();
			
			}


		}
	}

	public void DieByTower(){
		ComputerAI.Instance.computerUnitsOnField.Remove(gameObject);
		ComputerAI.Instance.computerSpawnedUnits.Remove(gameObject);
		ComputerAI.Instance.deadComputerUnit = true;
		DestroyUnit();
	}

	IEnumerator DestroyUnitTimer(int t){
		yield return new WaitForSeconds(t);
		DestroyUnit();
	}

	public void DestroyUnit(){
		gameObject.Recycle ();
	}

	public bool isAlive(){
		if (health.health < 0) {
			return false;
		} else
			return true;
	}

	public void Attack (GameObject go){
		/*
		IUnitBase u = go.GetComponent(typeof(IUnitBase)) as IUnitBase;
		u.Damage (attack.GetDamage(), attack.damageType);
		Debug.Log("unit attacked");
		*/
	}
	
	public void OnTriggerEnter(Collider other){

		if(other.tag.Equals("WayPoints")){
			currentWayPoint = int.Parse(other.name);
			//Debug.Log("@wp " + currentWayPoint);
		}

		//Debug.Log("unit trigger enter, other tag " + other.tag);
	}

	public HealthProperties getHealthProperties(){
		return health;
	}

	public AttackProperties getAttackProperties(){
		return attack;
	}
	
	public void AddtoAttackedBy(GameObject go){
		if (!attackedBy.Contains (go)) {
			attackedBy.Add(go);		
		}
	}

	public void RemoveFromAttackedBy(GameObject go){
		if (attackedBy.Contains (go)) {
			attackedBy.Remove(go);		
		}
	}

	protected int currentStand = 0;

	public int getNOAttackers(){
		return attackedBy.Count;
	}

	public virtual void playAttackAnimation(){
		//Debug.Log("play attack animation.");
	}

	public virtual void getAttackStand(){
		//Debug.Log("get attack attack stand.");
	}

	public virtual void getNormalStand(){
		//Debug.Log("get normal stand.");
	}

	public virtual void setUnitSprites(int typeTower){
		//Debug.Log("set sprites.");
	}

	public MovementProperties GetMovementProp(){
		return movement;
	}

	public void setTier(int t){
		tier = t;
	}

	public int getTier(){
		return tier;
	}

}

[Serializable]
public class HealthProperties //Not a MonoBehaviour!
{
	public float regenerationRate;
	public float healthMax;
	public float health{get; set;}
	public float damageReduction {get; set;}
	public int tier {get; set;}

	public HealthProperties(){


	}
	
	public HealthProperties(float mHealth, float dmgRed){
		healthMax = mHealth;
		health = healthMax;
		damageReduction = dmgRed;
	}

}

[Serializable]
public class DamageData{
	public float baseDamage {get; set;}
	public float maxDamage {get; set;}
	public float damageBonusPerTier {get; set;}
	public List<float> damageRateAgainstUnit {get; set;}

	public DamageData(){

	}

	public DamageData(float baseDmg, float maxDmg, float tierBonus, List<float> rateAgainst){
		baseDamage = baseDmg;
		maxDamage = maxDmg;
		damageBonusPerTier = tierBonus;
		damageRateAgainstUnit = rateAgainst;
	}
}

[Serializable]
public class AttackProperties //Not a MonoBehaviour!
{
	//damage = baseDamage + random(baseDamage,maxDamage)
	public DamageData damageData;
	public float bonusDamage;
	public float tier;
	public float damageType;
	//number of hits per second
	public float hitRate;
	public float attackDistance {get; set;}

	float mxDamage;
	float mnDamage;

	public AttackProperties(){

	}

	public AttackProperties(int unitCode, int t){
		tier = t;
		damageData = GameConstants.unitDamageDataTable[unitCode];
		mxDamage = damageData.maxDamage  + damageData.damageBonusPerTier * tier;
		mnDamage = damageData.baseDamage + damageData.damageBonusPerTier * tier/1.4f;
	}

	public AttackProperties(DamageData dd, int t){
		tier = t;
		damageData = dd;
		mxDamage = damageData.maxDamage  + damageData.damageBonusPerTier * tier;
		mnDamage = damageData.baseDamage + damageData.damageBonusPerTier * tier/1.4f;
	}
	
	public float GetDamage(int bau){
		float damage = UnityEngine.Random.Range (mnDamage,mxDamage) * damageData.damageRateAgainstUnit[bau]; 
		return damage; 
	}
}

[Serializable]
public class MovementProperties //Not a MonoBehaviour!
{
	public float speedCoeff = 0.2f;
	public bool isMoving ;
	public float speed;
	//default provided
	public float turnSpeed = 1; 
	public Vector3 direction;
	public GameObject enemyTower;
	public GameObject target;
	int rotaionSmoothingValue = 2;
	
	public List<Vector3> waypoints;
	
	public MovementProperties(){
		
	}
	
	public void SetDirectionOnTarget(Transform u, GameObject t){
		Vector3 tDirection = Vector3.Normalize( t.transform.position - u.position);
		direction = tDirection;
	}
	
	public void Look(Transform u){
		u.transform.eulerAngles = new Vector3 (0.0f,0.0f,(float)Math.Atan2(direction.y,direction.x)*Mathf.Rad2Deg);
	}
	
	public void LookAtTarget(Transform u, GameObject t){
		SetDirectionOnTarget (u, t);
		u.eulerAngles = new Vector3 (0.0f,0.0f,(float)(Math.Atan2(direction.y,direction.x)*Mathf.Rad2Deg)-90f);
	}

	public void LookAtTargetX(Transform u, GameObject t){
		SetDirectionOnTarget (u, t);
		if(u.localScale.x >= 0)
			u.eulerAngles = new Vector3 (0.0f,0.0f,(float)(Math.Atan2(direction.y,direction.x)*Mathf.Rad2Deg)-180f);
		else {
			u.eulerAngles = new Vector3 (0.0f,0.0f,(float)(Math.Atan2(direction.y,direction.x)*Mathf.Rad2Deg));
		}
	}

	public void MoveForward(Transform u){
		if (target == null) {
			LookAtTarget (u, enemyTower);
		} else {
			LookAtTarget (u, target);	
		}
		u.position = u.position + direction * speed*speedCoeff; 
	}
	
	public void LookAtTargetObject(Transform u, GameObject t){
		SetDirectionOnTargetObject (u, t);
		//objectTransform.Rotate (0, 0, Mathf.LerpAngle(0, angle, Time.deltaTime * smoothingValue) );
		float oldDirectionAngleZ = u.eulerAngles.z; 
		float nextDirectionAngleZ = (float) (Mathf.Atan2(direction.y,direction.x)*Mathf.Rad2Deg)-90f;//+u.localScale.x*90f;
		//u.eulerAngles = new Vector3 (0.0f,0.0f,Mathf.LerpAngle(oldDirectionAngleZ,nextDirectionAngleZ, rotaionSmoothingValue * Time.deltaTime));
		u.eulerAngles = new Vector3 (0.0f,0.0f,nextDirectionAngleZ);
	}
	

	public void LookAtTargetPoint(Transform u, Vector3 t){
		SetDirectionOnTargetPoint (u, t);
		float oldDirectionAngleZ = u.eulerAngles.z; 
		float nextDirectionAngleZ = (float) (Mathf.Atan2(direction.y,direction.x)*Mathf.Rad2Deg)-90f;
		//u.eulerAngles = new Vector3 (0.0f,0.0f,Mathf.LerpAngle(oldDirectionAngleZ,nextDirectionAngleZ, rotaionSmoothingValue * Time.deltaTime));
		u.eulerAngles = new Vector3 (0.0f,0.0f,nextDirectionAngleZ);
	}
	
	public void SetDirectionOnTargetObject(Transform u, GameObject t){
		Vector3 nDirection = Vector3.Normalize( t.transform.position - u.position);
		Vector3 smoothedDirection = new Vector3 (Mathf.Lerp (nDirection.x, direction.x, Time.deltaTime * rotaionSmoothingValue),
		                                         Mathf.Lerp (nDirection.y, direction.y, Time.deltaTime * rotaionSmoothingValue), 0);
		direction = nDirection;
	}
	
	public void SetDirectionOnTargetPoint(Transform u, Vector3 t){
		Vector3 nDirection = Vector3.Normalize( t - u.position);
		Vector3 smoothedDirection = new Vector3 (Mathf.Lerp (nDirection.x, direction.x, Time.deltaTime * rotaionSmoothingValue),
		                                         Mathf.Lerp (nDirection.y, direction.y, Time.deltaTime * rotaionSmoothingValue), 0);
		direction = smoothedDirection;
		//direction = nDirection;
	}
}