﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class SpawnerAniUnit : MonoBehaviour {
	
	// Use this for initialization
	int oscDir = 1;
	public Vector3 targetPosition;
	public Vector3 homePosition;
	
	
	void OnAwake(){

	}
	void Start () {

	}

	void OnEnable(){
		//Vector3 nDirection = Vector3.Normalize( transform.position - targetPosition);
		//float nextDirectionAngleZ = (float) (Mathf.Atan2(nDirection.y,nDirection.x)*Mathf.Rad2Deg)-90f;
		//transform.eulerAngles = new Vector3 (0.0f,0.0f,nextDirectionAngleZ);
		
		gameObject.transform.DOLocalMove(targetPosition,1f).OnComplete(OnReachTarget);
	}
	
	// Update is called once per frame
	void Update () {
		
		float localZ = transform.localRotation.eulerAngles.z;
		
		if(localZ > 10 && localZ <180)
			oscDir = -1;
		else if(localZ < 350 && localZ > 180)
			oscDir = 1;
		
		float osc = Random.Range(1,4) * oscDir * 60 * Time.deltaTime;
		transform.localRotation = Quaternion.Euler( new Vector3(0,0,localZ+osc) );
	}
	
	void OnReachTarget(){
		transform.transform.localPosition = homePosition;
		gameObject.SetActive(false);
	}
}