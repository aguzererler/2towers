﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class UnitCoinAni:MonoBehaviour {
	public Vector3 originalLocalPos;
	public tk2dTextMesh unitCoinAniText;

	void Awake(){
		originalLocalPos = gameObject.transform.localPosition;
		unitCoinAniText = gameObject.GetComponentInChildren<tk2dTextMesh>();
	}

	public void PlayCoinAni(){
		gameObject.transform.eulerAngles = new Vector3(0,0,0);
		gameObject.transform.SetLocalPositionZ(-2);
		gameObject.transform.DOLocalMoveY(originalLocalPos.y + 30f, 1.5f);
		gameObject.transform.DOScale(new Vector3(1.4f,1.4f,1.4f), 1.7f).OnComplete(CoinAniCompleted);
	}

	void CoinAniCompleted(){
		gameObject.transform.localPosition = originalLocalPos;
		gameObject.transform.localScale = new Vector3(0.1f,0.1f,0.1f);
		gameObject.SendMessageUpwards("DestroyUnit");
	}

	public void SetCoinAniText(int c){

		unitCoinAniText.text ="+" + c.ToString();
	}
}
