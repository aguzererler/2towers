﻿using UnityEngine;
using System.Collections;

public class RandomAudio : MonoBehaviour
{
	public AudioClip[] soundtrack;
	AudioSource sAudio;

	void Awake(){
		sAudio = GetComponent<AudioSource>();
	}
	
	// Use this for initialization
	void Start ()
	{
		if (!sAudio.playOnAwake)
		{
			sAudio.clip = soundtrack[Random.Range(0, soundtrack.Length)];
			sAudio.Play();
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (!GetComponent<AudioSource>().isPlaying)
		{
			sAudio.clip = soundtrack[Random.Range(0, soundtrack.Length)];
			sAudio.Play();
		}
	}
}