using UnityEngine;
using System.Collections;
using System;

public class Stables : PlayerSpawner {

	public Stables() {


	}
	void Start(){

		spawnerType = 1;

		spawnDuration = GameConstants.unitSpawnDurationTable [spawnerType];
		flagText.text = spawnDuration + " s.";
		spawnerSpriteName = GameConstants.getSpawnerSpriteName (spawnerType);
		base.Init();
	}
}
