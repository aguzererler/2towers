﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class UIMenuAnimation : MonoBehaviour {

	public tk2dTextMesh twText;

	void OnEnable(){


	}
	
	// Use this for initialization
	void Start () {
		gameObject.transform.DOPunchScale(new Vector3(0.3f,0.3f,0),0.8f, 4, 0.3f);
		if(twText != null)
			twText.text = "TOWER "+ ComputerAI.Instance.currentTowerLevel;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
