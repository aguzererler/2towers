using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using System.Linq;
using System;

public class PlayerSpawner : MonoBehaviour {

	bool isSaving = false;

	public bool isSpawning;
	public int tier{get;set;}
	protected float spawnStartTime;

	//necessary time for spawning
	protected float spawnDuration;

	//type of the spawner
	protected String spawnerSpriteName;
	protected GameObject spawnUnit;
	protected int spawnerType;
	
	public GameObject computerTower;
	public Tower tower;

	private bool isFlagHigh = true;
	public GameObject flag;
	protected tk2dTextMesh flagText;
	public GameObject aniUnit;

	private Vector3 flagOriginalPos;
	private int flagDownCounter;

	public GameObject sKing;
	
	void Awake(){
		//Debug.Log ("player spawner awake.");
		tk2dUIItem tUI= this.GetComponent<tk2dUIItem> ();
		tUI.OnDown += StartSpawning;
		tUI.OnRelease += EndSpawning;
		flagText = flag.GetComponentInChildren<tk2dTextMesh>();


	}

	protected void Init(){
		//Debug.Log ("player spawner init.");
		tier = 0;
		flagOriginalPos = flag.transform.position;
		flagDownCounter = 1;
		LoadSpawnerSprite ();
		spawnUnit = Resources.Load<GameObject>(GameConstants.getSpawnUnitPrefabName(spawnerType)) as GameObject;
	}



	public void Reset(){
		flagDownCounter = 3;
		if(!isFlagHigh){
			isFlagHigh = true;
			flag.transform.position = flagOriginalPos;
			flag.transform.localScale = new Vector3(1f,1f,1f);
		}
		tier = 0;
		UpdateTierText();
	}

	void UpdateTierText(){

	}

	void StartSpawning(){
		int isTouchValid;
		spawnStartTime = Time.timeSinceLevelLoad;

		if (!tower.isOtherSpawning ()) {
			isSpawning = true;
			isTouchValid = 1;
			sKing.SetActive(true);
			//Debug.Log ("spawning started " + Time.timeSinceLevelLoad);
		} else {
			isTouchValid = 0;
			//Debug.Log ("spawning cant start, other is spawning");
		}
		ComputerAI.Instance.RecordTouch (new PlayerDataTouch(spawnerType, spawnStartTime, 0, isTouchValid, spawnDuration, -1, -999));
	}

	void EndSpawning(){
		int isTouchValid;
		float realeaseTime = Time.timeSinceLevelLoad;
		float touchDuration = Time.timeSinceLevelLoad - spawnStartTime;
		float deviation  = touchDuration - spawnDuration;

		if(isSpawning){
			isTouchValid = 1;
			sKing.SetActive(false);
		
			String cTime = Time.time.ToString();

			if (touchDuration > spawnDuration){

				tower.Spawn (spawnUnit, tier);
				aniUnit.SetActive(true);
				if(isFlagHigh && flagDownCounter == 0){
					flag.transform.DOMoveY(flag.transform.position.y-115,0.45f).OnComplete(FlagDown);
					flag.transform.DOScale(new Vector3(0.2f,0.2f,0.1f),0.4f);
				}else{
					flagDownCounter -= 1; 
				}
			}else
			{
				ComputerAI.Instance.ShowSmallMessage("My Lord, be more patient! \n Please, wait a bit more.");
				//Debug.Log ("you need to wait more");
			}
				isSpawning = false;
		}else
		{
				isTouchValid = 0;
				//Debug.Log ("spawning cant start, other is spawning");
		}

		ComputerAI.Instance.RecordTouch (new PlayerDataTouch(spawnerType,realeaseTime, 1, isTouchValid, spawnDuration, touchDuration, deviation ));
	}

	bool isOthersSpawning(){
		return false;
	}

	void DownTest(){
		//Debug.Log ("Spawner down");
	}

	void LoadSpawnerSprite (){
		tk2dSprite ts;
		ts = GetComponentInChildren<tk2dSprite> ();
		ts.SetSprite (GameConstants.getSpawnerSpriteName (spawnerType));

	}

	void FlagDown(){
		flag.transform.SetPositionZ(1);
		isFlagHigh = false;
	}

}

[Serializable]
public class PlayerDataTouch{

	public int spawnerCode;
	public float touchTime;
	public int touchType;
	public int isValid;
	public float targetedDuration;
	public float touchDuration;
	public float sDeviation;

	public PlayerDataTouch(int sCode, float tTime, int tType, int iV, float tedD, float tD, float sD){
		spawnerCode = sCode;
		touchTime = tTime;
		touchType = tType;
		isValid = iV;
		targetedDuration = tedD;
		touchDuration = tD;
		sDeviation = sD;
	}

	public string CastToString(){
		string sTD, sDev, del;
		del = ";";
		if (touchDuration == -1) {
			sTD = "NaN";
			sDev = "NaN";
		}
		else{
			sTD = touchDuration.ToString();
			sDev = sDeviation.ToString();
		}

		string r = touchTime.ToString () + del + touchType + del + isValid + del + targetedDuration + del + sTD;
		return r;
	}
	
}