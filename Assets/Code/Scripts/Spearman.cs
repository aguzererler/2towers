﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Spearman : Unit
{
	const int COMPUTER = 2;
	const int  PLAYER = 1;

	void Awake(){

		typeUnit = "Spearman";
		unitCode = 0;

		movement.isMoving = true;
		movement.speed = 12;
		movement.turnSpeed = 1;

	}

	public override void setUnitSprites(int typeTower){

		uSprite = gameObject.GetComponent<tk2dSprite> ();
		tk2dSprite ts;
		if(typeTower == COMPUTER){
			uSprite.SetSprite(GameConstants.unitSpriteNamesComputer[unitCode]);
			ts = sprites[0].GetComponent<tk2dSprite> ();
			ts.SetSprite(GameConstants.accSpriteNamesComputer[unitCode]);
		}else if(typeTower == PLAYER){
			uSprite.SetSprite(GameConstants.unitSpriteNamesPlayer[unitCode]);
			ts = sprites[0].GetComponent<tk2dSprite> ();
			ts.SetSprite(GameConstants.accSpriteNamesPlayer[unitCode]);
		}else{
			//Debug.Log("wrong tower type");
		}
	}

	public override void playAttackAnimation(){
		//Debug.Log("play atack ani. spearman");
		sprites[1].transform.DOLocalMoveX(10f,0.3f, false).SetLoops(2, LoopType.Yoyo);
	}
	
	public override void getAttackStand(){
		sprites[1].transform.DOLocalRotate(new Vector3(0,0,55),0.2f);
		//Debug.Log("sp get attack attack stand.");
	}
	
	public override void getNormalStand(){
		sprites[1].transform.DOLocalMove(new Vector3(-8.5f,1.5f,-0.5f),0.2f);
		sprites[1].transform.DOLocalRotate(new Vector3(0,0,0),0.2f);
		//Debug.Log("sp get normal stand.");
	}
}
