﻿using UnityEngine;
using System.Collections;

public class FakeStableSpawner : MonoBehaviour {

	private Tower sTower;

	void Start () {
		sTower = GetComponent<Tower> ();
	}
	
	// Update is called once per frame
	void Update() {

		if (Input.GetKeyDown ("z")) {
			GameObject spawnUnit = Resources.Load<GameObject>(GameConstants.getSpawnUnitPrefabName(0)) as GameObject;
			sTower.Spawn(spawnUnit, ComputerAI.Instance.GetTierPlayerSpawner(0));
		}else if (Input.GetKeyDown ("x")) {
			GameObject spawnUnit = Resources.Load<GameObject>(GameConstants.getSpawnUnitPrefabName(1)) as GameObject;
			sTower.Spawn(spawnUnit, ComputerAI.Instance.GetTierPlayerSpawner(1));
		}else if(Input.GetKeyDown ("c")){
			GameObject spawnUnit = Resources.Load<GameObject>(GameConstants.getSpawnUnitPrefabName(2)) as GameObject;
			sTower.Spawn(spawnUnit, ComputerAI.Instance.GetTierPlayerSpawner(2));
		}else if (Input.GetKeyDown ("space")) {
			//print ("a key was pressed");
			sTower.ToggleGates();
		}



		
	}
}
