using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using Statistics;
using Parse;
using System.Threading.Tasks;
using UnityEngine.Analytics;

public class ComputerAI : MonoBehaviour {

	//Singleton variables.
	protected static ComputerAI instance;
	public static ComputerAI instanceRef;

	//Game & Field parameters.
	public Tower computerTower;
	public Tower playerTower;

	public TimeKeeper timeKeeper { get; set; }

	public List<GameObject> computerUnitsOnField;
	public List<GameObject> computerUnitsInOutQue;

	public List<GameObject> playerUnitsOnField;
	public List<GameObject> playerUnitsOnQue;

	public List<GameObject>[] playerUnitStack;
	public List<GameObject> computerSpawnedUnits;

	public List<GameObject> playerEstimatedUnitsInCastle;
	public List<GameObject> playerSpawnedUnits;
	public bool playerUnitsInComputersRedZone{ get; set;}
	public bool computerUnitsInPlayersRedZone{ get; set;}

	//Data collection 
	protected String touchDataFilePath;
	protected String aiDataFilePath;
	private String delimiter;
	private String playerID;
	private StreamWriter tSW;
	private StreamWriter aiSW;
	private bool isTouchDataSaving;

	public float closestComputerUnitToPlayerTw { get; set;}
	public float closestPlayerUnitToComputerTw { get; set;}

	//pivot points.
	public List<DistancePivotPoint> dPivotPoints;

	//Behaviour Tree Events.
	public bool newUnitOnField { get; set; }
	public bool newComputerUnitOnField { get; set; }
	public bool newPlayerUnitOnField { get; set; }

	public bool deadComputerUnit { get; set; }
	public bool deadPlayerUnit { get; set; }

	//Parameters.
	public float minTimePointsToAttack { get; set;}
	


	//AI parameters calculation
	public float computerEstimationErrorUnit { get; set;}
	public float baseComputerEstimationErrorUnit { get; set;}
	public float coLevelComputerEstimationError { get; set;}

	public float computerSpawnDeviationFromPlayer { get; set;}
	public float maxComputerSpawnDeviationFromPlayer { get; set;}	//= 2f;
	private float minComputerSpawnDeviationFromPlayer { get; set;}	//= 0.6f;

	public float lvlIncMinComputerSpawnDeviation {get;set;}
	public float lvlIncMaxComputerSpawnDeviation {get;set;}
	public float lvlIncBaseComputerSpawnDeviation {get;set;}
	public float baseComputerSpawnDeviation {get;set;}

	private float computerDeviationSlopeCo { get; set;}	//= 0.75f;
	private float thresholdSlopeTH { get; set;}	//= 0.05f;
	private float thresholdSlopeTL { get; set;}	//= 0.02f;
	private float slopeTLDenominator { get; set;}	//= 0.03f;

	public float randomBaseComputerDeviationFromPlayer;

	public bool isAIDataSaving;
	private int playerDeviationPrecision = 81;

	private double playerSlopeTowerHealth { get; set;}//;
	private double playerSlopeTimeLoss{ get; set;}//;
	private int playerDeviationTrackingWindowSize { get; set;}	//= 10;
	private int deltaTowerHealthTrackingWindowSize { get; set;}	//= 10;
	
	public float aiDataUpdateInterval { get; set;}	//= 10f;
	public int moreBoostTimePoints { get; set;}
	public float moreBoostComputerSpawnDeviation { get; set;}




	//AI variables
	protected List<String> playerTimeData;
	public float avgTimeLossBetweenSpawns;
	private float lastSpawnEndTime;
	private float newSpawnStartTime;
	private int timeLossBetweenSpawnsWindowSize;
	private List<float> timeLossBetweenSpawnList;
	private float[][] deviationMatrix;
	private List<float>[] deviationMatrixEntries;
	private List<PlayerStatusData> playerStatusDataList;

	public int[] tierPlayerSpawners;
	public int[] tierComputerSpawners;

	public int currentTowerLevel;
	public float baseTowerHealth;
	public float levelIncTowerHealth;

	public PopUpMessage smallMessage;
	public PopUpMessage bigMessage;

	public tk2dTextMesh towerLevelText;
	public bool isBoostMode;
	

	//
	public int playerCoins;
	public tk2dTextMesh PlayerCoinsText;

	private string aiDataString;
	private string tDataString;
	

	public static ComputerAI Instance
	{
		get
		{
			if(instance == null)
			{
				instance = (ComputerAI) FindObjectOfType(typeof(ComputerAI));
				
				if (instance == null)
				{
					Debug.LogError("An instance of " + typeof(ComputerAI) + 
					               " is needed in the scene, but there is none.");
				}
			}
			return instance;
		}
	}
	

	void Awake(){
		Debug.Log("CompAI Awake");


		if (instanceRef == null) {
			instanceRef = this;
			//DontDestroyOnLoad (this);
		} else {
			DestroyImmediate(gameObject);
		}


	}
	void Start(){
		Time.timeScale = 1;
		Debug.Log("CompAI Start");
		playerUnitsInComputersRedZone = false;
		computerUnitsInPlayersRedZone = false;
		
		closestComputerUnitToPlayerTw = 1500f;
		closestPlayerUnitToComputerTw = 1500f;
		
		timeKeeper = new TimeKeeper ();
		
		newUnitOnField = false;
		newComputerUnitOnField = false;
		newPlayerUnitOnField = false;
		
		timeLossBetweenSpawnList = new List<float>();
		avgTimeLossBetweenSpawns = 0;
		lastSpawnEndTime = 0;
		newSpawnStartTime = 0;
		timeLossBetweenSpawnsWindowSize = 30;
		
		minTimePointsToAttack = 30f;
		
		tierPlayerSpawners = new int[3]{0,0,0};
		tierComputerSpawners = new int[3]{0,0,0};
		
		isTouchDataSaving = GameConstants.compAIData.SaveTouchData;
		isAIDataSaving = GameConstants.compAIData.SaveAIData;
		delimiter = ";";  
		isBoostMode = false;




		String directoryPath = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)+Path.DirectorySeparatorChar+"2Towers"+Path.DirectorySeparatorChar+"PlayersData";
		if(!Directory.Exists(directoryPath))
			Directory.CreateDirectory(directoryPath);
		if(isAIDataSaving){


			aiDataFilePath = 
				String.Format (directoryPath+Path.DirectorySeparatorChar+"ais{0:yyyy-MM-dd_hh-mm}.txt", DateTime.Now);
		
			int n = 0;
			while(System.IO.File.Exists(aiDataFilePath)){
				aiDataFilePath = 
					String.Format (directoryPath+Path.DirectorySeparatorChar+"ais{0:yyyy-MM-dd_hh-mm}-"+n.ToString()+".txt", DateTime.Now);
				n++;
			}

			string titles = "SubjectID;UpdateTime;ComputerTowerLevel;PlayerTH;ComputerTH;DeltaTH;SlopeDeltaTH;"+
				"SpearmanTimeLoss;ArcherTimeLoss;HorsemanTimeLoss;SlopeTimeLossAvg;ComputerSpawnDeviation;PreComputerSpawnDeviation";
			playerID = GameConstants.playerID;
			aiDataString += titles; 
			aiSW = new StreamWriter (aiDataFilePath);
			aiSW.WriteLine(titles);
			Debug.Log (aiDataFilePath);
		}


		if(isTouchDataSaving){

			touchDataFilePath = String.Format (directoryPath+Path.DirectorySeparatorChar+"ts{0:yyyy-MM-dd_hh-mm}.txt", DateTime.Now);

			int n = 0;
			while(System.IO.File.Exists(touchDataFilePath)){
				touchDataFilePath = 
					String.Format (directoryPath+Path.DirectorySeparatorChar+"ts{0:yyyy-MM-dd_hh-mm}-"+n.ToString()+".txt", DateTime.Now);
				n++;
			}
			
			string titles = "SubjectID;TouchTime;TouchType;IsValid;TargetedTouchDuration;TouchDuration;ComputerTowerLevel;PlayerTowerHealth;ComputerTowerHealth;" +
					"PlayerUnitsStack-Spearman;PlayerUnitsStack-Horseman;PlayerUnitsStack-Archer;" +
					"PlayerUnitsOnField-Spearman;PlayerUnitsOnField-Horseman;PlayerUnitsOnField-Archer;" +
					"ComputerUnitsOnField-Spearman;ComputerUnitsOnField-Horseman;ComputerUnitsOnField-Archer;" +
					"MapPoint0-NoPlayerUnits;MapPoint0-NoComputerUnits;" +
					"MapPoint1-NoPlayerUnits;MapPoint1-NoComputerUnits;" +
					"MapPoint2-NoPlayerUnits;MapPoint2-NoComputerUnits;" +
					"MapPoint3-NoPlayerUnits;MapPoint3-NoComputerUnits;" +
					"MapPoint4-NoPlayerUnits;MapPoint4-NoComputerUnits;" +
					"MapPoint5-NoPlayerUnits;MapPoint5-NoComputerUnits;" +
					"MapPoint6-NoPlayerUnits;MapPoint6-NoComputerUnits;" +
					"MapPoint7-NoPlayerUnits;MapPoint7-NoComputerUnits";
			playerID = GameConstants.playerID;



			tSW = new StreamWriter (touchDataFilePath);
			tSW.WriteLine(titles);
			tDataString += titles;
			Debug.Log (touchDataFilePath);
		}
		

		
		deviationMatrix = new float[GameConstants.noUnitTypes][]; 
		deviationMatrixEntries = new List<float>[GameConstants.noUnitTypes];
		
		for(int i = 0; i < GameConstants.noUnitTypes; i++){
			deviationMatrix[i] = new float[playerDeviationPrecision];
			for(int j = 30; j < 51; j++){
				deviationMatrix[i][j] = 1f;
			}
			deviationMatrixEntries[i] = new List<float>();
		}
		
		
		playerStatusDataList = new List<PlayerStatusData>();

		playerDeviationTrackingWindowSize = GameConstants.compAIData.playerDeviationTrackingWindowSize;
		deltaTowerHealthTrackingWindowSize = GameConstants.compAIData.deltaTowerHealthTrackingWindowSize;
		maxComputerSpawnDeviationFromPlayer = GameConstants.compAIData.maxComputerSpawnDeviation;
		minComputerSpawnDeviationFromPlayer = GameConstants.compAIData.minComputerSpawnDeviation;
		computerDeviationSlopeCo = GameConstants.compAIData.slopeCoComputerSpawnDeviation;
		thresholdSlopeTH = GameConstants.compAIData.threshSlopeTH;
		thresholdSlopeTL = GameConstants.compAIData.threshSlopeTL;
		slopeTLDenominator = GameConstants.compAIData.slopeTLDenominator;
		aiDataUpdateInterval = GameConstants.compAIData.aiDataUpdateInterval;

		lvlIncMinComputerSpawnDeviation = GameConstants.compAIData.lvlIncMinComputerSpawnDeviation;
		lvlIncMaxComputerSpawnDeviation = GameConstants.compAIData.lvlIncMaxComputerSpawnDeviation;
		lvlIncBaseComputerSpawnDeviation = GameConstants.compAIData.lvlIncBaseComputerSpawnDeviation;
		baseComputerSpawnDeviation = GameConstants.compAIData.baseComputerSpawnDeviation;

		baseComputerEstimationErrorUnit = GameConstants.compAIData.baseComputerEstimationError;

		randomBaseComputerDeviationFromPlayer = 0.2f;

		isAIDataSaving = GameConstants.compAIData.SaveAIData;
		isTouchDataSaving = GameConstants.compAIData.SaveTouchData;

		moreBoostTimePoints = GameConstants.compAIData.moreBoostTimePoints;
		moreBoostComputerSpawnDeviation = GameConstants.compAIData.moreBoostComputerSpawnDeviation;
	

		currentTowerLevel = 1;

		computerEstimationErrorUnit = baseComputerEstimationErrorUnit;
		computerSpawnDeviationFromPlayer = baseComputerSpawnDeviation;

		UpdateCurrentLevelText();
		StartCoroutine(UpdateComputerError());
		GenerateDistancePivotPoints ();

		//getplayer coins
		playerCoins = 0;
		PlayerCoinsText.text = playerCoins.ToString();

	}
	public void ComputerTowerSpawn(int[] counterHorde){
		//Debug.Log ("ComputerAI units to counter: " + counterHorde [0]);
		for(int i = 0; i < counterHorde.Length; i++ ){
			for(int j = 0; j < counterHorde[i]; j++ ){
				GameObject spawnUnit;
				spawnUnit = Resources.Load<GameObject>(GameConstants.getSpawnUnitPrefabName(i)) as GameObject;

				if(spawnUnit != null)
					computerTower.Spawn (spawnUnit, GetTierComputerSpawner(i));
			}
		}
		if(!computerTower.areGatesOpen)
			computerTower.OpenGates ();
	}

	public void ComputerTowerSpawnSingleUnit(int unitCode){
		//Debug.Log ("ComputerAI units to counter: " + counterHorde [0]);

		GameObject spawnUnit;
		spawnUnit = Resources.Load<GameObject>(GameConstants.getSpawnUnitPrefabName(unitCode)) as GameObject;
				
		if(spawnUnit != null){
			computerTower.Spawn (spawnUnit, GetTierComputerSpawner(unitCode));
		}

		if(!computerTower.areGatesOpen){
			computerTower.OpenGates ();
		}
	}

	public int[] EstimatePlayerUnitsInCastle (){
		int nUT = GameConstants.noUnitTypes;
		float err = 1f - computerEstimationErrorUnit;
		int[] sPUC = new int[nUT];


		for(int i = 0; i< nUT; i++){
			sPUC[i] = playerUnitStack[i].Count;
		}

		foreach (GameObject pgo in playerUnitsOnQue) {
			Unit u = pgo.GetComponent<Unit>();
			sPUC[u.unitCode]++;
		}

		for(int i = 0; i< nUT; i++){
			sPUC[i] = (int)(sPUC[i] *  UnityEngine.Random.Range(computerEstimationErrorUnit,1f+err)) ;
		}

		return sPUC;

	}

	public void FixedUpdate(){
		if(playerUnitsOnField.Count != 0 )
			UpdateWayPoints ();
	}

	void LateUpdate(){
		newUnitOnField = false;
		newPlayerUnitOnField = false;
		deadComputerUnit = false;
		deadPlayerUnit = false;
	}

	public void TowerDefeated(Tower t){
		if(t.CompareTag("Player")){

//			Analytics.CustomEvent("GameOver", new Dictionary<string, object>
//			{
//				{ "Time", Time.timeSinceLevelLoad },
//				{ "PlayerTowerHealth", GetPlayerTowerHealth() },
//				{ "AvgTimeLoss", avgTimeLossBetweenSpawns },
//				{"PlayerSpawnDeviation", GetPlayerCurrentSpawnDeviation()},
//				{"ComputerSpawnDeviation", computerSpawnDeviationFromPlayer},
//				{"CompTowerLevel", currentTowerLevel}
//			});

			Dictionary<string, string> GameOverAnalytics = new Dictionary<string, string>() {
				// What level are you playing?
				{ "Time", Time.timeSinceLevelLoad.ToString() },
				{ "PlayerTowerHealth", GetPlayerTowerHealth().ToString() },
				{ "AvgTimeLoss", avgTimeLossBetweenSpawns.ToString() },
				{"PlayerSpawnDeviation", GetPlayerCurrentSpawnDeviation().ToString()},
				{"ComputerSpawnDeviation", computerSpawnDeviationFromPlayer.ToString()},
				{"CompTowerLevel", currentTowerLevel.ToString()}
			};
			// Send the dimensions to Parse along with the 'LostLife' event
			
			ParseAnalytics.TrackEventAsync("GameOver", GameOverAnalytics);
			//StateManager.Instance.SwitchMainMenuState();
			GameObject.Find("UI").SendMessage("OnGameOver");
			Debug.Log("Game ended swtiech state");

		}else if(t.CompareTag("Computer")){
			//Play defeat annimation
			NextTower();
		}
	}
	
	private void NextTower(){
		foreach ( GameObject u in playerUnitsOnField){
			playerSpawnedUnits.Remove(u);
			//playerUnitsOnField.Remove(u);
			u.Recycle();
		}

		foreach ( GameObject u in computerUnitsOnField){
			computerSpawnedUnits.Remove(u);
			//computerUnitsOnField.Remove(u);
			u.Recycle();
		}
		playerUnitsOnField.Clear();
		computerUnitsOnField.Clear();
		computerUnitsInOutQue.Clear();
		playerStatusDataList.Clear();
		computerUnitsInPlayersRedZone = false;
		timeKeeper.ResetBudget();

		//computerUnitsOnField;
		int nTowerLevel = currentTowerLevel+1;

//		Analytics.CustomEvent("LevelUp", new Dictionary<string, object>
//		{
//			{ "Time", Time.timeSinceLevelLoad },
//			{ "PlayerTowerHealth", GetPlayerTowerHealth() },
//			{ "AvgTimeLoss", avgTimeLossBetweenSpawns },
//			{ "PlayerSpawnDeviation", GetPlayerCurrentSpawnDeviation()},
//			{ "ComputerSpawnDeviation", computerSpawnDeviationFromPlayer},
//			{ "CompTowerLevel", currentTowerLevel +1}
//		});

		Dictionary<string, string> LevelUpAnalytics = new Dictionary<string, string>() {
			// What level are you playing?
			{ "Time", Time.timeSinceLevelLoad.ToString() },
			{ "PlayerTowerHealth", GetPlayerTowerHealth().ToString() },
			{ "AvgTimeLoss", avgTimeLossBetweenSpawns.ToString() },
			{ "PlayerSpawnDeviation", GetPlayerCurrentSpawnDeviation().ToString()},
			{ "ComputerSpawnDeviation", computerSpawnDeviationFromPlayer.ToString()},
			{ "CompTowerLevel", (currentTowerLevel +1).ToString()}
		};
		// Send the dimensions to Parse along with the 'LostLife' event
		
		ParseAnalytics.TrackEventAsync("LevelUp", LevelUpAnalytics);

		ShowBigMessage("Long live the King! \n You have defeated Tower " + currentTowerLevel + ". \n Relax and be ready for Tower " +  nTowerLevel + ".");
		StartCoroutine(WaitForNextTower(5));
		PlayerButtonsSetActive(false);

		currentTowerLevel = nTowerLevel;

		minComputerSpawnDeviationFromPlayer += lvlIncMinComputerSpawnDeviation;
		maxComputerSpawnDeviationFromPlayer += lvlIncMaxComputerSpawnDeviation;
		computerEstimationErrorUnit += coLevelComputerEstimationError;
		computerSpawnDeviationFromPlayer += lvlIncBaseComputerSpawnDeviation;
	}

	public float GetComputerCurrentSpawnDeviation(int sUC){
		float pD = GetPlayerCurrentSpawnDeviation(sUC);
		float cCompDev = 0;
		float pDB = pD - 1;

		cCompDev = pDB * computerSpawnDeviationFromPlayer + 1 +  UnityEngine.Random.Range(-1*randomBaseComputerDeviationFromPlayer,randomBaseComputerDeviationFromPlayer);

		if(cCompDev > maxComputerSpawnDeviationFromPlayer){
			cCompDev = maxComputerSpawnDeviationFromPlayer;
		}else {
			if(cCompDev < minComputerSpawnDeviationFromPlayer)
				cCompDev = minComputerSpawnDeviationFromPlayer;
		}
		return cCompDev;
	}

	public void UpdateWayPoints(){
		float cPUTCT = 0;
		foreach(DistancePivotPoint wp in dPivotPoints){
			wp.Init();
		}
		foreach (GameObject go in playerUnitsOnField) 
		{

			PolyNavAgent pna = go.GetComponent<PolyNavAgent>();

			Unit u  = go.GetComponent<Unit>();
			dPivotPoints[u.currentWayPoint].playerUnitsCodes[u.unitCode]++;
			if(cPUTCT > u.currentWayPoint)
				cPUTCT = u.currentWayPoint;

			if(u.currentWayPoint == 6){
				playerUnitsInComputersRedZone= true;
				u.isTargetRedZone = true;
			}
			closestComputerUnitToPlayerTw = 240*(7-cPUTCT);
		}		
	
		foreach (GameObject go in computerUnitsOnField) 
		{
			float cCUTPT = 8;
			Unit u  = go.GetComponent<Unit>();
			dPivotPoints[7 - u.currentWayPoint].computerUnitsCodes[u.unitCode]++;
			
			if(u.currentWayPoint == 0){
				computerUnitsInPlayersRedZone  = true;
				u.isTargetRedZone = true;
			}

			if(cCUTPT < u.currentWayPoint)
				cCUTPT = u.currentWayPoint;

			closestComputerUnitToPlayerTw = 240*cCUTPT;
		}
	}
	
	void GenerateDistancePivotPoints (){
		dPivotPoints = new List<DistancePivotPoint> ();

		for (int i = 0; i < 8; i++) {
			dPivotPoints.Add( new DistancePivotPoint());		
		}

		UpdateWayPoints ();
	}

	public float GetComputerTowerHealth(){
		return computerTower.towerHealth;
	}

	public float GetPlayerTowerHealth(){
		return playerTower.towerHealth;
	}

	public void ShowSmallMessage(string t){
		smallMessage.ShowMessage(t,2f);
	}

	public void ShowBigMessage(string t){
		bigMessage.ShowMessage(t,5f);
	}

	//buraya prop list.
	int playerStatusDataWindowSize = 25;

	public void RecordTouch(PlayerDataTouch pT){
		string dataEntry;
		int[] pFU = SeperateUnitsList (playerUnitsOnField);
		int[] pQU = SeperateUnitsList (playerUnitsOnQue);
		int[] cFU = SeperateUnitsList (computerUnitsOnField);

		for (int i = 0; i < pFU.Length; i++) {
			pFU[i] += playerUnitStack[i].Count + pQU[i];	
		}
		/*
		"SubjectID;TouchTime;TouchType;IsValid;TargetedTouchDuration;TouchDuration;ComputerTowerLevel;PlayerTowerHealth;ComputerTowerHealth;" +
			"PlayerUnitsStack-Spearman;PlayerUnitsStack-Horseman;PlayerUnitsStack-Archer;" +
				"PlayerUnitsOnField-Spearman;PlayerUnitsOnField-Horseman;PlayerUnitsOnField-Archer;" +
				"ComputerUnitsOnField-Spearman;ComputerUnitsOnField-Horseman;ComputerUnitsOnField-Archer;" +
				"MapPoint0-NoPlayerUnits;MapPoint0-NoComputerUnits;" +
				"MapPoint1-NoPlayerUnits;MapPoint1-NoComputerUnits;" +
				"MapPoint2-NoPlayerUnits;MapPoint2-NoComputerUnits;" +
				"MapPoint3-NoPlayerUnits;MapPoint3-NoComputerUnits;" +
				"MapPoint4-NoPlayerUnits;MapPoint4-NoComputerUnits;" +
				"MapPoint5-NoPlayerUnits;MapPoint5-NoComputerUnits;" +
				"MapPoint6-NoPlayerUnits;MapPoint6-NoComputerUnits;" +
				"MapPoint7-NoPlayerUnits;MapPoint7-NoComputerUnits";
		*/
		dataEntry =  playerID.ToString() + delimiter + pT.CastToString() + delimiter + currentTowerLevel +  delimiter +
				GetPlayerTowerHealth() + delimiter + GetComputerTowerHealth() + delimiter + 
				(playerUnitStack[0].Count+pQU[0]).ToString() + delimiter + (playerUnitStack[1].Count+pQU[1]).ToString() + delimiter + (playerUnitStack[2].Count+pQU[2]).ToString() + delimiter +
				pFU[0] + delimiter + pFU[1] + delimiter + pFU[2] + delimiter + 
				cFU[0] + delimiter + cFU[1] + delimiter + cFU[2] + delimiter + 
				dPivotPoints[0].GetPlayerUnitNumber() + delimiter + dPivotPoints[0].GetComputerUnitNumber() + delimiter +
				dPivotPoints[1].GetPlayerUnitNumber() + delimiter + dPivotPoints[1].GetComputerUnitNumber() + delimiter + 
				dPivotPoints[2].GetPlayerUnitNumber() + delimiter + dPivotPoints[2].GetComputerUnitNumber() + delimiter + 	
				dPivotPoints[3].GetPlayerUnitNumber() + delimiter + dPivotPoints[3].GetComputerUnitNumber() + delimiter + 
				dPivotPoints[4].GetPlayerUnitNumber() + delimiter + dPivotPoints[4].GetComputerUnitNumber() + delimiter +// + playerun
				dPivotPoints[5].GetPlayerUnitNumber() + delimiter + dPivotPoints[5].GetComputerUnitNumber() + delimiter +
				dPivotPoints[6].GetPlayerUnitNumber() + delimiter + dPivotPoints[6].GetComputerUnitNumber() + delimiter +
				dPivotPoints[7].GetPlayerUnitNumber() + delimiter + dPivotPoints[7].GetComputerUnitNumber();

		Dictionary<string, string> touchDataAnalytics = new Dictionary<string, string>() {
			// What level are you playing?
			{ "TouchData", dataEntry }
		};
		// Send the dimensions to Parse along with the 'LostLife' event
		
		ParseAnalytics.TrackEventAsync("TouchData", touchDataAnalytics);


		if(isTouchDataSaving){
			/*
			"SubjectID;TouchTime;TouchType;IsValid;TargetedTouchDuration;TouchDuration;PlayerTowerHealth;ComputerTowerHealth;" +
					"PlayerUnitsStack-Spearman;" +  "PlayerUnitsStack-Horseman;PlayerUnitsStack-Archer;" +
					"PlayerUnits-Spearman;" +  "PlayerUnits-Horseman;PlayerUnits-Archer;" +
					"ComputerUnitsOnField-Spearman;ComputerUnitsOnField-Horseman;ComputerUnitsOnField-Archer;" +

					"MapPoint0-NoPlayerUnits;MapPoint0-NoComputerUnits;MapPoint1-NoPlayerUnits;MapPoint1-NoComputerUnits" +
					"MapPoint2-NoPlayerUnits;MapPoint2-NoComputerUnits;MapPoint3-NoPlayerUnits;MapPoint3-NoComputerUnits" +
					"MapPoint4-NoPlayerUnits;MapPoint4-NoComputerUnits;MapPoint5-NoPlayerUnits;MapPoint5-NoComputerUnits" +
					"MapPoint6-NoPlayerUnits;MapPoint6-NoComputerUnits;MapPoint7-NoPlayerUnits;MapPoint7-NoComputerUnits";
			*/

				
			tSW.WriteLine (dataEntry);
			tDataString += dataEntry;
			tSW.Flush ();
		}
		if (pT.touchType == 1 && pT.isValid == 1) {
			lastSpawnEndTime = pT.touchTime;
			int sCode = pT.spawnerCode;
			float dev = pT.touchDuration/pT.targetedDuration;
			float wastedTime = pT.touchDuration > pT.targetedDuration ? (pT.touchDuration - pT.targetedDuration) : pT.touchDuration;
	
			if(dev > 2)
				dev = 2;
			dev = (float) Math.Round(dev * 40f);
			deviationMatrixEntries[sCode].Add(dev);


			if(deviationMatrixEntries[sCode].Count > playerDeviationTrackingWindowSize){
				int devTBRemoved = (int)deviationMatrixEntries[sCode][0];

				deviationMatrix[sCode][devTBRemoved] -= 3;
				if(devTBRemoved + 1 < playerDeviationPrecision)
					deviationMatrix[sCode][devTBRemoved+1] -= 1f;
				if(devTBRemoved - 1 >= 0)
					deviationMatrix[sCode][devTBRemoved-1] -= 1f;

				deviationMatrixEntries[sCode].RemoveAt(0);

			}
			int iDev = (int)dev;
			deviationMatrix[sCode][iDev] += 3f;
			if(dev + 1 < playerDeviationPrecision)
				deviationMatrix[sCode][iDev+1] += 1f;
			if(dev - 1 >= 0)
				deviationMatrix[sCode][iDev-1] += 1f;

			playerStatusDataList.Add(new PlayerStatusData
			                         (Time.timeSinceLevelLoad, (GetPlayerTowerHealth()- GetComputerTowerHealth()), (avgTimeLossBetweenSpawns + wastedTime)));


			if(playerStatusDataList.Count > playerStatusDataWindowSize){
				playerStatusDataList.RemoveAt(0);
			}
			
		}else if(pT.touchType == 0 && pT.isValid == 1){
			newSpawnStartTime = pT.touchTime;
			float timeLoss = newSpawnStartTime - lastSpawnEndTime;
			timeLossBetweenSpawnList.Add(timeLoss);
			if(timeLossBetweenSpawnList.Count > timeLossBetweenSpawnsWindowSize ){
				timeLossBetweenSpawnList.RemoveAt(0);
			}

			float sumTimeLoss =  0;

			foreach(float s in timeLossBetweenSpawnList){
				sumTimeLoss += s;
			}

			avgTimeLossBetweenSpawns = sumTimeLoss/timeLossBetweenSpawnList.Count;
		}

	}

	public float GetPlayerCurrentSpawnDeviation(int sUC){
		float devInd = (float)RandomFromDistribution.RandomChoiceFollowingDistribution(new List<float>(deviationMatrix[sUC]));
		int mid = (playerDeviationPrecision-1)/2; 
		return (devInd - mid) / mid + 1f;
	}

	public float GetMinPlayerSpawnDeviation(int sUC){
		List<float> playerSpawnDeviations = new List<float>();


		for (int i = 0; i < GameConstants.noUnitTypes; i++) {
			float devInd = (float)RandomFromDistribution.RandomChoiceFollowingDistribution (new List<float> (deviationMatrix [sUC]));
			int mid = (playerDeviationPrecision - 1) / 2; 
			float pDev = ((devInd - mid) / mid) + 1f;
			playerSpawnDeviations.Add(pDev);

		}

		return playerSpawnDeviations[UnityEngine.Random.Range (0, 3)];


	}

	public float GetPlayerCurrentSpawnDeviation(){
		int mid = (playerDeviationPrecision-1)/2; 
		float devInd = 0;
		for(int i = 0; i < 3; i++){

			devInd += (float)RandomFromDistribution.RandomChoiceFollowingDistribution(new List<float>(deviationMatrix[i]));

		}
		devInd = devInd/3;

		return (devInd - mid) / mid + 1f;
	}


	public int[] SeperateUnitsList(List<GameObject> uList){
		int[] uA = new int[3]{0,0,0};

		foreach(GameObject go in uList){
			Unit u = go.GetComponent<Unit>() as Unit;
			uA[u.unitCode]++;
		}

		return uA;
	}

	int midStateChangeCounter = 0;
	IEnumerator UpdateComputerError ()
	{
		yield return new WaitForSeconds(aiDataUpdateInterval);

		if(playerStatusDataList.Count > 2){

			int N = 1;
			double[] yTH = new double[playerStatusDataList.Count - 1];
			double[] yTL = new double[playerStatusDataList.Count - 1];
			double[,] x = new double[N+1, playerStatusDataList.Count  - 1];
			double[] w = new double[playerStatusDataList.Count  - 1];

			for (int i = 0; i < playerStatusDataList.Count -1; i++)
			{
				if (playerStatusDataList.Count != null)
				{
					x[0, i] = 1;
					double xx = playerStatusDataList[i].time;
					double term = xx;

					for (int j = 1; j <= N; j++)
					{
						x[j, i] = term;
						term *= xx;
					}

					yTH[i] = playerStatusDataList[i].deltaTowerHealths;
					yTL[i] = playerStatusDataList[i].timeLoss;
					w[i] = 1f;
				}

			}


			LinearRegression lRegTH = new LinearRegression();
			lRegTH.Regress(yTH, x, w);

			LinearRegression lRegTL = new LinearRegression();
			lRegTL.Regress(yTL, x, w);

			double slopeTH;
			double slopeTL;

			if(lRegTH.C == null)
				slopeTH = 0;
			else
				slopeTH = lRegTH.C[1];

			if(lRegTL.C == null)
				slopeTL = 0;
			else
				slopeTL = lRegTL.C[1];

			playerSlopeTowerHealth = slopeTH;
			playerSlopeTimeLoss = slopeTL;

			float errInc = 0;
			float deltaTH = GetPlayerTowerHealth()-GetComputerTowerHealth();
		
			if(deltaTH >= 0){
				//Debug.Log("DeltaTwHealth > 0 " );

				if(slopeTH > thresholdSlopeTH){
					//Debug.Log("SlopeDeltaTwHealth > 0 slopeTH:" +  slopeTH);

					if(slopeTL < -1*thresholdSlopeTL){
						errInc = -1 * computerDeviationSlopeCo * (Mathf.Abs((float)slopeTL/slopeTLDenominator)+1);
						//Debug.Log("SlopeDeltaTL < 0 slopeTL: " + slopeTL);

					} else{
						errInc = -2 * computerDeviationSlopeCo * (Mathf.Abs((float)slopeTL/slopeTLDenominator)+1);
						//Debug.Log("SlopeDeltaTL > 0 slopeTL: " + slopeTL);
					}

				}else{
					//Debug.Log("SlopeDeltaTwHealth < 0 slopeTH:" +  slopeTH);

					if(slopeTL < -1*thresholdSlopeTL){
						//Debug.Log("SlopeDeltaTL < 0 slopeTL: " + slopeTL);
					} else{

						//Debug.Log("SlopeDeltaTL > 0 slopeTL: " + slopeTL);
						midStateChangeCounter++;
						if(midStateChangeCounter == 3){
							errInc = computerDeviationSlopeCo;
							midStateChangeCounter = 0;
						}
					}
				}

			}else{
				Debug.Log("DeltaTwHealth < 0 ");

				if(slopeTH > thresholdSlopeTL){
					Debug.Log("SlopeDeltaTwHealth > 0 slopeTH:" +  slopeTH);
					if(slopeTL < -1*thresholdSlopeTL){

						Debug.Log("SlopeDeltaTL < 0 slopeTL: " + slopeTL);
						midStateChangeCounter++;
						if(midStateChangeCounter == 3){
							errInc = computerDeviationSlopeCo;
							midStateChangeCounter = 0;
						}

					} else{
						
						Debug.Log("SlopeDeltaTL > 0 slopeTL: " + slopeTL);
					}
					
				}else{

					Debug.Log("SlopeDeltaTwHealth < 0 slopeTH:" +  slopeTH);

					if(slopeTL < -1*thresholdSlopeTL){
						Debug.Log("SlopeDeltaTL < 0 slopeTL: " + slopeTL);
						errInc = 1.1f * computerDeviationSlopeCo  * (Mathf.Abs((float)slopeTL/slopeTLDenominator)+1);

					} else{
						Debug.Log("SlopeDeltaTL > 0 slopeTL: " + slopeTL);
						errInc = 2.2f * computerDeviationSlopeCo  * (Mathf.Abs((float)slopeTL/slopeTLDenominator)+1);

					}
				}

			}

			float preCompSpawnDev = computerSpawnDeviationFromPlayer;
			computerSpawnDeviationFromPlayer += errInc;
			if(computerSpawnDeviationFromPlayer < minComputerSpawnDeviationFromPlayer)
				computerSpawnDeviationFromPlayer = minComputerSpawnDeviationFromPlayer;

			if(computerSpawnDeviationFromPlayer < 0)
				computerSpawnDeviationFromPlayer = 0 ;

			string dataEntry =  playerID.ToString() + delimiter + Time.timeSinceLevelLoad.ToString() + delimiter +  currentTowerLevel 
				+ delimiter + GetPlayerTowerHealth().ToString() 
				 + delimiter + GetComputerTowerHealth().ToString() + delimiter + deltaTH.ToString() + delimiter + slopeTH.ToString() + delimiter 
					+ timeLossBetweenSpawnList[0] + delimiter + timeLossBetweenSpawnList[1] + delimiter + timeLossBetweenSpawnList[2] + delimiter 
					+ avgTimeLossBetweenSpawns + delimiter + slopeTL + delimiter + computerSpawnDeviationFromPlayer + delimiter + preCompSpawnDev;

			Dictionary<string, string> aiDataAnalytics = new Dictionary<string, string>() {
				{ "AIData", dataEntry }
			};

			ParseAnalytics.TrackEventAsync("AIData", aiDataAnalytics);
			if(isAIDataSaving){

				aiDataString += dataEntry;
				aiSW.WriteLine (dataEntry);
				aiSW.Flush ();

			}

			if(Time.timeSinceLevelLoad%30 <= 10 ){

//				Analytics.CustomEvent("AIUpdate", new Dictionary<string, object>
//				{
//					{ "Time", Time.timeSinceLevelLoad },
//					{ "PlayerTowerHealth", GetPlayerTowerHealth() },
//					{ "ComputerTowerHealth", GetComputerTowerHealth() },
//					{ "DeltaPlayerTowerHealth", deltaTH},
//					{ "SlopePlayerTowerHealth",slopeTH },
//					{ "AvgTimeLoss", avgTimeLossBetweenSpawns },
//					{ "SlopeTimeLoss", slopeTL},
//					{"PlayerSpawnDeviation", GetPlayerCurrentSpawnDeviation()},
//					{"ComputerSpawnDeviation", computerSpawnDeviationFromPlayer},
//				});
			}
		}

		StartCoroutine(UpdateComputerError ());
	}

	public IEnumerator WaitForNextTower(float t){
		yield return new WaitForSeconds(t);
		timeKeeper.ResetBudget();
		float playerTimePoints = 0;
		for(int i = 0; i< GameConstants.noUnitTypes; i++){
			playerTimePoints =  playerUnitStack[i].Count * GameConstants.unitSpawnDurationTable[i];
		}
		timeKeeper.AddTimePoints(playerTimePoints/1.5f);
		computerTower.ResetTowerHealth();
		playerTower.towerHealth += Mathf.RoundToInt( currentTowerLevel * GameConstants.compAIData.lvlIncPlayerHealth 
									* UnityEngine.Random.Range(1-GameConstants.compAIData.computerTowerHealthDeviation,1+GameConstants.compAIData.computerTowerHealthDeviation));
		playerTower.UpdateToweHealthText();
		UpdateCurrentLevelText();
		PlayerButtonsSetActive(true);

	}

	public void PlayerButtonsSetActive(bool isA){

		GameObject[] spL = GameObject.FindGameObjectsWithTag("PlayerSpawner");

		foreach(GameObject sp in spL){
			sp.GetComponent<BoxCollider>().enabled  = isA;
		}
		
		playerTower.archersR.GetComponent<BoxCollider>().enabled = isA;
		playerTower.spearmenR.GetComponent<BoxCollider>().enabled = isA;
		playerTower.horsemenR.GetComponent<BoxCollider>().enabled = isA;
	}
	

	public IEnumerator MyDelay(int sec, Task task1, Task task2, bool quit){

		yield return new WaitForSeconds(sec);
		if(!task1.IsCompleted ||  !task2.IsCompleted){
			StartCoroutine(MyDelay(1, task1, task2, quit));
		}else{
			if(quit)
				Application.Quit();
		}
	}

	void UpdateCurrentLevelText(){
		towerLevelText.text = "TOWER " + currentTowerLevel;
	}

	public int GetTierPlayerSpawner(int i){
		return tierPlayerSpawners[i];
	}
	
	public int GetTierComputerSpawner(int i){
		return tierComputerSpawners[i];
	}

	public void SetTierPlayerSpawner(int i, int t){
		tierPlayerSpawners[i] = t;
	}
	
	public void SetTierComputerSpawner(int i, int t){
		tierComputerSpawners[i] = t;
	}

	void OnApplicationQuit() {
//		Analytics.CustomEvent("ApplicationQuit", new Dictionary<string, object>
//		{
//			{ "Time", Time.timeSinceLevelLoad },
//			{ "PlayerTowerHealth", GetPlayerTowerHealth() },
//			{ "AvgTimeLoss", avgTimeLossBetweenSpawns },
//			{"PlayerSpawnDeviation", GetPlayerCurrentSpawnDeviation()},
//			{"ComputerSpawnDeviation", computerSpawnDeviationFromPlayer},
//			{"CompTowerLevel", currentTowerLevel}
//		});

		Dictionary<string, string> ApplicationQuitAnalytics = new Dictionary<string, string>() {
			// What level are you playing?
			{ "Time", Time.timeSinceLevelLoad.ToString() },
			{ "PlayerTowerHealth", GetPlayerTowerHealth().ToString() },
			{ "AvgTimeLoss", avgTimeLossBetweenSpawns.ToString() },
			{"PlayerSpawnDeviation", GetPlayerCurrentSpawnDeviation().ToString()},
			{"ComputerSpawnDeviation", computerSpawnDeviationFromPlayer.ToString()},
			{"CompTowerLevel", currentTowerLevel.ToString()}
		};
		// Send the dimensions to Parse along with the 'LostLife' event
		
		ParseAnalytics.TrackEventAsync("OnApplicationQuit", ApplicationQuitAnalytics);

		Application.CancelQuit();
		SaveData();

	}

	public void OnGameQuit(){
		SaveData();
		ResetCompAI();

		StateManager.Instance.SwitchMainMenuState();
	}

	public void OnReplay(){
		Debug.Log("OnReplay");
		//StartCoroutine(SaveData(false));
		SaveData();
		ResetCompAI();
		Start ();
		computerTower.Init();
		playerTower.Init();


	}

	public void ResetCompAI(){
		foreach ( GameObject u in playerUnitsOnField){
			playerSpawnedUnits.Remove(u);
			u.Recycle();
		}
		
		foreach ( GameObject u in computerUnitsOnField){
			computerSpawnedUnits.Remove(u);
			u.Recycle();
		}
		playerUnitsOnField.Clear();
		computerUnitsOnField.Clear();


	}

	public void SaveData(){
		StopCoroutine("UpdateComputerError");
		if(isAIDataSaving) 
		   aiSW.Close();

		 if(!isTouchDataSaving)
			tSW.Close ();
	}

	public void SetBoostMode(){
		if(!isBoostMode){
			timeKeeper.AddTimePoints(moreBoostTimePoints);
			isBoostMode = true;
			computerSpawnDeviationFromPlayer += moreBoostComputerSpawnDeviation;

//			Analytics.CustomEvent("BoostModeOn", new Dictionary<string, object>
//			{
//				{ "Time", Time.timeSinceLevelLoad },
//				{ "PlayerTowerHealth", GetPlayerTowerHealth() },
//				{ "AvgTimeLoss", avgTimeLossBetweenSpawns },
//				{ "PlayerSpawnDeviation", GetPlayerCurrentSpawnDeviation()},
//				{ "ComputerSpawnDeviation", computerSpawnDeviationFromPlayer},
//				{ "CompTowerLevel", currentTowerLevel +1}
//			});

			Dictionary<string, string> BoostModeOn = new Dictionary<string, string>() {
				// What level are you playing?
				{ "Time", Time.timeSinceLevelLoad.ToString() },
				{ "PlayerTowerHealth", GetPlayerTowerHealth().ToString() },
				{ "AvgTimeLoss", avgTimeLossBetweenSpawns.ToString() },
				{ "PlayerSpawnDeviation", GetPlayerCurrentSpawnDeviation().ToString()},
				{ "ComputerSpawnDeviation", computerSpawnDeviationFromPlayer.ToString()},
				{ "CompTowerLevel", (currentTowerLevel +1).ToString()}
			};
			// Send the dimensions to Parse along with the 'LostLife' event
			
			ParseAnalytics.TrackEventAsync("BoostModeOn", BoostModeOn);
		}
	}

	void Update(){
		if (Input.GetKeyDown ("n")) {
			NextTower();
		}else if (Input.GetKeyDown ("k")) {
			playerTower.TowerDamage(50);
		}if (Input.GetKeyDown ("l")) {
			computerTower.TowerDamage(50);
		}
	}
	
}


[Serializable]
public class PlayerStatusData{
	
	public float time;
	public float deltaTowerHealths;
	public float timeLoss;
	
	public PlayerStatusData(float t, float dTH, float tl){
		time = t;
		deltaTowerHealths = dTH;
		timeLoss = tl; 
	}
}

[Serializable]
public class DistancePivotPoint{

	public Vector2 position;
	public int[] playerUnitsCodes;
	public int[] computerUnitsCodes;

	public float closestComputerUnitToPlayerTw { get; set;}
	public float closestPlayerUnitToComputerTw { get; set;}

	public DistancePivotPoint(){
		playerUnitsCodes = new int[3]{0,0,0};
		computerUnitsCodes = new int[3]{0,0,0};
	}

	public DistancePivotPoint(Vector2 point){
		position = point;
		playerUnitsCodes = new int[3]{0,0,0};
		computerUnitsCodes = new int[3]{0,0,0};
	}

	public void Init(){
		for (int i = 0; i < 3; i++) {
			playerUnitsCodes[i] = 0;
			computerUnitsCodes[i] = 0;
		}
	}

	public int GetPlayerUnitNumber(){
		return playerUnitsCodes[0] + playerUnitsCodes[1] + playerUnitsCodes[2];
	}

	public int GetComputerUnitNumber(){
		return computerUnitsCodes[0] + computerUnitsCodes[1] + computerUnitsCodes[2];
	}
	
}

