using UnityEngine;
using System.Collections;

public class Archery : PlayerSpawner {

	public Archery() {

	}

	void Start(){

		spawnerType = 2;
		spawnDuration = GameConstants.unitSpawnDurationTable [spawnerType];
		flagText.text = spawnDuration + " s.";
		spawnerSpriteName = GameConstants.getSpawnerSpriteName (spawnerType);
		base.Init();
	}
}
