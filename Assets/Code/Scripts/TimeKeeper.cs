using UnityEngine;
using System.Collections.Generic;

public class TimeKeeper{
	private float gameStartTime;
	private float collectionTime;
	private float timePoints { get; set;}
	private float leftBudget;
	private float[] unitCostTable;
	private float maxComputerTimeLossBtSpawn;


	public TimeKeeper(){
		//Debug.Log("Timekeeper start.");
		gameStartTime = Time.timeSinceLevelLoad;
		collectionTime = Time.timeSinceLevelLoad;
		unitCostTable = GameConstants.unitSpawnDurationTable;
		maxComputerTimeLossBtSpawn = 5f;
		timePoints = 0f;
		leftBudget = 0f;
	}

	public float UpdateKeeper(){
		timePoints +=  (Time.timeSinceLevelLoad - collectionTime);
		collectionTime = Time.timeSinceLevelLoad;
		
		return timePoints;
	}

	public void AddTimePoints(float tp){
		timePoints += tp;
	}

	public void ResetBudget(){
		timePoints = 0;
		collectionTime = Time.timeSinceLevelLoad;
	}
	
	public float CalculateCurrentBudget(){
		return (Time.timeSinceLevelLoad - collectionTime + timePoints);
	}
	
	public float GetCurrentBudget(){
		return UpdateKeeper();
	}
	
	public bool CheckSimulationCost(float hordeCost){
		UpdateKeeper ();
		if (hordeCost > timePoints)
			return false;
		else
			return true;
	}
	
	public bool CheckSimulationCost(int[] horde){
		UpdateKeeper ();
		float cost = CalculateHordeCostWithError (horde);
		if (cost > timePoints)
			return false;
		else {
			return true;
		}
	}
	
	
	public bool SpendTime(float cost){
		UpdateKeeper ();
		if (cost > timePoints)
			return false;
		else {
			timePoints-= cost;
			return true;
		}
		
	}
	
	public float CalculateHordeCost(int[] horde){
		float cost = 0;
		
		for(int i = 0; i < horde.Length; i++ ){
			for(int j = 0; j < horde[i]; j++ ){
				cost += unitCostTable[i];
			}
		}
		return cost;
	}
	
	public float CalculateHordeCostReturnLeftover(int[] horde){
		float cost = 0;
		
		for(int i = 0; i < horde.Length; i++ ){
			for(int j = 0; j < horde[i]; j++ ){
				cost += unitCostTable[i];
			}
		}
		UpdateKeeper ();
		return (timePoints  - cost);
	}
	
	public bool SpawnHordeWithError(int[] horde){
		bool sucessHorde = true;
		for(int i = 0; i < horde.Length; i++ ){
			for(int j = 0; j < horde[i]; j++ ){
				if(!SpawnSingleWithError(i)){
					sucessHorde = false;
					break;
				}

			}
		}
		return sucessHorde;
		
	}
	

	
	public bool SpawnSingleWithError(int unitCode){
		bool success = false;
		while(!success){
			
			float[] cPD = GetComputerUnitCostWithError(unitCode);
			float tBS = ComputerAI.Instance.avgTimeLossBetweenSpawns * ComputerAI.Instance.computerSpawnDeviationFromPlayer;
			if(tBS > maxComputerTimeLossBtSpawn)
				tBS = maxComputerTimeLossBtSpawn;
			SpendTime (tBS);
			if(SpendTime(cPD[0])){
				if(cPD[0] >= unitCostTable[unitCode] || cPD[0] >= cPD[1]){
					ComputerAI.Instance.ComputerTowerSpawnSingleUnit(unitCode);
					success = true;
				}else{
					success = false;
				}
			}else
				return false;
		}
		return success;
		
	}
	
	public float calculateSingleWithError(int unitCode){
		bool success = false;
		float cost = 0;
		while(!success){
			float[] cPD = GetComputerUnitCostWithError(unitCode);
			float tBS = (ComputerAI.Instance.avgTimeLossBetweenSpawns * ComputerAI.Instance.computerSpawnDeviationFromPlayer);
			if(tBS > maxComputerTimeLossBtSpawn)
				tBS = maxComputerTimeLossBtSpawn;
			cost += tBS;
			cost += cPD[0];	
			if(cPD[0] >= unitCostTable[unitCode] || cPD[0] >= cPD[1] ){
				return cost;
			}else{
				success = false;
			}
		}
		return 9999;
		
	}
	
	
	public float CalculateHordeCostWithError(int[] horde){
		float cost = 0;
		
		for(int i = 0; i < horde.Length; i++ ){
			for(int j = 0; j < horde[i]; j++ ){
				cost += calculateSingleWithError(i);

			}
		}
		return cost;
	}
	
	public float[] GetComputerUnitCostWithError(int unitCode){

		//Debug.Log ("player deviation for unit code: " + unitCode + " deviation: " + pD);
		float computerDeviation = ComputerAI.Instance.GetComputerCurrentSpawnDeviation(unitCode);
		float cP = unitCostTable[unitCode] * computerDeviation;
		//Debug.Log("player unit cost for unit code: " + unitCode + " cost: " + cP);
		return new float[2]{cP,ComputerAI.Instance.maxComputerSpawnDeviationFromPlayer};
	}
	
}