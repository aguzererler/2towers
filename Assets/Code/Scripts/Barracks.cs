using UnityEngine;
using System.Collections;

public class Barracks : PlayerSpawner {

	public Barracks()  {


	}

	void Start(){

		spawnerType = 0;
		spawnDuration = GameConstants.unitSpawnDurationTable [spawnerType];
		flagText.text = spawnDuration + " s.";
		spawnerSpriteName = GameConstants.getSpawnerSpriteName (spawnerType);
		base.Init();
	}

}
