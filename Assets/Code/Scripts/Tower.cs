using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;



public class Tower : MonoBehaviour {

	const int COMPUTER = 2;
	const int  PLAYER = 1;

	public bool isSpawning;

	public List<PlayerSpawner> Spawners;
	public List<GameObject> spawnedUnits;
	public List<GameObject> unitsInOutQue;
	public List<GameObject> unitsInTempQue;
	public List<GameObject> unitsOnField;
	public List<GameObject>[] unitStack;

	public int[] unitsInQueNumbers;

	private float lastUnitOutTimeOffset;
	private float lastUnitOutTime;
	private bool areUnitsLeaving;

	//
	public GameObject nOStackSpearmenObject;
	private tk2dTextMesh nOStackSpearmen;
	public GameObject nOStackHorsemenObject;
	private tk2dTextMesh nOStackHorsemen;
	public GameObject nOStackArchersObject;
	private tk2dTextMesh nOStackArchers;

	private tk2dTextMesh[] queUnitNoText;
	private tk2dTextMesh[] stackUnitNoText;

	public float towerHealth { get; set;}
	public int[] spawnerTiers;
	public GameObject towerHealthObj;
	private tk2dTextMesh towerHealthText;
	
	public GameObject spearmenR;
	public GameObject horsemenR;
	public GameObject archersR;


	public Dictionary<GameObject, int> spawnObjects;

	//type of the spawner
	protected string towerSpriteName;
	protected int towerType;
	protected string towerTag;

	public bool areGatesOpen;
	public GameObject enemyTower;

	public AudioClip releaseSoundMWalk;
	public AudioClip releaseSoundHWalk;
	public AudioClip releaseSoundFail;

	void Awake(){

		if(this.tag.Equals ("Player")){

			
			tk2dUIItem spearmenRUI = spearmenR.GetComponent<tk2dUIItem> ();
			spearmenRUI.OnClickUIItem += UnitReleaseButton;
			tk2dUIItem horsemenRUI = horsemenR.GetComponent<tk2dUIItem> ();
			horsemenRUI.OnClickUIItem += UnitReleaseButton;
			tk2dUIItem archersRUI  =  archersR.GetComponent<tk2dUIItem> ();
			archersRUI.OnClickUIItem += UnitReleaseButton;
			

		}
	
	}

	public void Init(){
		unitsInOutQue = new List<GameObject> ();
		unitsOnField = new List<GameObject> ();
		unitsInQueNumbers = new int[3]{0,0,0};
		spawnerTiers = new int[3]{0,0,0};
		
		if (this.tag.Equals ("Computer")) {
			towerHealth = GameConstants.compAIData.computerTowerHeatlhBase;
			towerType = COMPUTER;	
			ComputerAI.Instance.computerUnitsOnField  = unitsOnField;
			ComputerAI.Instance.computerUnitsInOutQue = unitsInOutQue;
			ComputerAI.Instance.computerSpawnedUnits = spawnedUnits;
		}
		else if(this.tag.Equals ("Player")){
			towerHealth = GameConstants.compAIData.playerStartHealth;
			towerType  = PLAYER;
			unitStack = new List<GameObject>[GameConstants.noUnitTypes];
			queUnitNoText = new tk2dTextMesh[GameConstants.noUnitTypes];
			stackUnitNoText = new tk2dTextMesh[GameConstants.noUnitTypes];
			
			ComputerAI.Instance.playerUnitsOnField = unitsOnField;
			ComputerAI.Instance.playerUnitsOnQue = unitsInOutQue;
			ComputerAI.Instance.playerUnitStack = unitStack;
			ComputerAI.Instance.playerSpawnedUnits = spawnedUnits;
			
			nOStackSpearmen = nOStackSpearmenObject.GetComponent<tk2dTextMesh> ();
			nOStackHorsemen = nOStackHorsemenObject.GetComponent<tk2dTextMesh> ();
			nOStackArchers  = nOStackArchersObject.GetComponent<tk2dTextMesh> ();
			
			for (int i = 0; i < GameConstants.noUnitTypes; i++) {
				unitStack[i] = new List<GameObject>();
				if(i == 0){
					stackUnitNoText[i] = nOStackSpearmen;
				}else if(i == 1){
					stackUnitNoText[i] = nOStackHorsemen;
				}else if(i == 2){
					stackUnitNoText[i] = nOStackArchers;
				}
				
			} 

			for(int i = 0; i < GameConstants.noUnitTypes; i++)
				stackUnitNoText[i].text = "0";

			foreach (PlayerSpawner s in Spawners){
				s.Reset();
			}
		}
		
		lastUnitOutTimeOffset = -1;
		lastUnitOutTime = -1;
		areUnitsLeaving = false;
		unitsInTempQue = new List<GameObject> ();
		areGatesOpen =  true;
		
		towerHealthText = towerHealthObj.GetComponent<tk2dTextMesh> ();
		UpdateToweHealthText();

	}

	void Start () {
		Init ();
	}

	void UnitReleaseButton(tk2dUIItem tkUI){

		string uiName = tkUI.name;
		int uC = -1;

		//Debug.Log ("release pressed " + uiName);
		if (uiName.Equals ("SpearmanRelease")) {
			uC = 0;
		}else if(uiName.Equals ("HorsemanRelease")){
			uC = 1;
		}else if(uiName.Equals ("ArcherRelease")){
			uC = 2;
		}
		if(!areGatesOpen)
			ToggleGates();

		if(unitStack[uC].Count > 0){
			GameObject go = unitStack[uC].Last();
			if(areUnitsLeaving)
				unitsInTempQue.Add(go);
			else
				unitsInOutQue.Add(go);
			unitsInQueNumbers[uC]++;
			unitStack[uC].Remove(go);
			UpdateHordeNumberTexts (uC);
		}else{
			if(uC == 0){
				tk2dUISoundItem si = spearmenR.GetComponent<tk2dUISoundItem>();
				si.releaseButtonSound = releaseSoundFail;
			}else if(uC == 1){
				tk2dUISoundItem si = horsemenR.GetComponent<tk2dUISoundItem>();
				si.releaseButtonSound = releaseSoundFail;
			}else if(uC == 2){
				tk2dUISoundItem si = archersR.GetComponent<tk2dUISoundItem>();
				si.releaseButtonSound = releaseSoundFail;
			}
		}


	}

	void UpdateHordeNumberTexts(int uC){
		int queCount = unitsInQueNumbers [uC];
		int stackCount = unitStack[uC].Count;

		stackUnitNoText[uC].text = stackCount.ToString();
	}
	
	// Update is called once per frame
	void Update () {
		if ((unitsInOutQue.Count > 0 || unitsInTempQue.Count > 0) && areGatesOpen && !areUnitsLeaving){
			if (unitsInTempQue.Count > 0) {
				foreach(GameObject go in unitsInTempQue){
					Unit u = go.GetComponent<Unit>();
					int uC = u.unitCode;
					unitsInOutQue.Add(go);
				}
				unitsInTempQue.Clear();
			}
				
			SendOutUnitsInOutQue ();
		}
	}

	public void ToggleGates(){
		//Debug.Log ("GatesChanged " + gameObject.tag);
		if (areGatesOpen) {
			CloseGates();
		} else {
			OpenGates();
		}
	}

	public void SendOutUnitsInOutQue(){

		Vector3 towerPosition = this.transform.position;
		float timeOffset = lastUnitOutTime - Time.time; 
		if (timeOffset < 0)
						timeOffset = 0;
		int i = 0;
		foreach (GameObject go in unitsInOutQue) {
			
			go.transform.position = new Vector3 (transform.position.x,transform.position.y,-1.9f);
			StartCoroutine(SetActiveUnitByTime((timeOffset + i*0.6f),go));
			i++;
		}
		lastUnitOutTime = Time.time + i * 0.6f;
		areUnitsLeaving = true;
		//Debug.Log(this.tag + " Tower, areUnitsLeaving " + areUnitsLeaving);
	}

	public void OpenGates(){
		areGatesOpen = true;
	}

	void CloseGates(){
		areGatesOpen = false;
	}
	
	public bool isOtherSpawning(){
		bool tIsSpawning = false;
		foreach (PlayerSpawner tS in Spawners) {
			if(tS.isSpawning){
				tIsSpawning = true;
				break;
			}
		}
		return tIsSpawning;
	}

	public void Spawn(GameObject spObj, int tier){
 		
		GameObject spawnedObject = spObj.Spawn(transform.position, Quaternion.Euler(0,0,0));
		if (towerType == COMPUTER) {
			spawnedObject.tag = "Computer";
			spawnedObject.layer = 9;
		}
		else if(towerType == PLAYER){
			spawnedObject.tag = "Player";
			spawnedObject.layer = 8;
		}

		IUnitBase sU = spawnedObject.GetComponent(typeof(IUnitBase)) as IUnitBase;

		sU.Init (enemyTower,tier);
		sU.setUnitSprites(towerType);
		spawnedObject.SetActive(false);


		if(towerType == COMPUTER){
			if(areUnitsLeaving){
				unitsInTempQue.Add (spawnedObject);
				unitsInQueNumbers[sU.unitCode]++;
			}
			else{
				unitsInOutQue.Add(spawnedObject);
				unitsInQueNumbers[sU.unitCode]++;
			}
		}else if(towerType == PLAYER){

			unitStack[sU.unitCode].Add(spawnedObject);
			
			if(unitStack[sU.unitCode].Count == 1){
				if(sU.unitCode == 0){
					tk2dUISoundItem si = spearmenR.GetComponent<tk2dUISoundItem>();
					si.releaseButtonSound = releaseSoundMWalk;
				}else if(sU.unitCode == 1){
					tk2dUISoundItem si = horsemenR.GetComponent<tk2dUISoundItem>();
					si.releaseButtonSound = releaseSoundHWalk;
				}else if(sU.unitCode == 2){
					tk2dUISoundItem si = archersR.GetComponent<tk2dUISoundItem>();
					si.releaseButtonSound = releaseSoundMWalk;
				}
				
			}

			UpdateHordeNumberTexts(sU.unitCode);
		}

		spawnedUnits.Add (spawnedObject);
	}


	IEnumerator SetActiveUnitByTime(float activationTime, GameObject go)
	{

		yield return new WaitForSeconds(activationTime);
		ComputerAI.Instance.newUnitOnField = true;
		unitsInOutQue.Remove (go);
		Unit u  = go.GetComponent<Unit>();
		unitsInQueNumbers[u.unitCode]--;
		if(towerType == COMPUTER){
			ComputerAI.Instance.newComputerUnitOnField = true;
			
		} else if(towerType == PLAYER){
			ComputerAI.Instance.newPlayerUnitOnField = true;
			UpdateHordeNumberTexts(u.unitCode);
		}
		if(unitsInOutQue.Count == 0){
			areUnitsLeaving = false;
			if(unitsInTempQue.Count == 0){
				CloseGates();
			}
		}

		if(unitsInOutQue.Count <= 0){
			areUnitsLeaving = false;
			//Debug.Log(this.tag + " Tower, areUnitsLeaving " + areUnitsLeaving);
		}

		unitsOnField.Add (go);
		//Debug.Log(this.tag + " Tower, coroutine gates");
		go.SetActive (true);
	}

	IEnumerator Countdown(int t)
	{
		for (float timer = t ; timer >= 0; timer -= Time.deltaTime)
			yield return 0;
				
		gameObject.Recycle ();
	}

	private bool regenerationCourotineNotRunning = true;
	public void TowerDamage(float dmg){

		towerHealth -= dmg;
		if(towerHealth < 0 ){
			towerHealth = 0;
			ComputerAI.Instance.TowerDefeated(this);
		}
		UpdateToweHealthText();

		if (regenerationCourotineNotRunning) {
			StartCoroutine (RegenerateTowerLife (30f));
			regenerationCourotineNotRunning = false;		
		}
	}

	public void UpdateToweHealthText(){
		int tText = Mathf.CeilToInt(towerHealth);
		towerHealthText.text = tText.ToString();
	}

	IEnumerator RegenerateTowerLife (float t)
	{
		yield return new WaitForSeconds(t);
		Debug.Log (this.name + " regenerate tower life");
		towerHealth += 1f;
		if (towerHealth >= 200) {
			towerHealth = 200;
			StopCoroutine("RegenerateTowerLife");
			regenerationCourotineNotRunning= true;
		}else{
			StartCoroutine (RegenerateTowerLife(t));
		}
		int tText = (int)(towerHealth);
		towerHealthText.text = tText.ToString();
	}

	public void ResetTowerHealth(){
		towerHealth = Mathf.Round(GameConstants.compAIData.computerTowerHeatlhBase + 
		                          GameConstants.compAIData.lvlIncCoComputerHealth * ComputerAI.Instance.currentTowerLevel * Random.Range(1-GameConstants.compAIData.computerTowerHealthDeviation,1+GameConstants.compAIData.computerTowerHealthDeviation));
		towerHealthText.text = towerHealth.ToString();
	}
}
