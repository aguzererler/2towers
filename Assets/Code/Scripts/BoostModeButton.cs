﻿using UnityEngine;
using System.Collections;

public class BoostModeButton:MonoBehaviour {

	private tk2dSprite bSprite;
	BoxCollider bC;

	void Start(){
		bSprite = gameObject.GetComponentInChildren<tk2dSprite>();
		bC = GetComponent<BoxCollider>();
	}

	public void StartMoreMode(){
		bSprite.SetSprite("MoreOnButton");
		bC.enabled = false;
		gameObject.SendMessageUpwards("ShowSmallMessage","More enemies are coming \n and they drop MORE COINS.");
		StartCoroutine(WaitForMoreMode(60));

	}

	public void EndMoreMode(){
		bSprite.SetSprite("BoostMode");
		gameObject.SendMessageUpwards("SetBoostMode");


	}

	IEnumerator WaitForMoreMode(float t){
		yield return new WaitForSeconds(t);
		EndMoreMode();
		bC.enabled = true;
		gameObject.SendMessageUpwards("ShowSmallMessage","My Lord, \n It is normal mode again.");

	}
}
