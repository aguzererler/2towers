﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Horseman : Unit {
	const int COMPUTER = 2;
	const int  PLAYER = 1;

	void Awake(){
		unitCode = 1;
		typeUnit = "Horseman";
		//health.maxHealth = GameConstants.unitMaxHealthTable [unitCode];
		//health.regenerationRate = 0.012f;
		
		movement.isMoving = true;
		//movement.Speed = 18;
		movement.speed = 18;
		movement.turnSpeed = 1;
		
		//attack.damageTable = GameConstants.GetUnitDamageTable(unitCode);
		//attack.bonusDamage = 0;
		//attack.damageType = 1;
	}

	public override void setUnitSprites(int typeTower){
		//Debug.Log("horseman SetSprites");
		uSprite = gameObject.GetComponent<tk2dSprite> ();
		
		if(typeTower == COMPUTER){
			uSprite.SetSprite(GameConstants.unitSpriteNamesComputer[unitCode]);

		}else if(typeTower == PLAYER){
			uSprite.SetSprite(GameConstants.unitSpriteNamesPlayer[unitCode]);

		}else{
			Debug.Log("wrong tower type");
		}
	}
	
	public override void playAttackAnimation(){
		//Debug.Log("play atack ani. hman");
		sprites[0].transform.DOLocalMove( new  Vector3(-20f, 5f, 0), 0.3f, false).SetLoops(2, LoopType.Yoyo);
	}
	
	public override void getAttackStand(){
		sprites[0].transform.DOLocalRotate(new Vector3(0,0,20),0.2f);
		//Debug.Log("hman get attack attack stand.");
	}
	
	public override void getNormalStand(){
		sprites[0].transform.DOLocalMove(new Vector3(-6f,13.5f,0),0.2f);
		sprites[0].transform.DOLocalRotate(new Vector3(0,0,-20),0.2f);
		//Debug.Log("hman get normal stand.");
	}
}
