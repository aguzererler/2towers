﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class PopUpMessage : MonoBehaviour {

	tk2dTextMesh text;

	// Use this for initialization
	void Start () {
		text =  gameObject.GetComponentInChildren<tk2dTextMesh>() ;

	}

	public void ShowMessage(string t, float d){
		text.text = t;
		//gameObject.transform.localScale = new Vector3 (0.01,0.01, 0.01);
		gameObject.transform.SetLocalPositionZ(-1);
		gameObject.transform.DOPunchScale(new Vector3(0.3f,0.3f,0), 0.3f, 4, 0.3f);
		StartCoroutine(DeleteMessage(d));
	}

	IEnumerator DeleteMessage (float t)
	{
		yield return new WaitForSeconds(t);
		gameObject.transform.SetLocalPositionZ(15);

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
