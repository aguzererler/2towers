﻿using UnityEngine;
using System.Collections;
using System;
using GameDataEditor;


public class GameConstants : MonoBehaviour {

	protected static GameConstants instance;

	public static float unitsInitalDistanceToTargetTower = 1300f;


	//Game construction parameters.
	public static int noUnitTypes = 3;
	public static int noTiers = 0;

	public static readonly int BARRACKS = 0;
	public static readonly int STABLE = 1;
	public static readonly int ARCHERY = 2;

	public static float criticalTowerHealth = 15f;

	public static string[] unitSpriteNamesPlayer = new string[]{"SpearmanRedNA","HorsemanRedNA","ArcherRedNA"};
	public static string[] accSpriteNamesPlayer = new string[]{"ShieldRed","","BowRed"};
	public static string[] unitSpriteNamesComputer = new string[]{"SpearmanBlueNA","HorsemanBlueNA","ArcherBlueNA"};
	public static string[] accSpriteNamesComputer = new string[]{"ShieldBlue","","BowBlue"};
	
	public static readonly string barracksSpriteName = "Barracks";
	public static readonly string stableSpriteName = "Stables";
	public static readonly string archerySpriteName = "Archery";

	//To be consturcted from UnitDatasheets
	public static DamageData[] unitDamageDataTable;
	//public static float[,] avgDamageTable = new float[,]{{5.5f, 5.4f, 4.5f}, {7.5f, 7.5f, 9f},{6.5f, 5.5f, 6.5f}};

	public static HealthPropertiesGC[] unitHealthPTable;
	public static float[] unitSpawnDurationTable;

	public static float[] unitSpeedTable;
	public static float unitTowerDamageCoeff = 0.2f;

	public static int[] unitAttackUpgradeCostTable;
	public static int[] unitHealthUpgradeCostTable;
	public static Vector2[] unitCoinPTable;
	
	//Save data parameters.
	public static String playerID = string.Format ("{0:yyyyMMddhhmm}", DateTime.Now);
	public static GDEGameAIData compAIData;

	public bool areParametersUpdated = false;


	public static GameConstants Instance
	{
		get
		{
			if(instance == null)
			{

				instance = (GameConstants) FindObjectOfType(typeof(GameConstants));

				
				if (instance == null)
				{
					Debug.LogError("An instance of " + typeof(GameConstants) + 
					               " is needed in the scene, but there is none.");
				}
			}
			
			return instance;
		}
	}

	void Awake(){
		if(!instance) {
			//Init ();

			instance = this;
			DontDestroyOnLoad(this);
			StartCoroutine(InitGDE());
		}else 
			Destroy(this);

	}

	IEnumerator InitGDE () {
		WWW payload = new WWW("http://2towers.ahmetguzererler.com/gde_data.txt");
		yield return payload;
		
		if (!GDEDataManager.InitFromText(payload.text)) {
			Debug.LogError("Error initializing!");
			GDEDataManager.Init("gde_data");
		}


		Init();
	}

	 static void Init(){
		/*
		GDEDataManager.Init("gde_data");
		*/
		GDEUnitData paramArcher = new GDEUnitData(GDEItemKeys.Unit_Archer);
		GDEUnitData paramSpearman = new GDEUnitData(GDEItemKeys.Unit_Spearman);
		GDEUnitData paramHorseman = new GDEUnitData(GDEItemKeys.Unit_Horseman);

		compAIData = new GDEGameAIData (GDEItemKeys.GameAI_CompAI);
	
//		if (!GDEDataManager.DataDictionary.TryGetCustom(GDEItemKeys.Unit_Archer, out paramArcher))
//			Debug.LogError("Error reading archer data!");
//
//		if (!GDEDataManager.DataDictionary.TryGetCustom(GDEItemKeys.Unit_Spearman, out paramSpearman))
//			Debug.LogError("Error reading spearman data!");
//
//		if (!GDEDataManager.DataDictionary.TryGetCustom(GDEItemKeys.Unit_Horseman, out paramHorseman))
//			Debug.LogError("Error reading horseman data!");
//
//		if (!GDEDataManager.DataDictionary.TryGetCustom(GDEItemKeys.GameAI_CompAI, out compAIData))
//			Debug.LogError("Error reading gameai data!");



		unitSpawnDurationTable = new float[]{paramSpearman.spawnTime, paramHorseman.spawnTime, paramArcher.spawnTime};

		DamageData spearmanDamageData = new DamageData(paramSpearman.damageBase, paramSpearman.damageMax, paramSpearman.damagePerTierBonus, paramSpearman.damageUnitMulti);
		DamageData horsemanDamageData = new DamageData(paramHorseman.damageBase, paramHorseman.damageMax, paramHorseman.damagePerTierBonus, paramHorseman.damageUnitMulti);
		DamageData archerDamageData = new DamageData(paramArcher.damageBase, paramArcher.damageMax, paramArcher.damagePerTierBonus, paramArcher.damageUnitMulti);
		
		unitDamageDataTable = new DamageData[]{spearmanDamageData, horsemanDamageData, archerDamageData};

		unitSpeedTable = new float[]{paramSpearman.speed, paramHorseman.speed, paramArcher.speed};

		unitHealthPTable  = new HealthPropertiesGC[]{ 
			new HealthPropertiesGC(paramSpearman.healthMaxBase, paramSpearman.healthBonusPerTier, 
			                     paramSpearman.damageReductionBase, paramSpearman.damageReductionBonusPerTier),
			new HealthPropertiesGC(paramHorseman.healthMaxBase, paramHorseman.healthBonusPerTier, 
			                     paramHorseman.damageReductionBase, paramHorseman.damageReductionBonusPerTier),
			new HealthPropertiesGC(paramArcher.healthMaxBase, paramArcher.healthBonusPerTier, 
			                     paramArcher.damageReductionBase, paramArcher.damageReductionBonusPerTier)};

		unitAttackUpgradeCostTable = new int[]{paramSpearman.tierAttackUpgradeCost, 
												paramHorseman.tierAttackUpgradeCost, 
													paramArcher.tierAttackUpgradeCost};

		unitHealthUpgradeCostTable = new int[]{paramSpearman.tierHealthUpgradeCost, 
												paramHorseman.tierHealthUpgradeCost, 
													paramArcher.tierHealthUpgradeCost};

		unitCoinPTable = new Vector2[]{new Vector2(paramSpearman.coinstoDrop, paramSpearman.coinDropRate),
							new Vector2(paramHorseman.coinstoDrop, paramHorseman.coinDropRate),
								new Vector2(paramArcher.coinstoDrop, paramArcher.coinDropRate)};

		Instance.areParametersUpdated = true;
		Debug.Log("GC Parameters Update End");

	}
	
	public static HealthProperties GetUnitHealthProperties(int uc, int tier){
		return new HealthProperties( unitHealthPTable[uc].healthMaxBase + unitHealthPTable[uc].healthBonusPerTier*tier, 
		                            unitHealthPTable[uc].damageReductionBase + unitHealthPTable[uc].damageReductionPerTier * tier);
	}

	public static AttackProperties GetUnitAttackProperties(int uc, int tier){
		return new AttackProperties(unitDamageDataTable[uc], tier);
	}


	public static float GetUnitHealthMax(int uc, int tier){
		return unitHealthPTable[uc].healthMaxBase + unitHealthPTable[uc].healthBonusPerTier*tier;
	}

	public static string getSpawnerSpriteName(int t){
		switch (t) {
		case 0:
			return barracksSpriteName;
			break;
			
		case 1:
			return stableSpriteName;
			break;
			
		case 2:
			return archerySpriteName;
			break;
		default:
			return "";
			break;
			
		}
	}

	public static string getSpawnUnitPrefabName(int t){
		switch (t) {
		case 0:
			return "4towers/uPrefabSpearman";
			break;
			
		case 1:
			return "4towers/uPrefabHorseman";
			break;
			
		case 2:
			return "4towers/uPrefabArcher";
			break;
		default:
			return "";
			break;
			
		}
	}


	public static float MapInterval(float val, float srcMin, float srcMax, float dstMin, float dstMax) {
		if (val>=srcMax) return dstMax;
		if (val<=srcMin) return dstMin;
		return dstMin + ((val-srcMin) / (srcMax-srcMin)) * (dstMax-dstMin);
	} 
	

/*
	public static float GetAvgDamage(int uc, int auc){
		return avgDamageTable[uc,auc];
	}
	*/

}

[Serializable]
public class HealthPropertiesGC //Not a MonoBehaviour!
{
	public float regenerationRate;
	public float healthMaxBase;
	public float healthBonusPerTier;
	public float health;
	public float damageReductionBase;
	public float damageReductionPerTier;
	public int tier {get; set;}
	
	public HealthPropertiesGC(float mHealth, float hBPT, float dmgRed, float dmgRedPT ){
		healthMaxBase = mHealth;
		healthBonusPerTier = hBPT;
		health = healthMaxBase;
		damageReductionBase = dmgRed;
		damageReductionPerTier = dmgRedPT;
	}

}