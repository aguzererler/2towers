﻿	using UnityEngine;
using System.Collections;

using Assets.Code.States;
using Assets.Code.Interfaces;

public class StateManager : MonoBehaviour {

	public IBaseState activeState;
	private StateManager instanceRef;

	protected static StateManager instance;
	
	/**
      Returns the instance of this singleton.
   */
	public static StateManager Instance
	{
		get
		{
			if(instance == null)
			{
				instance = (StateManager) FindObjectOfType(typeof(StateManager));
				
				if (instance == null)
				{
					Debug.LogError("An instance of " + typeof(StateManager) + 
					               " is needed in the scene, but there is none.");
				}
			}
			
			return instance;
		}
	}


	void Awake(){
		if (instanceRef == null) {
			instanceRef = this;
			DontDestroyOnLoad (this);
		} else {
			DestroyImmediate(gameObject);
		}
	}
	// Use this for initialization
	void Start () {
		activeState = new MainMenuState(this);
		Debug.Log ("active state: " + activeState);

	}
	
	// Update is called once per frame
	void Update () { 

		if (activeState != null) {
			activeState.StateUpdate();	
		}
	}

	//called before showing graphics.
	void OnGUI(){
		if (activeState != null) {
			activeState.Show ();
		}
	}

	public void SwitchState(IBaseState activeStateRef){
		activeState = activeStateRef;
		activeState.Initialize ();
	}

	public void SwitchPlayState(){
		SwitchState (new PlayState(this));
		Application.LoadLevel("4T_G020");
	}

	public void SwitchMainMenuState(){
		SwitchState (new MainMenuState(this));
		Application.LoadLevel("4T_Main");
	}
}
