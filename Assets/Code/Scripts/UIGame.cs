﻿using UnityEngine;
using System.Collections;

public class UIGame : MonoBehaviour {

	public GameObject menuGameOver;
	public GameObject menuPause;
	private GameObject activeMenu;
	public GameObject overlay;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	//Game Events
	public void OnGameOver(){
		menuGameOver.SetActive(true);
		overlay.SetActive(true);
		activeMenu = menuGameOver;
		Time.timeScale = 0;
	}
	
	//UI Buttons
	public void OnPause(){
		menuPause.SetActive(true);
		overlay.SetActive(true);
		activeMenu = menuPause;
		Time.timeScale = 0;
	}
	
	public void OnInGameBuyCoins(){
		
	}
	
	//Menu Functions
	public void OnVolumeChange(){
		AudioListener.pause = !AudioListener.pause;
	}
	
	public void OnShare(){
		
	}
	
	public void OnResume(){
		activeMenu.SetActive(false);
		overlay.SetActive(false);
		Time.timeScale = 1;
	}
	
	public void OnReplay(){
		ComputerAI.Instance.OnReplay();
		activeMenu.SetActive(false);
		overlay.SetActive(false);
		Time.timeScale = 1;
	}
	
	public void OnQuitMenu(){
		ComputerAI.Instance.OnGameQuit();
		activeMenu.SetActive(false);
		overlay.SetActive(false);
		Time.timeScale = 1;
	}

}
