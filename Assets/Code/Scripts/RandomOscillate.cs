﻿using UnityEngine;
using System.Collections;

public class RandomOscillate : MonoBehaviour {
	// Use this for initialization
	int oscDir = 1;
	int preOScDir = 1;
	float oscSpeed = 6;
	float osc = 120;
	Vector3 initPos;
	void Awake(){
		initPos = this.transform.localPosition;

	}

	void Start () {
		transform.localPosition = initPos;
	}
	
	// Update is called once per frame
	void Update () {

		float localZ = gameObject.transform.localRotation.eulerAngles.z;

		if(localZ > 10 && localZ <180){
			oscDir = -1;
		}else if(localZ < 350 && localZ > 180){
			oscDir = 1;
		}
		
		if(preOScDir != oscDir ){
			preOScDir = oscDir;
			oscSpeed = Random.Range(2.5f,6f);
			osc = oscSpeed * oscDir * 60;
		}
	
		transform.SetLocalPositionX(transform.localPosition.x - osc*Time.deltaTime);  
		transform.localRotation = Quaternion.Euler( new Vector3(0,0,localZ+(osc*Time.deltaTime)) );
	}
}
