﻿using UnityEngine;
using System.Collections;

public class PlayObject : MonoBehaviour {

	public int _duration;
	public int _scale;

	public void Init(int duration, int scale){
		_duration = duration;
		_scale = scale;
		Start ();
	}

	// Use this for initialization
	void Start () {
		StartCoroutine(Countdown(_duration));
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	IEnumerator Countdown(int t)
	{
		for (float timer = t ; timer >= 0; timer -= Time.deltaTime)
			yield return 0;
		
		gameObject.Recycle ();
	}
}
