﻿using UnityEngine;
using System.Collections;

public class VolumeButton : MonoBehaviour {

	public GameObject volumeDisableSprite;
	// Use this for initialization
	void Start () {
		gameObject.GetComponent<tk2dUIItem>().OnRelease += OnVolumeButtonRelease;
	}

	void OnVolumeButtonRelease(){
		if(AudioListener.pause)
			volumeDisableSprite.SetActive(true);
		else
			volumeDisableSprite.SetActive(false);
	}

}
