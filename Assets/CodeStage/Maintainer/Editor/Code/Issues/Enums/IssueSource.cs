﻿namespace CodeStage.Maintainer.Issues
{
	public enum IssueSource : byte
	{
		Scene = 5,
		Prefab = 10,
	}
}