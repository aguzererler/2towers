using UnityEditor;
using UnityEngine;
using System.Collections;

namespace LocalizationEditor
{
    public class LEMenu : EditorWindow
    {
        const string menuItemLocation = "Window/" + LEConstants.MenuLbl;

        /// <summary>
        /// Displays the Localization Editor Window
        /// </summary>
        [MenuItem(menuItemLocation + "/" + LEConstants.LEEditorWindowLbl, false, 250)]
        public static void ShowLEManagerWindow()
        {
            var window = EditorWindow.GetWindow<LEStringTableEditorWindow> (false, LEConstants.LEEditorWindowLbl);
            window.LoadSettings();
            window.Show();
        }

        
        /** Divider **/


        /// <summary>
        /// Starts a sync with a spreadsheet.
        /// </summary>
        [MenuItem(menuItemLocation + "/" + LEConstants.SyncWindowTitleLbl, false, 300)]
        public static void DoSyncWithSpreadsheet()
        {
            // User is trying to get a master copy of his languages to save to vcs
            // Any edits in the String Table View are saved to the local json language files
            //
            // Steps for syncing to Google Drive
            // * Save edits made to the string table from the LE Manager window
            // * Diff the dictionaries for all the languages
            // * Output results

            LEStringTableEditor.Save();
            
            LESettings settings = LESettings.Instance;
            
            // If the user has never synced, launch the choose sync method wizard
            if (settings.LastSyncType.Equals(LESettings.SyncType.None))
            {
                // Launch the dialog from the beginning
                var window = EditorWindow.GetWindow<LESyncExcel>(true, LEConstants.SyncDlgTitleLbl);
                window.LoadSettings(LEStringTableEditorWindow.DoSync);
                window.Show();
            }
            else
            {
                LEStringTableEditorWindow.DoSync();
            }
        }

        /// <summary>
        /// Clears the sync settings.
        /// </summary>
        [MenuItem(menuItemLocation + "/" + LEConstants.ClearSyncSettings, false, 301)]
        static void ClearSyncSettings()
        {
            LESettings settings = LESettings.Instance;
            
            settings.ExportedLocalSpreadsheetName = string.Empty;
            settings.ExportedGoogleSpreadsheetPath = string.Empty;
            settings.ImportedGoogleSpreadsheetName = string.Empty;
            settings.ImportedLocalSpreadsheetName = string.Empty;
            settings.LastSyncType = LESettings.SyncType.None;
            
            settings.Save();
            
            EditorUtility.DisplayDialog(LEConstants.SyncSettingClearedTitle, 
                                        LEConstants.SyncSettingsMsg, 
                                        LEConstants.OkLbl);
        }


        /** Divider **/
        
        
        /// <summary>
        /// Displays the Localization Editor Import Spreadsheet Wizard
        /// </summary>
        [MenuItem(menuItemLocation + "/" + LEConstants.ImportSpreadsheetLbl, false, 312)]
        public static void DoImportSpreadsheet()
        {
            // Make sure the String Table Manager window updates after an import
            LEImportExcel.OnImportComplete += LEStringTableEditorWindow.OnComplete;

            var window = EditorWindow.GetWindow<LocalizationEditor.LEImportExcel>(true, LEConstants.ImportSpreadsheetLbl);
            window.LoadSettings();
            window.Show();
        }

        /// <summary>
        /// Displays the Localization Editor Export Spreadsheet Wizard
        /// </summary>
        [MenuItem(menuItemLocation + "/" + LEConstants.ExportSpreadsheetLbl, false, 313)]
        public static void DoExportSpreadsheet()
        {
            var window = EditorWindow.GetWindow<LocalizationEditor.LEExportExcel>(true, LEConstants.ExportSpreadsheetLbl);
            window.LoadSettings();
            window.Show();
        }


        /** Divider **/


        [MenuItem(menuItemLocation + "/" + LEConstants.SceneCrawlerLbl, false, 324)]
        public static void DoLESceneCrawl()
        {
            LESceneCrawler.ProcessScene();
        }

        [MenuItem(menuItemLocation + "/" + LEConstants.GenKeysLbl, false, 325)]
        public static void DoGenStaticKeys()
        {
            LECodeGen.GenStaticKeysClass(LEStringTableEditor.MasterKeys);
        }

        /** Divider **/

        [MenuItem(menuItemLocation + "/" + LEConstants.ForumMenu, false, 338)]
        private static void LEForumPost()
        {
            Application.OpenURL(LEConstants.ForumURL);
        }
        
        [MenuItem(menuItemLocation + "/" + LEConstants.DocsMenu, false, 339)]
        private static void LEFreeDocs()
        {
            Application.OpenURL(LEConstants.DocURL);
        }


		/** Divider **/


        [MenuItem(menuItemLocation + "/" + LEConstants.RateLELbl, false, 360)]
        private static void LERate()
        {
            Application.OpenURL(LEConstants.RateMeURL);
        }

        [MenuItem(menuItemLocation + "/" + LEConstants.ContactMenu + "/" + LEConstants.EmailMenu, false, 361)]
        private static void LEEmail()
        {
            Application.OpenURL(LEConstants.Contact);
        }
        
        [MenuItem(menuItemLocation + "/" + LEConstants.ContactMenu + "/" + LEConstants.TwitterMenu, false, 362)]
        private static void LETwitter()
        {
            Application.OpenURL(LEConstants.Twitter);
        }
    }
}
