using UnityEngine;
using System;
using OfficeOpenXml;
using System.Linq;
using System.Collections.Generic;
using System.IO;

namespace LocalizationEditor
{
    using LELangDict = System.Collections.Generic.Dictionary<string, string>;
    using LELangDictCollection = System.Collections.Generic.Dictionary<string, System.Collections.Generic.Dictionary<string, string>>;

    public class EPPExcelHelper
    {
        const string KEYS_RANGE = "a2:a";
        const string LANGUAGES_RANGE = "b1:az1";

        ExcelPackage _excelPackage;

        public EPPExcelHelper(string filePath) : this(filePath, false)
        {
        }

        public EPPExcelHelper(string filePath, bool useNewWorkbook)
        {
            FileInfo newFile = new FileInfo(filePath);
            if (newFile.Exists && useNewWorkbook)
            {
                newFile.Delete();  // ensures we create a new workbook
                newFile = new FileInfo(filePath);
            }
            _excelPackage = new ExcelPackage(newFile);
        }

        public void WriteAllStringTables(string sheetName, LELangDictCollection all_langs)
        {
            try
            {
                ExcelWorksheet worksheet = _excelPackage.Workbook.Worksheets.Add(sheetName);

                // Start writing the value of the first key
                int row = 2;
                int col = 2;
                // First run through the first dictionary writes out the keys
                // This assumes all languages have all keys
                bool writeKeys = true;

                foreach (var lang in all_langs)
                {
                    WriteLangDict(row, col, lang.Key, lang.Value, worksheet, writeKeys);
                    // The first run through write out the keys then flip the bool
                    writeKeys = false;
                    col++;
                }
                _excelPackage.Save();
            }
            catch(Exception ex)
            {
                Debug.LogException(ex);
            }
        }

        public List<string> GetSheetNames()
        {
            List<string> sheetNames = new List<string>();

            foreach (var ws in _excelPackage.Workbook.Worksheets)
                sheetNames.Add(ws.Name);

            return sheetNames;
        }

        void WriteLangDict(int row_, int col_, string lang, LELangDict langDict, ExcelWorksheet worksheet, bool writeKeys)
        {
            int row = row_;
            int col = col_;
            // writing the column title which is the language code
            worksheet.Cells[1,col].Value = lang;

            foreach (var l_str in langDict)
            {
                if (writeKeys)
                    worksheet.Cells[row, 1].Value = l_str.Key;

                worksheet.Cells[row,col].Value = l_str.Value;
                row++;
            }
        }

        struct FieldInfo
        {
            public string name;
            public string cellCol;
            public int cellColInt;
            public string cellRow;

            public string cellID
            {
                get { return cellCol + cellRow; }
                private set {}
            }
        }

        public LELangDictCollection GetLanguagesFromWorkbook()
        {
            LELangDictCollection allLangs = new LELangDictCollection();

            try
            {
                foreach (ExcelWorksheet worksheet in _excelPackage.Workbook.Worksheets)
                {
                    var langCellQuery = (from cell in worksheet.Cells[LANGUAGES_RANGE]
                                         where cell != null && !string.IsNullOrEmpty(cell.GetValue<string>())
                                         select cell);

                    var langDict = new Dictionary<int, string>();
                    foreach(var langCell in langCellQuery)
                        langDict.Add(langCell.End.Column, langCell.GetValue<string>().Trim());

                    var keyCellQuery = (from cell in worksheet.Cells[KEYS_RANGE]
                                        where cell != null && !string.IsNullOrEmpty(cell.GetValue<string>())
                                        select cell);

                    var keyDict = new Dictionary<int, string>();
                    foreach(var keyCell in keyCellQuery)
                        keyDict.Add(keyCell.End.Row, keyCell.GetValue<string>().Trim());

                    foreach(var currentLang in langDict)
                    {
                        // Pull out the language code
                        string lang_code = currentLang.Value;

                        // If the language code already exists, move on
                        if (allLangs.ContainsKey(lang_code))
                        {
                            Debug.LogError(string.Format(LEConstants.LangAlreadyDefinedFormat, lang_code));
                            continue;
                        }

                        // Pull out all the keys and values for this language
                        LELangDict string_table = new LELangDict();

                        foreach(var currentKey in keyDict)
                        {
                            string key = currentKey.Value;
                            if (string_table.ContainsKey(key))
                            {
                                Debug.LogWarning(string.Format(LEConstants.DuplicateKeyFormat, key));
                                continue;
                            }

                            string value = string.Empty;
                            ExcelRange valueCell = worksheet.Cells[currentKey.Key, currentLang.Key];

                            if (valueCell != null && valueCell.Value != null)
                                value = valueCell.GetValue<string>();

                            string_table.Add(key, value);
                        }
                        allLangs.Add(lang_code, string_table);
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.LogError(ex);
            }

            return allLangs;
        }
    }
}
