using UnityEngine;
using UnityEditor;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace LocalizationEditor
{
    using LELangDict = System.Collections.Generic.Dictionary<string, string>;
    using LELangDictCollection = System.Collections.Generic.Dictionary<string, System.Collections.Generic.Dictionary<string, string>>;

    public class LEExportExcel : EditorWindow
    {
        public enum View
        {
            Default,
            LocalFile,
            LaunchAuthURL,
            Authenticate,
            UploadExisting,
            UploadNew,
            ImportComplete,

            None
        }

        GUIStyle headerStyle = null;
        GUIStyle linkStyle = null;
        GUIStyle buttonStyle = null;
        GUIStyle labelStyle = null;
        GUIStyle comboBoxStyle = null;
        GUIStyle textFieldStyle = null;

        GUIContent _content;
        GUIContent content 
        {
            get
            {
                if (_content == null)
                    _content = new GUIContent();
                return _content;
            }
        }

        Vector2 size;

        string spreadsheetPath = "";
        string spreadsheetName = "";

        string accessCode = "";
        int downloadSelectionIndex = 0;

        static View currentView;
        static View nextView;
        static View viewAfterAuth = View.None;

        public static string googleSheetImportName;
        public static int googleSheetImportIndex;

        LEDrawHelper _drawHelper;
        LEDrawHelper drawHelper
        {
            get
            {
                if (_drawHelper == null)
                    _drawHelper = new LEDrawHelper(this, 2f, 10f, 10f, 10f, 20f);

                return _drawHelper;
            }
        }

        void SetStyles()
        {
            if (headerStyle == null || string.IsNullOrEmpty(headerStyle.name))
            {
                headerStyle = new GUIStyle(GUI.skin.label);
                headerStyle.fontStyle = FontStyle.Bold;
                headerStyle.fontSize = 16;
            }
            
            if (buttonStyle == null || string.IsNullOrEmpty(buttonStyle.name))
            {
                buttonStyle = new GUIStyle(GUI.skin.button);
                buttonStyle.fontSize = 14;
                buttonStyle.padding = new RectOffset(15, 15, 5, 5);
            }
            
            if (labelStyle == null || string.IsNullOrEmpty(labelStyle.name))
            {
                labelStyle = new GUIStyle(GUI.skin.label);
                labelStyle.fontSize = 14;
                labelStyle.padding = new RectOffset(0, 0, 3, 3);
            }
            
            if (linkStyle == null || string.IsNullOrEmpty(linkStyle.name))
            {
                linkStyle = new GUIStyle(GUI.skin.label);
                linkStyle.fontSize = 16;
                linkStyle.alignment = TextAnchor.MiddleCenter;
                linkStyle.normal.textColor = Color.blue;
            }
            
            if (comboBoxStyle == null || string.IsNullOrEmpty(comboBoxStyle.name))
            {
                comboBoxStyle = new GUIStyle(EditorStyles.popup);
                comboBoxStyle.fontSize = 14;
                comboBoxStyle.fixedHeight = 24f;
            }
            
            if (textFieldStyle == null || string.IsNullOrEmpty(textFieldStyle.name))
            {
                textFieldStyle = new GUIStyle(GUI.skin.textField);
                textFieldStyle.fontSize = 14;
                textFieldStyle.fixedHeight = 22f;
                textFieldStyle.alignment = TextAnchor.UpperLeft;
                textFieldStyle.margin = new RectOffset(0, 0, 0, 8);
            }
        }

        public void LoadSettings(View view = View.Default)
        {
            LESettings settings = LESettings.Instance;
            spreadsheetPath = settings.ExportedGoogleSpreadsheetPath;

            currentView = view;
            nextView = view;

            this.minSize = new Vector2(420, 250);
            this.maxSize = this.minSize;
        }

        void OnGUI()
        {
            SetStyles();

            size = Vector2.zero;

            drawHelper.ResetToTop();

            if (currentView.Equals(View.Default))
                DrawDefaultView();
            else if (currentView.Equals(View.LaunchAuthURL))
                DrawLaunchAuthURL();
            else if (currentView.Equals(View.Authenticate))
                DrawAuthenticateView();
            else if (currentView.Equals(View.UploadExisting))
                DrawUploadExistingView();
            else if (currentView.Equals(View.UploadNew))
                DrawUploadNewView();
            else if (currentView.Equals(View.LocalFile))
                DrawExportLocalFile();
            else if (currentView.Equals(View.ImportComplete))
                DrawImportCompleteView();

            currentView = nextView;
        }

        void DrawDefaultView()
        {
            content.text = LEConstants.ExportDlgTitleLbl;
            drawHelper.TryGetCachedSize(LEConstants.SizeExportDlgTitleLblKey, content, headerStyle, out size);
            EditorGUI.LabelField(new Rect(drawHelper.CenteredOnLine(size.x), drawHelper.TopOfLine(), size.x, size.y), content, headerStyle);

            drawHelper.NewLine(2);

            content.text = LEConstants.ExportToLocalFileBtn;
            drawHelper.TryGetCachedSize(LEConstants.SizeExportToLocalFileBtnKey, content, buttonStyle, out size);
            if (GUI.Button(new Rect(drawHelper.CenteredOnLine(size.x), drawHelper.TopOfLine(), size.x, size.y), content, buttonStyle))
                nextView = View.LocalFile;

            drawHelper.NewLine(2.5f);

            content.text = LEConstants.ExportToSheetsBtn;
            drawHelper.TryGetCachedSize(LEConstants.SizeExportToSheetsBtnKey, content, buttonStyle, out size);
            if (GUI.Button(new Rect(drawHelper.CenteredOnLine(size.x), drawHelper.TopOfLine(), size.x, size.y), content, buttonStyle))
            {
                if (HasAuthenticated())
                {
                    nextView = View.UploadExisting;
                    GoogleDriveHelper.Instance.GetSpreadsheetList();
                }
                else
                {
                    nextView = View.LaunchAuthURL;
                    viewAfterAuth = View.UploadExisting;
                }
            }

            drawHelper.NewLine(2.5f);

            content.text = LEConstants.ExportToNewSheetBtn;
            drawHelper.TryGetCachedSize(LEConstants.SizeExportToNewSheetBtnKey, content, buttonStyle, out size);
            if (GUI.Button(new Rect(drawHelper.CenteredOnLine(size.x), drawHelper.TopOfLine(), size.x, size.y), content, buttonStyle))
            {
                if (HasAuthenticated())
                {
                    nextView = View.UploadNew;
                }
                else
                {
                    nextView = View.LaunchAuthURL;
                    viewAfterAuth = View.UploadNew;
                }
            }

            drawHelper.NewLine(2.5f);

            content.text = LEConstants.ReauthenticateGoogleLbl;
            drawHelper.TryGetCachedSize(LEConstants.SizeReauthenticateGoogleLblKey, content, buttonStyle, out size);
            if (GUI.Button(new Rect(drawHelper.CenteredOnLine(size.x), drawHelper.TopOfLine(), size.x, size.y), content, buttonStyle))
            {
                nextView = View.LaunchAuthURL;
            }
        }

       void DrawExportLocalFile()
        {
            content.text = LEConstants.ExportExcelWorkbookLbl;
            drawHelper.TryGetCachedSize(LEConstants.SizeExportExcelWorkbookLblKey, content, headerStyle, out size);
            EditorGUI.LabelField(new Rect(drawHelper.CenteredOnLine(size.x), drawHelper.TopOfLine(), size.x, size.y), content, headerStyle);

            drawHelper.NewLine(2);

            content.text = LEConstants.ExcelFilePathLbl;
            drawHelper.TryGetCachedSize(LEConstants.SizeExcelFilePathLblKey, content, labelStyle, out size);
            EditorGUI.LabelField(new Rect(drawHelper.CurrentLinePosition, drawHelper.TopOfLine(), size.x, size.y), content, labelStyle);
            drawHelper.CurrentLinePosition += size.x + 2f;
            drawHelper.NewLine();

            spreadsheetPath = EditorGUI.TextField(new Rect(drawHelper.CurrentLinePosition, drawHelper.TopOfLine(), drawHelper.FullSeparatorWidth(), 
                                                           textFieldStyle.fixedHeight), spreadsheetPath, textFieldStyle);
            drawHelper.CurrentLinePosition += size.x + 2f;
            drawHelper.NewLine(1.1f);

            content.text = LEConstants.BrowseLbl;
            drawHelper.TryGetCachedSize(LEConstants.SizeBrowseLblKey, content, buttonStyle, out size);
            if (GUI.Button(new Rect(drawHelper.CurrentLinePosition, drawHelper.TopOfLine(), size.x, size.y), content, buttonStyle))
            {
                string newSpreadSheetPath = EditorUtility.OpenFilePanel(LEConstants.OpenWBLbl, spreadsheetPath, string.Empty);
                if (!string.IsNullOrEmpty(newSpreadSheetPath) && !newSpreadSheetPath.Equals(spreadsheetPath))
                    spreadsheetPath = newSpreadSheetPath;
                GUI.FocusControl(string.Empty);
            }

            // Draw Back & Export Buttons
            content.text = LEConstants.BackBtn;
            drawHelper.TryGetCachedSize(LEConstants.SizeBackBtnKey, content, buttonStyle, out size);
            if (GUI.Button(new Rect(drawHelper.CurrentLinePosition, position.height-size.y-drawHelper.BottomBuffer, size.x, size.y), content, buttonStyle))
                nextView = View.Default;

            content.text = LEConstants.ExportBtn;
            drawHelper.TryGetCachedSize(LEConstants.SizeExportBtnKey, content, buttonStyle, out size);
            if (GUI.Button(new Rect(position.width-size.x-drawHelper.LeftBuffer, position.height-size.y-drawHelper.BottomBuffer, size.x, size.y), content, buttonStyle))
            {
                LESettings settings = LESettings.Instance;
                string oldValue = settings.ExportedGoogleSpreadsheetPath;
                settings.ExportedGoogleSpreadsheetPath = spreadsheetPath;
                
                if (!oldValue.Equals(spreadsheetPath))
                    settings.Save();
                
                // take the local languages dictionary
                // write it out to an excel file
                EPPExcelHelper excelHelper = new EPPExcelHelper(spreadsheetPath, true);
                excelHelper.WriteAllStringTables(LEConstants.DefaultSheetName, LEStringTableEditor.AllLangsLogical);
                nextView = View.ImportComplete;
            }
        }

        void DrawLaunchAuthURL()
        {
            content.text = LEConstants.AuthWithGoogleLbl;
            drawHelper.TryGetCachedSize(LEConstants.SizeAuthWithGoogleLblKey, content, headerStyle, out size);
            EditorGUI.LabelField(new Rect(drawHelper.CenteredOnLine(size.x), drawHelper.TopOfLine(), size.x, size.y), content, headerStyle);
            
            drawHelper.NewLine(2);

            content.text = LEConstants.GoogleAuthInstruction1_1;
            drawHelper.TryGetCachedSize(LEConstants.SizeGoogleAuthInstruction1_1Key, content, labelStyle, out size);
            EditorGUI.LabelField(new Rect(drawHelper.CurrentLinePosition, drawHelper.TopOfLine(), size.x, size.y), content, labelStyle);

            drawHelper.NewLine();

            content.text = LEConstants.GoogleAuthInstruction1_2;
            drawHelper.TryGetCachedSize(LEConstants.SizeGoogleAuthInstruction1_2Key, content, labelStyle, out size);
            EditorGUI.LabelField(new Rect(drawHelper.CurrentLinePosition, drawHelper.TopOfLine(), size.x, size.y), content, labelStyle);

            drawHelper.NewLine(2);

            content.text = LEConstants.GoogleAuthInstruction2_1;
            drawHelper.TryGetCachedSize(LEConstants.SizeGoogleAuthInstruction2_1Key, content, labelStyle, out size);
            EditorGUI.LabelField(new Rect(drawHelper.CurrentLinePosition, drawHelper.TopOfLine(), size.x, size.y), content, labelStyle);

            drawHelper.NewLine();

            content.text = LEConstants.GoogleAuthInstruction2_2;
            drawHelper.TryGetCachedSize(LEConstants.SizeGoogleAuthInstruction2_2Key, content, labelStyle, out size);
            EditorGUI.LabelField(new Rect(drawHelper.CurrentLinePosition, drawHelper.TopOfLine(), size.x, size.y), content, labelStyle);


            // Draw Back & GOTO Auth buttons
            content.text = LEConstants.BackBtn;
            drawHelper.TryGetCachedSize(LEConstants.SizeBackBtnKey, content, buttonStyle, out size);
            if (GUI.Button(new Rect(drawHelper.CurrentLinePosition, position.height-size.y-drawHelper.BottomBuffer, size.x, size.y), content, buttonStyle))
                nextView = View.Default;
            
            content.text = LEConstants.GoToAuthURLBtn;
            drawHelper.TryGetCachedSize(LEConstants.SizeGoToAuthURLBtnKey, content, buttonStyle, out size);
            if (GUI.Button(new Rect(position.width-size.x-drawHelper.LeftBuffer, position.height-size.y-drawHelper.BottomBuffer, size.x, size.y), content, buttonStyle))
            {
                GoogleDriveHelper.Instance.RequestAuthFromUser();
                nextView = View.Authenticate;
            }
        }

        void DrawAuthenticateView()
        {
            content.text = LEConstants.AuthWithGoogleLbl;
            drawHelper.TryGetCachedSize(LEConstants.SizeAuthWithGoogleLblKey, content, headerStyle, out size);
            EditorGUI.LabelField(new Rect(drawHelper.CenteredOnLine(size.x), drawHelper.TopOfLine(), size.x, size.y), content, headerStyle);
            
            drawHelper.NewLine(2);

            content.text = LEConstants.EnterAccessCodeLbl;
            drawHelper.TryGetCachedSize(LEConstants.SizeEnterAccessCodeLblKey, content, labelStyle, out size);
            EditorGUI.LabelField(new Rect(drawHelper.CurrentLinePosition, drawHelper.TopOfLine(), size.x, size.y), content, labelStyle);

            drawHelper.NewLine(1.1f);

            accessCode = EditorGUI.TextField(new Rect(drawHelper.CurrentLinePosition, drawHelper.TopOfLine(), drawHelper.FullSeparatorWidth(), size.y), accessCode, textFieldStyle);

            // Draw Back & Set Code Buttons
            content.text = LEConstants.BackBtn;
            drawHelper.TryGetCachedSize(LEConstants.SizeBackBtnKey, content, buttonStyle, out size);
            if (GUI.Button(new Rect(drawHelper.CurrentLinePosition, position.height-size.y-drawHelper.BottomBuffer, size.x, size.y), content, buttonStyle))
                nextView = View.LaunchAuthURL;
            
            content.text = LEConstants.SetCodeBtn;
            drawHelper.TryGetCachedSize(LEConstants.SizeSetCodeBtnKey, content, buttonStyle, out size);
            if (!string.IsNullOrEmpty(accessCode) && GUI.Button(new Rect(position.width-size.x-drawHelper.LeftBuffer, position.height-size.y-drawHelper.BottomBuffer, size.x, size.y), content, buttonStyle))
            {
                GoogleDriveHelper.Instance.SetAccessCode(accessCode);
                GoogleDriveHelper.Instance.GetSpreadsheetList();

                if (!viewAfterAuth.Equals(View.None))
                {
                    nextView = viewAfterAuth;
                    viewAfterAuth = View.None;
                }
                else
                {
                    nextView = View.Default;
                }

                GUI.FocusControl(string.Empty);
            }
        }

        void DrawUploadExistingView()
        {
            content.text = LEConstants.ExportGoogleSheetLbl;
            drawHelper.TryGetCachedSize(LEConstants.SizeExportGoogleSheetLblKey, content, headerStyle, out size);
            EditorGUI.LabelField(new Rect(drawHelper.CenteredOnLine(size.x), drawHelper.TopOfLine(), size.x, size.y), content, headerStyle);
            
            drawHelper.NewLine(2);

            content.text = LEConstants.SelectSpreadSheetLbl;
            drawHelper.TryGetCachedSize(LEConstants.SizeSelectSpreadSheetLblKey, content, labelStyle, out size);
            EditorGUI.LabelField(new Rect(drawHelper.CurrentLinePosition, drawHelper.TopOfLine(), size.x, size.y), content, labelStyle);

            drawHelper.NewLine(1.1f);

            downloadSelectionIndex = EditorGUI.Popup(new Rect(drawHelper.CurrentLinePosition, drawHelper.TopOfLine(), drawHelper.FullSeparatorWidth(), size.y),
                downloadSelectionIndex, GoogleDriveHelper.Instance.SpreadSheetNames, comboBoxStyle);

            // Draw Back & Upload Buttons
            content.text = LEConstants.BackBtn;
            drawHelper.TryGetCachedSize(LEConstants.SizeBackBtnKey, content, buttonStyle, out size);
            if (GUI.Button(new Rect(drawHelper.CurrentLinePosition, position.height-size.y-drawHelper.BottomBuffer, size.x, size.y), content, buttonStyle))
                nextView = View.Default;
            
            content.text = LEConstants.UploadBtn;
            drawHelper.TryGetCachedSize(LEConstants.SizeUploadBtnKey, content, buttonStyle, out size);
            if (GUI.Button(new Rect(position.width-size.x-drawHelper.LeftBuffer, position.height-size.y-drawHelper.BottomBuffer, size.x, size.y), content, buttonStyle))
            {
                googleSheetImportIndex = downloadSelectionIndex;
                googleSheetImportName = GoogleDriveHelper.Instance.SpreadSheetNames[downloadSelectionIndex];
                string tempSheetPath = FileUtil.GetUniqueTempPathInProject() + googleSheetImportName + ".xlsx";
                
                // take the local languages dictionary
                // write it out to an excel file so we can upload to sheets
                EPPExcelHelper excelHelper = new EPPExcelHelper(tempSheetPath, true);
                // we need to save the sheet name if it was imported
                // otherwise we need a default one if they go from
                // LEEditor to exported sheet
                excelHelper.WriteAllStringTables(LEConstants.DefaultSheetName, LEStringTableEditor.AllLangsLogical);
                GoogleDriveHelper.Instance.UploadToExistingSheet(googleSheetImportName, tempSheetPath);
                
                nextView = View.ImportComplete;
            }
        }

        void DrawUploadNewView()
        {
            content.text = LEConstants.ExportNewGoogleSheetLbl;
            drawHelper.TryGetCachedSize(LEConstants.SizeExportToNewSheetBtnKey, content, headerStyle, out size);
            EditorGUI.LabelField(new Rect(drawHelper.CenteredOnLine(size.x), drawHelper.TopOfLine(), size.x, size.y), content, headerStyle);
            
            drawHelper.NewLine(2);

            content.text = LEConstants.NewSheetFileNameLbl;
            drawHelper.TryGetCachedSize(LEConstants.SizeNewSheetFileNameLblKey, content, labelStyle, out size);
            EditorGUI.LabelField(new Rect(drawHelper.CurrentLinePosition, drawHelper.TopOfLine(), size.x, size.y), content, labelStyle);
            
            drawHelper.NewLine(1.1f);

            spreadsheetName = EditorGUI.TextField(new Rect(drawHelper.CurrentLinePosition, drawHelper.TopOfLine(), drawHelper.FullSeparatorWidth(), size.y),
                spreadsheetName, textFieldStyle);
           
            // Draw Back & Upload Buttons
            content.text = LEConstants.BackBtn;
            drawHelper.TryGetCachedSize(LEConstants.SizeBackBtnKey, content, buttonStyle, out size);
            if (GUI.Button(new Rect(drawHelper.CurrentLinePosition, position.height-size.y-drawHelper.BottomBuffer, size.x, size.y), content, buttonStyle))
                nextView = View.Default;
            
            content.text = LEConstants.UploadBtn;
            drawHelper.TryGetCachedSize(LEConstants.SizeUploadBtnKey, content, buttonStyle, out size);
            if (!string.IsNullOrEmpty(spreadsheetName) &&
                GUI.Button(new Rect(position.width-size.x-drawHelper.LeftBuffer, position.height-size.y-drawHelper.BottomBuffer, size.x, size.y), content, buttonStyle))
            {
                string tempSheetPath = FileUtil.GetUniqueTempPathInProject() + "exportnewgoog_" + spreadsheetName + ".xlsx";
                
                // take the local languages dictionary
                // write it out to an excel file so we can upload to sheets
                EPPExcelHelper excelHelper = new EPPExcelHelper(tempSheetPath, true);
                // we need to save the sheet name if it was imported
                // otherwise we need a default one if they go from
                // LEEditor to exported sheet
                excelHelper.WriteAllStringTables(LEConstants.DefaultSheetName, LEStringTableEditor.AllLangsLogical);
                GoogleDriveHelper.Instance.UploadNewSheet(tempSheetPath, spreadsheetName);
                
                nextView = View.ImportComplete;
            }
        }

        void DrawImportCompleteView()
        {
            content.text = LEConstants.ExportCompleteLbl;
            drawHelper.TryGetCachedSize(LEConstants.SizeExportCompleteLblKey, content, headerStyle, out size);
            EditorGUI.LabelField(new Rect(drawHelper.CenteredOnLine(size.x), drawHelper.TopOfLine(), size.x, size.y), content, headerStyle);
            
            drawHelper.NewLine(2);

            content.text = LEConstants.ExportMsg1;
            drawHelper.TryGetCachedSize(LEConstants.SizeExportMsg1Key, content, labelStyle, out size);
            EditorGUI.LabelField(new Rect(drawHelper.CenteredOnLine(size.x), drawHelper.TopOfLine(), size.x, size.y), content, labelStyle);

            drawHelper.NewLine(1.1f);

            content.text = LEConstants.ExportMsg2;
            drawHelper.TryGetCachedSize(LEConstants.SizeExportMsg2Key, content, labelStyle, out size);
            EditorGUI.LabelField(new Rect(drawHelper.CenteredOnLine(size.x), drawHelper.TopOfLine(), size.x, size.y), content, labelStyle);
            
            // Draw rate box
            float heightOfBox = 50f;
            float topOfBox = this.position.height * .5f + 5f;
            float bottomOfBox = topOfBox + heightOfBox;

            content.text = LEConstants.ForumLinkText;
            drawHelper.TryGetCachedSize(LEConstants.SizeForumLinkTextKey, content, linkStyle, out size);

            float widthOfBox = size.x + 10f;
            float leftOfBox = (this.position.width - widthOfBox) / 2f;

            if (GUI.Button(new Rect(leftOfBox + 6f, bottomOfBox - size.y - 2f, size.x, size.y), content, linkStyle))
            {
                Application.OpenURL(LEConstants.ForumURL);
            }

            content.text = LEConstants.RateMeText;
            if (GUI.Button(new Rect(leftOfBox + 6f, topOfBox + 3f, size.x, size.y), content, linkStyle))
            {
                Application.OpenURL(LEConstants.RateMeURL);
            }

            // Draw Export Again & Close Buttons
            content.text = LEConstants.ExportAgainBtn;
            drawHelper.TryGetCachedSize(LEConstants.SizeExportAgainBtnKey, content, buttonStyle, out size);
            if (GUI.Button(new Rect(drawHelper.CurrentLinePosition, position.height-size.y-drawHelper.BottomBuffer, size.x, size.y), content, buttonStyle))
                nextView = View.Default;
            
            content.text = LEConstants.CloseBtn;
            drawHelper.TryGetCachedSize(LEConstants.SizeCloseBtnKey, content, buttonStyle, out size);
            if (GUI.Button(new Rect(position.width-size.x-drawHelper.LeftBuffer, position.height-size.y-drawHelper.BottomBuffer, size.x, size.y), content, buttonStyle))
            {
                Close();
            }
        }

        bool HasAuthenticated()
        {
            return GoogleDriveHelper.Instance.HasAuthenticated();
        }
    }
}
