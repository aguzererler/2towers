using UnityEngine;
using UnityEditor;
using System;
using Google.GData.Client;

namespace LocalizationEditor
{
    public class LEOAuthHelper
    {
        const string CLIENT_ID = "165797088751-ac9gtdg5ail3tsf4e7gqo3a8fhdm29u5.apps.googleusercontent.com";
        const string CLIENT_SECRET = "qifyVW3GyMSn0-o83wS5Sv1X";
        const string SCOPE = "https://www.googleapis.com/auth/drive";
        const string REDIRECT_URI = "urn:ietf:wg:oauth:2.0:oob";
        const int ACCESS_TOKEN_TIMEOUT = 3600;

        OAuth2Parameters oauth2Params;

        public string AccessToken {
            get { return oauth2Params.AccessToken; }
            private set {}
        }

        public LEOAuthHelper()
        {
            oauth2Params = new OAuth2Parameters();

            oauth2Params.ClientId = CLIENT_ID;
            oauth2Params.ClientSecret = CLIENT_SECRET;
            oauth2Params.RedirectUri = REDIRECT_URI;
            oauth2Params.Scope = SCOPE;
        }

        public bool HasAuthenticated()
        {
            LESettings settings = LESettings.Instance;
            return !string.IsNullOrEmpty(settings.AccessTokenTimeout);
        }

        public string GetAuthURL()
        {
            return OAuthUtil.CreateOAuth2AuthorizationUrl (oauth2Params);
        }

        public void SetAccessCode (string code)
        {
            if (oauth2Params != null) {
                oauth2Params.AccessCode = code;
                OAuthUtil.GetAccessToken (oauth2Params);
                SaveTokens();
            }
        }

        public void Init()
        {
            if (HasAuthenticated()) {
                LESettings settings = LESettings.Instance;
                string accessToken = settings.AccessTokenKey;
                string refreshToken = settings.RefreshTokenKey;

                oauth2Params.AccessToken = accessToken;
                oauth2Params.RefreshToken = refreshToken;

                string timeString = settings.AccessTokenTimeout;
                DateTime lastRefreshed = DateTime.MinValue;

                if (!timeString.Equals (string.Empty))
                    DateTime.Parse (timeString);

                TimeSpan timeSinceRefresh = DateTime.Now.Subtract (lastRefreshed);

                if (timeSinceRefresh.TotalSeconds >= ACCESS_TOKEN_TIMEOUT)
                    RefreshAccessToken();
            }
        }

        public static void ClearAuth()
        {
            LESettings settings = LESettings.Instance;

            settings.AccessTokenTimeout = string.Empty;
            settings.AccessTokenKey = string.Empty;
            settings.RefreshTokenKey = string.Empty;

            // Clear sync settings if it was with google
            if (settings.LastSyncType.Equals(LESettings.SyncType.Google))
                settings.LastSyncType = LESettings.SyncType.None;

            settings.ExportedGoogleSpreadsheetPath = string.Empty;
            settings.ImportedGoogleSpreadsheetName = string.Empty;
            settings.SyncGoogleSpreadsheetName = string.Empty;
            
            settings.Save();
        }

        void RefreshAccessToken()
        {
            OAuthUtil.RefreshAccessToken (oauth2Params);
            SaveTokens();
        }

        void SaveTokens()
        {
            LESettings settings = LESettings.Instance;

            settings.AccessTokenTimeout = DateTime.Now.ToString();
            settings.AccessTokenKey = oauth2Params.AccessToken;
            settings.RefreshTokenKey = oauth2Params.RefreshToken;

            settings.Save();
        }
    }
}
