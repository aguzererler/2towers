using UnityEngine;
using UnityEditor;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace LocalizationEditor
{
    using LELangDict = System.Collections.Generic.Dictionary<string, string>;
    using LELangDictCollection = System.Collections.Generic.Dictionary<string, System.Collections.Generic.Dictionary<string, string>>;

    using LEDiffDict = System.Collections.Generic.Dictionary<string, LocalizationEditor.LEStringDiff>;
    using LEDiffDictCollection = System.Collections.Generic.Dictionary<string, System.Collections.Generic.Dictionary<string, LocalizationEditor.LEStringDiff>>;

    // Left is false, Right is true
    public enum DiffChoice
    {
        Left,
        Right,
        None
    }

    public enum ALLSide
    {
        AllLeft,
        AllRight,
        None
    }

    public class LEStringDiff
    {
        public LEStringDiff(string l, string r)
        {
            LeftValue = l;
            RightValue = r;
        }

        public string LeftValue;
        public string RightValue;
    }

    public class LESyncHelper
    {
        public delegate void SyncDoneAction();
        public static SyncDoneAction OnSyncComplete;

        static LELangDictCollection _otherLangs;
        static LELangDictCollection otherLangs
        {
            set
            {
                _otherLangs = value;
            }
            get
            {
                if (_otherLangs == null)
                    _otherLangs = new LELangDictCollection();
                return _otherLangs;
            }
        }

        #region Diff Dictionaries
        static LEDiffDictCollection _allDiffs;
        public static LEDiffDictCollection AllDiffs
        {
            private set
            {
                _allDiffs = value;
            }

            get
            {
                if (_allDiffs == null)
                    _allDiffs = new LEDiffDictCollection();

                return _allDiffs;
            }
        }

        static LELangDictCollection _allLeftDiffs;
        public static LELangDictCollection AllLeftDiffs
        {
            private set
            {
                _allLeftDiffs = value;
            }

            get
            {
                if (_allLeftDiffs == null)
                    _allLeftDiffs = new LELangDictCollection();

                return _allLeftDiffs;
            }
        }

        static LELangDictCollection _allRightDiffs;
        public static LELangDictCollection AllRightDiffs
        {
            private set
            {
                _allRightDiffs = value;
            }

            get
            {
                if (_allRightDiffs == null)
                    _allRightDiffs = new LELangDictCollection();

                return _allRightDiffs;
            }
        }

        static LELangDictCollection _allNewLeftLangs;
        public static LELangDictCollection AllNewLeftLangs
        {
            private set
            {
                _allNewLeftLangs = value;
            }

            get
            {
                if (_allNewLeftLangs == null)
                    _allNewLeftLangs = new LELangDictCollection();

                return _allNewLeftLangs;
            }
        }

        static LELangDictCollection _allNewRightLangs;
        public static LELangDictCollection AllNewRightLangs
        {
            private set
            {
                _allNewRightLangs = value;
            }

            get
            {
                if (_allNewRightLangs == null)
                    _allNewRightLangs = new LELangDictCollection();

                return _allNewRightLangs;
            }
        }

        static List<string> _allLangKeys;
        public static List<string> AllLangKeys
        {
            private set { _allLangKeys = value; }
            get
            {
                if (_allLangKeys == null)
                    _allLangKeys = new List<string>();

                return _allLangKeys;
            }
        }
        #endregion

        #region Diff Status
        static HashSet<string> langDiffStatus = new HashSet<string>();
        public static bool LangHasDifferences(string lang)
        {
            return langDiffStatus.Contains(lang);
        }

        public static bool NeedsSync()
        {
            return langDiffStatus.Count > 0;
        }
        #endregion

        #region Diff Choices
        static ALLSide allSideChoice;
        public static void ChooseAllLeft()
        {
            allSideChoice = ALLSide.AllLeft;
            diffChoices.Clear();
        }

        public static void ChooseAllRight()
        {
            allSideChoice = ALLSide.AllRight;
            diffChoices.Clear();
        }

        static Dictionary<string, DiffChoice> diffChoices = new Dictionary<string, DiffChoice>();
        public static void ChooseLeft(string lang, string key)
        {
            diffChoices.TryAddOrUpdateValue(string.Format(LEConstants.MetaDataFormat, lang, key), DiffChoice.Left);
        }

        public static void ChooseRight(string lang, string key)
        {
            diffChoices.TryAddOrUpdateValue(string.Format(LEConstants.MetaDataFormat, lang, key), DiffChoice.Right);
        }

        public static DiffChoice GetChoiceFor(string lang, string key)
        {
            DiffChoice choice = DiffChoice.None;
            string choiceKey = string.Format(LEConstants.MetaDataFormat, lang, key);

            if (!diffChoices.TryGetValue(choiceKey, out choice))
            {
                if (allSideChoice.Equals(ALLSide.AllLeft))
                    choice = DiffChoice.Left;
                else if (allSideChoice.Equals(ALLSide.AllRight))
                    choice = DiffChoice.Right;
                else
                    choice = DiffChoice.None;
            }

            return choice;
        }

        public static void ChooseRightForAllLangs(string key)
        {
            AllLangKeys.ForEach(lang => ChooseRight(lang, key));
        }

        public static void ChooseLeftForAllLangs(string key)
        {
            AllLangKeys.ForEach(lang => ChooseLeft(lang, key));
        }
        #endregion

        #region Sync Logic
        public static void Sync(LELangDictCollection other)
        {
            ResetSync();

            // This is the language collection we have locally
            LELangDictCollection localLangs = LEStringTableEditor.AllLangsLogical;

            // This is the language collection we are syncing with
            // This can be from the cloud or a local file
            otherLangs = other;

            // If other is null, there's nothing to do
            if (other == null)
            {
                Debug.LogWarning(LEConstants.SyncCollectionNull);
                return;
            }

            //
            // New Languages: Left Side Only
            //
            foreach (var lang in localLangs)
            {
                LELangDict rightDict;
                var leftDiffs = new LELangDict();
                if (!otherLangs.TryGetValue(lang.Key, out rightDict))
                {
                    // a new language was added to the left side which isn't on right side
                    // add all the keys for the new language
                    foreach(var str in lang.Value)
                    {
                        //Debug.Log(string.Format("lang: {2} adding {0}:{1}", str.Key, str.Value, lang.Key));
                        leftDiffs.Add(str.Key, str.Value);
                    }
                    langDiffStatus.Add(lang.Key);

                    AllNewLeftLangs.Add(lang.Key, leftDiffs);
                    AllLangKeys.Add(lang.Key);
                }
            }
            //
            // ====================================
            //

            foreach (var lang in otherLangs)
            {
                LELangDict rightDict = lang.Value;
                LELangDict leftDict;

                // Create dictionaries to hold diff info
                var allLangDiffsForKey = new LEDiffDict();
                var allLeftDiffsForKey = new LELangDict();
                var allRightDiffsForKey = new LELangDict();

                if (!localLangs.TryGetValue(lang.Key, out leftDict))
                {
                    var rightDiffs = new LELangDict();

                    // a new language was added to the right side
                    // which isn't on the left side add all the keys
                    // for the new language
                    rightDiffs.Merge(lang.Value);
                    AllNewRightLangs.Add(lang.Key, rightDiffs);
                }
                else
                {
                    List<string> allKeys = new List<string>();

                    // Add the left keys
                    allKeys.AddRange(leftDict.Keys);

                    // Add the right keys that we haven't already added
                    foreach(var key in rightDict.Keys)
                    {
                        if (leftDict.ContainsKey(key))
                            continue;

                        allKeys.Add(key);
                    }

                    // Loop through all keys and determine the diff for each
                    foreach(var key in allKeys)
                    {
                        bool onLeft = leftDict.ContainsKey(key);
                        bool onRight = rightDict.ContainsKey(key);

                        if (onLeft && onRight)
                        {
                            // Key is on both and value is different

                            string left = leftDict[key];
                            string right = rightDict[key];

                            if (!left.Equals(right))
                                allLangDiffsForKey.Add(key, new LEStringDiff(left, right));
                        }
                        else if (onLeft)
                        {
                            // Key is on left only
                            allLeftDiffsForKey.Add(key, leftDict[key]);
                        }
                        else if (onRight)
                        {
                            // Key is on right only
                            allRightDiffsForKey.Add(key, rightDict[key]);
                        }
                    }
                }

                // Update lang diff status
                if (allLangDiffsForKey.Count > 0 ||
                    allLeftDiffsForKey.Count > 0 ||
                    allRightDiffsForKey.Count > 0 ||
                    AllNewRightLangs.ContainsKey(lang.Key) ||
                    AllNewLeftLangs.ContainsKey(lang.Key))
                    langDiffStatus.Add(lang.Key);

                // Add the populated diffs
                AllDiffs.Add(lang.Key, allLangDiffsForKey);
                AllLeftDiffs.Add(lang.Key, allLeftDiffsForKey);
                AllRightDiffs.Add(lang.Key, allRightDiffsForKey);
                AllLangKeys.Add(lang.Key);
            }
        }

        public static void SaveAndExport()
        {
            bool needToExport = false;

            // make a copy of all langs in case they cancel
            LELangDictCollection all_langs_copy = new LELangDictCollection(LEStringTableEditor.AllLangsLogical);

            // spin through each language
            foreach (var lang in AllDiffs)
            {
                // spin through each diff
                foreach (var diff in lang.Value)
                {
                    Dbg.Assert(all_langs_copy[lang.Key][diff.Key] != null);

                    DiffChoice choice = GetChoiceFor(lang.Key, diff.Key);
                    if (choice.Equals(DiffChoice.None))
                        throw new Exception(string.Format(LEConstants.DiffChoiceNotFoundFormat, diff.Key, lang.Key));

                    if (choice == DiffChoice.Left)
                    {
                        all_langs_copy[lang.Key][diff.Key] = diff.Value.LeftValue;
                        needToExport = true;
                    }
                    else if (choice == DiffChoice.Right)
                    {
                        all_langs_copy[lang.Key][diff.Key] = diff.Value.RightValue;
                    }
                }
            }

            List<string> newLeftSideLangsChosen = new List<string>();

            // AllNewLeftLangs
            // Remove any new languages on the left that were
            // not chosen (They were added above. The all_langs_copy
            // is seeded with the local language collection)
            foreach(var pair in AllNewLeftLangs)
            {
                DiffChoice choice = GetChoiceFor(pair.Key, pair.Key);

                if (choice.Equals(DiffChoice.None))
                    throw new Exception(string.Format(LEConstants.DiffChoiceNotFoundForLang, pair.Key));
                
                // Add the new language if it was chosen
                if (choice.Equals(DiffChoice.Left))
                {
                    needToExport = true;

                    // Keep track of new local languages so we can add any
                    // new keys from the cloud to these because they will be missing
                    // yes, brain explosion.
                    newLeftSideLangsChosen.Add(pair.Key);
                }
                else
                {
                    all_langs_copy.Remove(pair.Key);
                    LEStringTableEditor.RemoveLanguage(pair.Key);
                }
            }
            
            // AllNewRightLangs
            // Add any new languages from the right that were chosen
            foreach(var pair in AllNewRightLangs)
            {
                DiffChoice choice = GetChoiceFor(pair.Key, pair.Key);

                if (choice.Equals(DiffChoice.None))
                    throw new Exception(string.Format(LEConstants.DiffChoiceNotFoundForLang, pair.Key));

                // Add the new language if it was chosen
                if (choice.Equals(DiffChoice.Right))
                {
                    all_langs_copy.Add(pair.Key, pair.Value);
                }
                else
                {
                    needToExport = true;
                }
            }

            // AllLeftDiffs
            // new keys that are only on the left side; determine if
            // any of these keys were chosen; if chosen they are
            // already in master so no changes needed, otherwise
            // delete key
            foreach(var langDiff in AllLeftDiffs)
            {
                foreach(var diff in langDiff.Value)
                {
                    DiffChoice choice = GetChoiceFor(langDiff.Key, diff.Key);

                    if (choice.Equals(DiffChoice.None))
                        throw new Exception(string.Format(LEConstants.DiffChoiceNotFoundFormat, langDiff.Key, diff.Key));

                    foreach(var lang in all_langs_copy)
                    {
                        if (choice.Equals(DiffChoice.Left))
                        {
                            // Make sure the key exists in all languages if it was chosen
                            if (!lang.Value.ContainsKey(diff.Key))
                                lang.Value.Add(diff.Key, diff.Value);
                            
                            needToExport = true;
                        }
                        else
                        {
                            lang.Value.Remove(diff.Key);
                        }
                    }
                }
            }

            // AllRightDiffs
            // new keys that are only on the right side; determine if
            // any of these keys were chosen; if so create the key in
            // master, delete the key in the cloud
            foreach(var langDiff in AllRightDiffs)
            {
                foreach(var diff in langDiff.Value)
                {
                    DiffChoice choice = GetChoiceFor(langDiff.Key, diff.Key);

                    if (choice.Equals(DiffChoice.None))
                        throw new Exception(string.Format(LEConstants.DiffChoiceNotFoundFormat, langDiff.Key, diff.Key));

                    Dbg.Assert(all_langs_copy.ContainsKey(langDiff.Key));
                    Dbg.Assert(!all_langs_copy[langDiff.Key].ContainsKey(diff.Key));

                    if (choice.Equals(DiffChoice.Right))
                    {
                        all_langs_copy[langDiff.Key].Add(diff.Key, diff.Value);

                        foreach(var newLangKey in newLeftSideLangsChosen)
                        {
                            // Add an empty key for these new langages
                            if (!all_langs_copy[newLangKey].ContainsKey(diff.Key))
                                all_langs_copy[newLangKey].Add(diff.Key, string.Empty);
                        }
                    }
                    else
                        needToExport = true;
                }
            }

            LELangDictCollection ordered_langs_copy;
            OrderKeys(all_langs_copy, otherLangs, out ordered_langs_copy);

            LEStringTableEditor.ClearAll();
            LEStringTableEditor.AllLangsLogical = ordered_langs_copy;
            LEStringTableEditor.Save();

            if (needToExport)
                Export();

            if (OnSyncComplete != null)
                OnSyncComplete();
        }

        static void Export()
        {
            LESettings settings = LESettings.Instance;
            string sheetName;

            if (settings.LastSyncType.Equals(LESettings.SyncType.Google))
            {
                sheetName = settings.SyncGoogleSpreadsheetName;
                Dbg.Assert(!string.IsNullOrEmpty(sheetName));

                string tempSheetPath = FileUtil.GetUniqueTempPathInProject() + "sync_upload_" + sheetName + ".xlsx";
                
                // take the local languages dictionary
                // write it out to an excel file so we can upload to sheets
                EPPExcelHelper excelHelper = new EPPExcelHelper(tempSheetPath, true);

                excelHelper.WriteAllStringTables(LEConstants.DefaultSheetName, LEStringTableEditor.AllLangsLogical);
                GoogleDriveHelper.Instance.UploadToExistingSheet(sheetName, tempSheetPath);
            }
            else if (settings.LastSyncType.Equals(LESettings.SyncType.Local))
            {
                sheetName = settings.SyncLocalSpreadsheetName;
                Dbg.Assert(!string.IsNullOrEmpty(sheetName));

                EPPExcelHelper excelHelper = new EPPExcelHelper(sheetName, true);
                excelHelper.WriteAllStringTables(LEConstants.DefaultSheetName, LEStringTableEditor.AllLangsLogical);
            }
            else
            {
                Debug.LogError(LEConstants.SyncSettingsMissing);
            }
        }

        // This method orders the dictionary according to the cloud order.
        // Any new keys that are not included in the cloud, are added to the end.
        private static void OrderKeys(LELangDictCollection source, LELangDictCollection order, out LELangDictCollection result)
        {
            result = new LELangDictCollection();
            foreach(var lang in order)
            {
                // Only add the lang if it exists in source
                if (source.ContainsKey(lang.Key))
                {
                    LELangDict orderedDict = new LELangDict();
                    LELangDict sourceDict = source[lang.Key];

                    foreach(var loc_string in lang.Value)
                    {
                        // Only add the key if it exists in source
                        if(sourceDict.ContainsKey(loc_string.Key))
                            orderedDict.Add(loc_string.Key, sourceDict[loc_string.Key]);
                    }

                    // Merge in any values that didn't exist in the order dict
                    // This happens when new keys were added locally
                    orderedDict.Merge(sourceDict);

                    Dbg.Assert(sourceDict.Count.Equals(orderedDict.Count));
                    result.Add(lang.Key, orderedDict);
                }
            }

            // Merge in any languages that didn't exist in the order dict
            // This happens when new languages were added locallly
            result.Merge(source);
        }

        #endregion

        #region Diff Helpers
        public static void ResetSync()
        {
            AllDiffs.Clear();
            AllLeftDiffs.Clear();
            AllRightDiffs.Clear();
            AllNewLeftLangs.Clear();
            AllNewRightLangs.Clear();

            AllLangKeys.Clear();

            langDiffStatus.Clear();
            ClearChoices();
        }

        public static void ClearChoices()
        {
            diffChoices.Clear();
            allSideChoice = ALLSide.None;
        }
        #endregion
    }
}
