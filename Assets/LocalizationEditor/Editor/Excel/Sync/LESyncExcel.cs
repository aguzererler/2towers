using UnityEngine;
using UnityEditor;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace LocalizationEditor
{
    using LELangDict = System.Collections.Generic.Dictionary<string, string>;
    using LELangDictCollection = System.Collections.Generic.Dictionary<string, System.Collections.Generic.Dictionary<string, string>>;
    
    public class LESyncExcel : EditorWindow
    {
        public enum View
        {
            Default,
            LocalFile,
            NewLocalFile,
            LaunchAuthURL,
            Authenticate,
            SyncExisting,
            SyncNew,
            SyncComplete,
            
            None
        }
        
        GUIStyle headerStyle = null;
        GUIStyle linkStyle = null;
        GUIStyle buttonStyle = null;
        GUIStyle labelStyle = null;
        GUIStyle comboBoxStyle = null;
        GUIStyle textFieldStyle = null;
        
        GUIContent _content;
        GUIContent content 
        {
            get
            {
                if (_content == null)
                    _content = new GUIContent();
                return _content;
            }
        }

        string spreadsheetPath = string.Empty;
        
        Vector2 size;
        
        string accessCode = "";
        int downloadSelectionIndex = 0;
        
        public static View currentView;
        public static View viewAfterAuth = View.None;
        static View nextView;
        
        LEDrawHelper _drawHelper;
        LEDrawHelper drawHelper
        {
            get
            {
                if (_drawHelper == null)
                    _drawHelper = new LEDrawHelper(this, 2f, 10f, 10f, 10f, 20f);
                
                return _drawHelper;
            }
        }

        public delegate void DoneAction();
        DoneAction OnDone;
        
        void SetStyles()
        {
            if (headerStyle == null || string.IsNullOrEmpty(headerStyle.name))
            {
                headerStyle = new GUIStyle(GUI.skin.label);
                headerStyle.fontStyle = FontStyle.Bold;
                headerStyle.fontSize = 16;
            }
            
            if (buttonStyle == null || string.IsNullOrEmpty(buttonStyle.name))
            {
                buttonStyle = new GUIStyle(GUI.skin.button);
                buttonStyle.fontSize = 14;
                buttonStyle.padding = new RectOffset(15, 15, 5, 5);
            }
            
            if (labelStyle == null || string.IsNullOrEmpty(labelStyle.name))
            {
                labelStyle = new GUIStyle(GUI.skin.label);
                labelStyle.fontSize = 14;
                labelStyle.padding = new RectOffset(0, 0, 3, 3);
                labelStyle.wordWrap = true;
            }
            
            if (linkStyle == null || string.IsNullOrEmpty(linkStyle.name))
            {
                linkStyle = new GUIStyle(GUI.skin.label);
                linkStyle.fontSize = 16;
                linkStyle.alignment = TextAnchor.MiddleCenter;
                linkStyle.normal.textColor = Color.blue;
            }
            
            if (comboBoxStyle == null || string.IsNullOrEmpty(comboBoxStyle.name))
            {
                comboBoxStyle = new GUIStyle(EditorStyles.popup);
                comboBoxStyle.fontSize = 14;
                comboBoxStyle.fixedHeight = 24f;
            }
            
            if (textFieldStyle == null || string.IsNullOrEmpty(textFieldStyle.name))
            {
                textFieldStyle = new GUIStyle(GUI.skin.textField);
                textFieldStyle.fontSize = 14;
                textFieldStyle.fixedHeight = 22f;
                textFieldStyle.alignment = TextAnchor.UpperLeft;
                textFieldStyle.margin = new RectOffset(0, 0, 0, 8);
            }
        }
        
        public void LoadSettings(DoneAction doneCallback, View view = View.Default)
        {
            currentView = view;
            nextView = view;
            
            minSize = new Vector2(420, 250);
            maxSize = minSize;

            OnDone += doneCallback;
        }
        
        void OnGUI()
        {
            SetStyles();
            
            size = Vector2.zero;
            
            drawHelper.ResetToTop();
            
            if (currentView.Equals(View.Default))
                DrawDefaultView();
            else if (currentView.Equals(View.LaunchAuthURL))
                DrawLaunchAuthURL();
            else if (currentView.Equals(View.Authenticate))
                DrawAuthenticateView();
            else if (currentView.Equals(View.SyncExisting))
                DrawSyncExistingGoogle();
            else if (currentView.Equals(View.SyncNew))
                DrawSyncNewGoogle();
            else if (currentView.Equals(View.LocalFile))
                DrawExistingLocalFile();
            else if (currentView.Equals(View.SyncComplete))
                DrawSyncComplete();
            else if (currentView.Equals(View.NewLocalFile))
                DrawNewLocalFile();
            
            currentView = nextView;
        }
        
        void DrawDefaultView()
        {
            content.text = LEConstants.SyncDlgTitleLbl;
            drawHelper.TryGetCachedSize(LEConstants.SizeSyncDlgTitleLblKey, content, headerStyle, out size);
            EditorGUI.LabelField(new Rect(drawHelper.CenteredOnLine(size.x), drawHelper.TopOfLine(), size.x, size.y), content, headerStyle);
            
            drawHelper.NewLine(2);

            content.text = LEConstants.SyncWithLocalFileBtn;
            drawHelper.TryGetCachedSize(LEConstants.SizeSyncWithLocalFileBtnKey, content, buttonStyle, out size);
            if (GUI.Button(new Rect(drawHelper.CenteredOnLine(size.x), drawHelper.TopOfLine(), size.x, size.y), content, buttonStyle))
                nextView = View.LocalFile;
            
            drawHelper.NewLine(2.5f);

            
            content.text = LEConstants.SyncWithNewLocalFileBtn;
            drawHelper.TryGetCachedSize(LEConstants.SizeSyncWithNewLocalFileBtnKey, content, buttonStyle, out size);
            if (GUI.Button(new Rect(drawHelper.CenteredOnLine(size.x), drawHelper.TopOfLine(), size.x, size.y), content, buttonStyle))
            {
                nextView = View.NewLocalFile;
                spreadsheetPath = string.Empty;
                GUI.FocusControl(string.Empty);
            }
            
            drawHelper.NewLine(2.5f);
            
            content.text = LEConstants.SyncWithSheetsBtn;
            drawHelper.TryGetCachedSize(LEConstants.SizeSyncWithSheetsBtnKey, content, buttonStyle, out size);
            if (GUI.Button(new Rect(drawHelper.CenteredOnLine(size.x), drawHelper.TopOfLine(), size.x, size.y), content, buttonStyle))
            {
                if (HasAuthenticated())
                {
                    nextView = View.SyncExisting;
                    GoogleDriveHelper.Instance.GetSpreadsheetList();
                }
                else
                {
                    nextView = View.LaunchAuthURL;
                    viewAfterAuth = View.SyncExisting;
                }
            }
            
            drawHelper.NewLine(2.5f);
            
            content.text = LEConstants.SyncWithNewSheetBtn;
            drawHelper.TryGetCachedSize(LEConstants.SizeSyncWithNewSheetBtnKey, content, buttonStyle, out size);
            if (GUI.Button(new Rect(drawHelper.CenteredOnLine(size.x), drawHelper.TopOfLine(), size.x, size.y), content, buttonStyle))
            {
                if (HasAuthenticated())
                {
                    nextView = View.SyncNew;
                }
                else
                {
                    nextView = View.LaunchAuthURL;
                    viewAfterAuth = View.SyncNew;
                }
            }
        }
        
        void DrawExistingLocalFile()
        {
            content.text = LEConstants.SyncExcelWorkbookLbl;
            drawHelper.TryGetCachedSize(LEConstants.SizeSyncExcelWorkbookLblKey, content, headerStyle, out size);
            EditorGUI.LabelField(new Rect(drawHelper.CenteredOnLine(size.x), drawHelper.TopOfLine(), size.x, size.y), content, headerStyle);
            
            drawHelper.NewLine(2);
            
            content.text = LEConstants.ExcelFilePathLbl;
            drawHelper.TryGetCachedSize(LEConstants.SizeExcelFilePathLblKey, content, labelStyle, out size);
            EditorGUI.LabelField(new Rect(drawHelper.CurrentLinePosition, drawHelper.TopOfLine(), size.x, size.y), content, labelStyle);
            drawHelper.CurrentLinePosition += size.x + 2f;
            drawHelper.NewLine();
            
            spreadsheetPath = EditorGUI.TextField(new Rect(drawHelper.CurrentLinePosition, drawHelper.TopOfLine(), drawHelper.FullSeparatorWidth(), 
                                                           textFieldStyle.fixedHeight), spreadsheetPath, textFieldStyle);
            drawHelper.CurrentLinePosition += size.x + 2f;
            drawHelper.NewLine(1.1f);
            
            content.text = LEConstants.BrowseLbl;
            drawHelper.TryGetCachedSize(LEConstants.SizeBrowseLblKey, content, buttonStyle, out size);
            if (GUI.Button(new Rect(drawHelper.CurrentLinePosition, drawHelper.TopOfLine(), size.x, size.y), content, buttonStyle))
            {
                string newSpreadSheetPath = EditorUtility.OpenFilePanel(LEConstants.OpenWBLbl, spreadsheetPath, string.Empty);
                if (!string.IsNullOrEmpty(newSpreadSheetPath) && !newSpreadSheetPath.Equals(spreadsheetPath))
                    spreadsheetPath = newSpreadSheetPath;
                GUI.FocusControl(string.Empty);
            }
            
            // Draw Back & Export Buttons
            content.text = LEConstants.BackBtn;
            drawHelper.TryGetCachedSize(LEConstants.SizeBackBtnKey, content, buttonStyle, out size);
            if (GUI.Button(new Rect(drawHelper.CurrentLinePosition, position.height-size.y-drawHelper.BottomBuffer, size.x, size.y), content, buttonStyle))
                nextView = View.Default;
            
            content.text = LEConstants.SyncBtn;
            drawHelper.TryGetCachedSize(LEConstants.SizeSyncBtnKey, content, buttonStyle, out size);
            if (GUI.Button(new Rect(position.width-size.x-drawHelper.LeftBuffer, position.height-size.y-drawHelper.BottomBuffer, size.x, size.y), content, buttonStyle))
            {
                LESettings settings = LESettings.Instance;
                settings.SyncLocalSpreadsheetName = spreadsheetPath;
                settings.LastSyncType = LESettings.SyncType.Local;
                settings.Save();
                
                // take the local languages dictionary
                // write it out to an excel file
                /*EPPExcelHelper excelHelper = new EPPExcelHelper(spreadsheetPath, true);
                excelHelper.WriteAllStringTables(LEConstants.DefaultSheetName, LEEditor.AllLangs);
                nextView = View.SyncComplete;*/

                Done();
            }
        }

        void DrawNewLocalFile()
        {
            content.text = LEConstants.SyncExcelWorkbookLbl;
            drawHelper.TryGetCachedSize(LEConstants.SizeSyncExcelWorkbookLblKey, content, headerStyle, out size);
            EditorGUI.LabelField(new Rect(drawHelper.CenteredOnLine(size.x), drawHelper.TopOfLine(), size.x, size.y), content, headerStyle);
            
            drawHelper.NewLine(2);
            
            content.text = LEConstants.ExcelFilePathLbl;
            drawHelper.TryGetCachedSize(LEConstants.SizeExcelFilePathLblKey, content, labelStyle, out size);
            EditorGUI.LabelField(new Rect(drawHelper.CurrentLinePosition, drawHelper.TopOfLine(), size.x, size.y), content, labelStyle);
            drawHelper.CurrentLinePosition += size.x + 2f;
            drawHelper.NewLine();
            
            spreadsheetPath = EditorGUI.TextField(new Rect(drawHelper.CurrentLinePosition, drawHelper.TopOfLine(), drawHelper.FullSeparatorWidth(), 
                                                           textFieldStyle.fixedHeight), spreadsheetPath, textFieldStyle);
            drawHelper.CurrentLinePosition += size.x + 2f;
            drawHelper.NewLine(1.1f);
            
            content.text = LEConstants.BrowseLbl;
            drawHelper.TryGetCachedSize(LEConstants.SizeBrowseLblKey, content, buttonStyle, out size);
            if (GUI.Button(new Rect(drawHelper.CurrentLinePosition, drawHelper.TopOfLine(), size.x, size.y), content, buttonStyle))
            {
                string newSpreadSheetPath = EditorUtility.SaveFilePanel(LEConstants.SaveSyncFileLbl, string.Empty, 
                                                                        LEConstants.DefaultSyncName, LEConstants.DefaultSyncExt);
                if (!string.IsNullOrEmpty(newSpreadSheetPath) && !newSpreadSheetPath.Equals(spreadsheetPath))
                    spreadsheetPath = newSpreadSheetPath;
                GUI.FocusControl(string.Empty);
            }
            
            // Draw Back & Export Buttons
            content.text = LEConstants.BackBtn;
            drawHelper.TryGetCachedSize(LEConstants.SizeBackBtnKey, content, buttonStyle, out size);
            if (GUI.Button(new Rect(drawHelper.CurrentLinePosition, position.height-size.y-drawHelper.BottomBuffer, size.x, size.y), content, buttonStyle))
                nextView = View.Default;
            
            content.text = LEConstants.SyncBtn;
            drawHelper.TryGetCachedSize(LEConstants.SizeSyncBtnKey, content, buttonStyle, out size);
            if (!string.IsNullOrEmpty(spreadsheetPath) &&
                GUI.Button(new Rect(position.width-size.x-drawHelper.LeftBuffer, position.height-size.y-drawHelper.BottomBuffer, size.x, size.y), content, buttonStyle))
            {
                LESettings settings = LESettings.Instance;
                settings.SyncLocalSpreadsheetName = spreadsheetPath;
                settings.LastSyncType = LESettings.SyncType.Local;
                settings.Save();

                EPPExcelHelper excelHelper = new EPPExcelHelper(spreadsheetPath, true);
                excelHelper.WriteAllStringTables(LEConstants.DefaultSheetName, LEStringTableEditor.AllLangsLogical);

                nextView = View.SyncComplete;
            }
        }
        
        void DrawLaunchAuthURL()
        {
            content.text = LEConstants.AuthWithGoogleLbl;
            drawHelper.TryGetCachedSize(LEConstants.SizeAuthWithGoogleLblKey, content, headerStyle, out size);
            EditorGUI.LabelField(new Rect(drawHelper.CenteredOnLine(size.x), drawHelper.TopOfLine(), size.x, size.y), content, headerStyle);
            
            drawHelper.NewLine(2);
            
            content.text = LEConstants.GoogleAuthInstruction1_1;
            drawHelper.TryGetCachedSize(LEConstants.SizeGoogleAuthInstruction1_1Key, content, labelStyle, out size);
            EditorGUI.LabelField(new Rect(drawHelper.CurrentLinePosition, drawHelper.TopOfLine(), size.x, size.y), content, labelStyle);
            
            drawHelper.NewLine();
            
            content.text = LEConstants.GoogleAuthInstruction1_2;
            drawHelper.TryGetCachedSize(LEConstants.SizeGoogleAuthInstruction1_2Key, content, labelStyle, out size);
            EditorGUI.LabelField(new Rect(drawHelper.CurrentLinePosition, drawHelper.TopOfLine(), size.x, size.y), content, labelStyle);
            
            drawHelper.NewLine(2);
            
            content.text = LEConstants.GoogleAuthInstruction2_1;
            drawHelper.TryGetCachedSize(LEConstants.SizeGoogleAuthInstruction2_1Key, content, labelStyle, out size);
            EditorGUI.LabelField(new Rect(drawHelper.CurrentLinePosition, drawHelper.TopOfLine(), size.x, size.y), content, labelStyle);
            
            drawHelper.NewLine();
            
            content.text = LEConstants.GoogleAuthInstruction2_2;
            drawHelper.TryGetCachedSize(LEConstants.SizeGoogleAuthInstruction2_2Key, content, labelStyle, out size);
            EditorGUI.LabelField(new Rect(drawHelper.CurrentLinePosition, drawHelper.TopOfLine(), size.x, size.y), content, labelStyle);
            
            
            // Draw Back & GOTO Auth buttons
            content.text = LEConstants.BackBtn;
            drawHelper.TryGetCachedSize(LEConstants.SizeBackBtnKey, content, buttonStyle, out size);
            if (GUI.Button(new Rect(drawHelper.CurrentLinePosition, position.height-size.y-drawHelper.BottomBuffer, size.x, size.y), content, buttonStyle))
                nextView = View.Default;
            
            content.text = LEConstants.GoToAuthURLBtn;
            drawHelper.TryGetCachedSize(LEConstants.SizeGoToAuthURLBtnKey, content, buttonStyle, out size);
            if (GUI.Button(new Rect(position.width-size.x-drawHelper.LeftBuffer, position.height-size.y-drawHelper.BottomBuffer, size.x, size.y), content, buttonStyle))
            {
                GoogleDriveHelper.Instance.RequestAuthFromUser();
                nextView = View.Authenticate;
            }
        }
        
        void DrawAuthenticateView()
        {
            content.text = LEConstants.AuthWithGoogleLbl;
            drawHelper.TryGetCachedSize(LEConstants.SizeAuthWithGoogleLblKey, content, headerStyle, out size);
            EditorGUI.LabelField(new Rect(drawHelper.CenteredOnLine(size.x), drawHelper.TopOfLine(), size.x, size.y), content, headerStyle);
            
            drawHelper.NewLine(2);
            
            content.text = LEConstants.EnterAccessCodeLbl;
            drawHelper.TryGetCachedSize(LEConstants.SizeEnterAccessCodeLblKey, content, labelStyle, out size);
            EditorGUI.LabelField(new Rect(drawHelper.CurrentLinePosition, drawHelper.TopOfLine(), size.x, size.y), content, labelStyle);
            
            drawHelper.NewLine(1.1f);
            
            accessCode = EditorGUI.TextField(new Rect(drawHelper.CurrentLinePosition, drawHelper.TopOfLine(), drawHelper.FullSeparatorWidth(), size.y), accessCode, textFieldStyle);
            
            // Draw Back & Set Code Buttons
            content.text = LEConstants.BackBtn;
            drawHelper.TryGetCachedSize(LEConstants.SizeBackBtnKey, content, buttonStyle, out size);
            if (GUI.Button(new Rect(drawHelper.CurrentLinePosition, position.height-size.y-drawHelper.BottomBuffer, size.x, size.y), content, buttonStyle))
                nextView = View.LaunchAuthURL;
            
            content.text = LEConstants.SetCodeBtn;
            drawHelper.TryGetCachedSize(LEConstants.SizeSetCodeBtnKey, content, buttonStyle, out size);
            if (!string.IsNullOrEmpty(accessCode) && GUI.Button(new Rect(position.width-size.x-drawHelper.LeftBuffer, position.height-size.y-drawHelper.BottomBuffer, size.x, size.y), content, buttonStyle))
            {
                GoogleDriveHelper.Instance.SetAccessCode(accessCode);
                GoogleDriveHelper.Instance.GetSpreadsheetList();
                
                if (!viewAfterAuth.Equals(View.None))
                {
                    nextView = viewAfterAuth;
                    viewAfterAuth = View.None;
                }
                else
                {
                    nextView = View.Default;
                }
                
                GUI.FocusControl(string.Empty);
            }
        }
        
        void DrawSyncExistingGoogle()
        {
            GoogleDriveHelper driveHelper = GoogleDriveHelper.Instance;

            content.text = LEConstants.ChooseGoogleSheetLbl;
            drawHelper.TryGetCachedSize(LEConstants.SizeChooseGoogleSheetLblKey, content, headerStyle, out size);
            EditorGUI.LabelField(new Rect(drawHelper.CenteredOnLine(size.x), drawHelper.TopOfLine(), size.x, size.y), content, headerStyle);
            
            drawHelper.NewLine(2);
            
            content.text = LEConstants.SelectSpreadSheetSyncLbl;
            drawHelper.TryGetCachedSize(LEConstants.SizeSelectSpreadSheetSyncLblKey, content, labelStyle, out size);
            EditorGUI.LabelField(new Rect(drawHelper.CurrentLinePosition, drawHelper.TopOfLine(), size.x, size.y), content, labelStyle);
            
            drawHelper.NewLine(1.1f);
            
            downloadSelectionIndex = EditorGUI.Popup(new Rect(drawHelper.CurrentLinePosition, drawHelper.TopOfLine(), drawHelper.FullSeparatorWidth(), size.y),
                                                     downloadSelectionIndex, driveHelper.SpreadSheetNames, comboBoxStyle);
            
            // Draw Back & Sync Buttons
            content.text = LEConstants.BackBtn;
            drawHelper.TryGetCachedSize(LEConstants.SizeBackBtnKey, content, buttonStyle, out size);
            if (GUI.Button(new Rect(drawHelper.CurrentLinePosition, position.height-size.y-drawHelper.BottomBuffer, size.x, size.y), content, buttonStyle))
                nextView = View.Default;
            
            content.text = LEConstants.SyncBtn;
            drawHelper.TryGetCachedSize(LEConstants.SizeSyncBtnKey, content, buttonStyle, out size);
            if (GUI.Button(new Rect(position.width-size.x-drawHelper.LeftBuffer, position.height-size.y-drawHelper.BottomBuffer, size.x, size.y), content, buttonStyle))
            {
                LESettings settings = LESettings.Instance;

                settings.SyncGoogleSpreadsheetName = driveHelper.SpreadSheetNames[downloadSelectionIndex];
                settings.LastSyncType = LESettings.SyncType.Google;
                settings.Save();
                
                Done();
            }
        }
        
        void DrawSyncNewGoogle()
        {
            content.text = LEConstants.SyncNewGoogleSheetLbl;
            drawHelper.TryGetCachedSize(LEConstants.SizeSyncNewGoogleSheetLblKey, content, headerStyle, out size);
            EditorGUI.LabelField(new Rect(drawHelper.CenteredOnLine(size.x), drawHelper.TopOfLine(), size.x, size.y), content, headerStyle);
            
            drawHelper.NewLine(2);
            
            content.text = LEConstants.NewSheetFileNameLbl;
            drawHelper.TryGetCachedSize(LEConstants.SizeNewSheetFileNameLblKey, content, labelStyle, out size);
            EditorGUI.LabelField(new Rect(drawHelper.CurrentLinePosition, drawHelper.TopOfLine(), size.x, size.y), content, labelStyle);
            
            drawHelper.NewLine(1.1f);
            
            spreadsheetPath = EditorGUI.TextField(new Rect(drawHelper.CurrentLinePosition, drawHelper.TopOfLine(), drawHelper.FullSeparatorWidth(), size.y),
                                                  spreadsheetPath, textFieldStyle);
            
            // Draw Back & Sync Buttons
            content.text = LEConstants.BackBtn;
            drawHelper.TryGetCachedSize(LEConstants.SizeBackBtnKey, content, buttonStyle, out size);
            if (GUI.Button(new Rect(drawHelper.CurrentLinePosition, position.height-size.y-drawHelper.BottomBuffer, size.x, size.y), content, buttonStyle))
                nextView = View.Default;
            
            content.text = LEConstants.SyncBtn;
            drawHelper.TryGetCachedSize(LEConstants.SizeSyncBtnKey, content, buttonStyle, out size);
            if (!string.IsNullOrEmpty(spreadsheetPath) &&
                GUI.Button(new Rect(position.width-size.x-drawHelper.LeftBuffer, position.height-size.y-drawHelper.BottomBuffer, size.x, size.y), content, buttonStyle))
            {
                string tempSheetPath = FileUtil.GetUniqueTempPathInProject() + "syncnewgoog_" + spreadsheetPath + ".xlsx";
                
                // take the local languages dictionary
                // write it out to an excel file so we can upload to sheets
                EPPExcelHelper excelHelper = new EPPExcelHelper(tempSheetPath, true);

                excelHelper.WriteAllStringTables(LEConstants.DefaultSheetName, LEStringTableEditor.AllLangsLogical);
                GoogleDriveHelper.Instance.UploadNewSheet(tempSheetPath, spreadsheetPath);

                LESettings settings = LESettings.Instance;
                settings.LastSyncType = LESettings.SyncType.Google;
                settings.SyncGoogleSpreadsheetName = spreadsheetPath;
                settings.Save();
                
                nextView = View.SyncComplete;
            }
        }
        
        void DrawSyncComplete()
        {
            content.text = LEConstants.SyncCompleteLbl;
            drawHelper.TryGetCachedSize(LEConstants.SizeSyncCompleteLblKey, content, headerStyle, out size);
            EditorGUI.LabelField(new Rect(drawHelper.CenteredOnLine(size.x), drawHelper.TopOfLine(), size.x, size.y), content, headerStyle);
            
            drawHelper.NewLine(2);
            
            content.text = LEConstants.SyncCompleteMsg1;
            drawHelper.TryGetCachedSize(LEConstants.SizeSyncCompleteMsg1Key, content, labelStyle, out size);
            EditorGUI.LabelField(new Rect(drawHelper.CurrentLinePosition, drawHelper.TopOfLine(), size.x, size.y), content, labelStyle);
            
            drawHelper.NewLine(1.1f);
            
            content.text = LEConstants.SyncCompleteMsg2;
            drawHelper.TryGetCachedSize(LEConstants.SizeSyncCompleteMsg2Key, content, labelStyle, out size);
            EditorGUI.LabelField(new Rect(drawHelper.CurrentLinePosition, drawHelper.TopOfLine(), size.x, size.y), content, labelStyle);

            drawHelper.NewLine(size.y/drawHelper.LineHeight + .5f);

            LESettings settings = LESettings.Instance;
            if (settings.LastSyncType.Equals(LESettings.SyncType.Google))
                content.text = LEConstants.GoogleSheetLbl + ": " + settings.SyncGoogleSpreadsheetName;
            else
                content.text = LEConstants.LocalFileLbl + ": " + settings.SyncLocalSpreadsheetName;

            if (drawHelper.SizeCache.ContainsKey(content.text))
                size = drawHelper.SizeCache[content.text];
            else
            {
                size.x = drawHelper.WidthLeftOnCurrentLine();
                size.y = Math.Min(labelStyle.CalcHeight(content, size.x), LEConstants.MaxSyncFileDisplayHeight);
                drawHelper.SizeCache.Add(content.text, new Vector2(size.x, size.y));
            }

            EditorGUI.LabelField(new Rect(drawHelper.CurrentLinePosition, drawHelper.TopOfLine(), size.x, size.y), content, labelStyle);
            
            // Draw rate box
            float heightOfBox = 50f;
            float topOfBox = this.position.height * .5f + 25f;
            float bottomOfBox = topOfBox + heightOfBox;
            
            content.text = LEConstants.ForumLinkText;
            drawHelper.TryGetCachedSize(LEConstants.SizeForumLinkTextKey, content, linkStyle, out size);
            
            float widthOfBox = size.x + 10f;
            float leftOfBox = (this.position.width - widthOfBox) / 2f;
            
            if (GUI.Button(new Rect(leftOfBox + 6f, bottomOfBox - size.y - 2f, size.x, size.y), content, linkStyle))
            {
                Application.OpenURL(LEConstants.ForumURL);
            }
            
            content.text = LEConstants.RateMeText;
            if (GUI.Button(new Rect(leftOfBox + 6f, topOfBox + 3f, size.x, size.y), content, linkStyle))
            {
                Application.OpenURL(LEConstants.RateMeURL);
            }
            
            content.text = LEConstants.CloseBtn;
            drawHelper.TryGetCachedSize(LEConstants.SizeCloseBtnKey, content, buttonStyle, out size);
            if (GUI.Button(new Rect(position.width-size.x-drawHelper.LeftBuffer, position.height-size.y-drawHelper.BottomBuffer, size.x, size.y), content, buttonStyle))
            {
                Close();
            }
        }

        void Done()
        {
            if (OnDone != null)
                OnDone();

            Close();
        }

        bool HasAuthenticated()
        {
            return GoogleDriveHelper.Instance.HasAuthenticated();
        }
    }
}