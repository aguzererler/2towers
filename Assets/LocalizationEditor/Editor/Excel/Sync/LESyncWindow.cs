using UnityEngine;
using UnityEditor;
using System;
using System.IO;
using System.Collections.Generic;

namespace LocalizationEditor
{
    using LEDiffDict = System.Collections.Generic.Dictionary<string, LocalizationEditor.LEStringDiff>;
    using LESideOnlyDiffDict = System.Collections.Generic.Dictionary<string, string>;

    public class LESyncWindow : EditorWindow
    {
        GUIStyle logStyle = null;
        GUIStyle syncButtonStyle = null;
        GUIStyle radioButtonStyle = null;
        GUIStyle labelStyle = null;
        GUIStyle textFieldStyle = null;
        GUIStyle linkStyle = null;
        GUIStyle rateBoxStyle = null;
        
        bool currentFoldoutAllState = true;

        LEDrawHelper drawHelper;

        GUIContent _content;
        GUIContent content
        {
            get {
                if (_content == null)
                    _content = new GUIContent();
                return _content;
            }
            set { _content = value; }
        }

        Color headerColor
        {
            get {
                Color color;
                if (EditorGUIUtility.isProSkin)
                    color = LEConstants.MainHeaderColorProString.ToColor();
                else
                    color = LEConstants.MainHeaderColorString.ToColor();
                color.a = 1f;
                return color;
            }
        }

        string rightSideHeader = string.Empty;
        string sizeKey = string.Empty;
        Vector2 size = Vector2.zero;
        float buttonHeightMultiplier = 1.5f;

        Dictionary<string, float> groupHeights = new Dictionary<string, float>();
        Vector2 compareScrollBarPosition = Vector2.zero;
        float compareScrollViewHeight = 0;
        float compareScrollViewY = 0;
        bool isCompareScrollBarVisible = false;

        HashSet<string> entryFoldoutState = new HashSet<string>();

        void SetStyles()
        {
            if (logStyle == null || string.IsNullOrEmpty(logStyle.name))
            {
                logStyle = new GUIStyle(GUI.skin.textArea);
                logStyle.richText = true;
            }

            if (syncButtonStyle == null || string.IsNullOrEmpty(syncButtonStyle.name))
            {
                syncButtonStyle = new GUIStyle(GUI.skin.button);
                syncButtonStyle.fontSize = 14;
            }

            if (radioButtonStyle == null || string.IsNullOrEmpty(radioButtonStyle.name))
                radioButtonStyle = new GUIStyle(EditorStyles.radioButton);

            if (labelStyle == null || string.IsNullOrEmpty(labelStyle.name))
                labelStyle = new GUIStyle(EditorStyles.label);

            if (textFieldStyle == null || string.IsNullOrEmpty(textFieldStyle.name))
            {
                string fontPath = Path.Combine(LESettings.RelativeRootDir, LEConstants.SyncFontPath);

                textFieldStyle = new GUIStyle(GUI.skin.textArea);
                textFieldStyle.font = (Font)AssetDatabase.LoadAssetAtPath(fontPath, typeof(Font));
                textFieldStyle.fontSize = 14;
                textFieldStyle.alignment = TextAnchor.MiddleLeft;
                textFieldStyle.padding = new RectOffset(5, 5, 8, 8);
            }

            if (rateBoxStyle == null || string.IsNullOrEmpty(rateBoxStyle.name))
            {
                string texPath = LESettings.RelativeRootDir + "/" + LEConstants.BorderTexturePath;
                
                rateBoxStyle = new GUIStyle(GUI.skin.box);
                rateBoxStyle.normal.background = (Texture2D)AssetDatabase.LoadAssetAtPath(texPath, typeof(Texture2D));
                rateBoxStyle.border = new RectOffset(2, 2, 2, 2);
            }

            if (linkStyle == null || linkStyle.name.Equals(string.Empty))
            {
                linkStyle = new GUIStyle(GUI.skin.label);
                linkStyle.fontSize = 16;
                linkStyle.alignment = TextAnchor.MiddleCenter;
                linkStyle.normal.textColor = Color.blue;
            }
        }

        public void LoadSettings()
        {
            minSize = new Vector2(700, 400);

            if (LESettings.Instance.LastSyncType.Equals(LESettings.SyncType.Google))
                rightSideHeader = string.Format(LEConstants.RightSide, LEConstants.GoogleSheetLbl);
            else
                rightSideHeader = string.Format(LEConstants.RightSide, LEConstants.ExcelFileLbl);

            // Expand everything
            LESyncHelper.AllLangKeys.ForEach(lang => entryFoldoutState.Add(lang));
        }

        void OnGUI()
        {
            SetStyles();

            if (drawHelper == null)
                drawHelper = new LEDrawHelper(this);

            size = Vector2.zero;
            sizeKey = string.Empty;

            drawHelper.ResetToTop();

            Color currentColor = headerColor;
            drawHelper.DrawMainHeaderLabel(LEConstants.SyncHeaderLbl, currentColor, LEConstants.SizeSyncHeaderKey);

            // Draw the file we're syncing with
            LESettings settings = LESettings.Instance;
            if (settings.LastSyncType.Equals(LESettings.SyncType.Google))
            {
                drawHelper.DrawSubHeader(LEConstants.SyncingWithGoogleSheet, currentColor, LEConstants.SizeSyncingWithGoogleSheetKey);
                content.text = LEConstants.SyncingLbl + settings.SyncGoogleSpreadsheetName;
            }
            else
            {
                drawHelper.DrawSubHeader(LEConstants.SyncingWithLocalFile, currentColor, LEConstants.SizeSyncingWithLocalFileKey);
                content.text = LEConstants.SyncingLbl + settings.SyncLocalSpreadsheetName;
            }

            drawHelper.TryGetCachedSize(content.text, content, labelStyle, out size);
            EditorGUI.LabelField(new Rect(drawHelper.CurrentLinePosition, drawHelper.TopOfLine(), size.x, size.y), content, labelStyle);
            drawHelper.NewLine();
            
            // If there are no differences, display a no diff msg
            // then exit onGUI
            if (!LESyncHelper.NeedsSync())
            {
                drawHelper.NewLine(3);

                content.text = LEConstants.NoSyncNeededLbl_1;
                drawHelper.TryGetCachedSize(LEConstants.SizeNoSyncNeeded_1Key, content, labelStyle, out size);
                EditorGUI.LabelField(new Rect(drawHelper.HorizontalMiddleOfLine()-size.x/2, drawHelper.TopOfLine(), size.x, size.y), content, labelStyle);
                drawHelper.NewLine();

                content.text = LEConstants.NoSyncNeededLbl_2;
                drawHelper.TryGetCachedSize(LEConstants.SizeNoSyncNeeded_2Key, content, labelStyle, out size);
                EditorGUI.LabelField(new Rect(drawHelper.HorizontalMiddleOfLine()-size.x/2, drawHelper.TopOfLine(), size.x, size.y), content, labelStyle);
                return;
            }

            drawHelper.DrawSubHeader(LEConstants.SyncActionsLbl, currentColor, LEConstants.SyncActionsLbl);

            float topOfSection = drawHelper.TopOfLine() - drawHelper.LineHeight;

            DrawAllLeftButton();
            DrawAllRightButton();
            DrawClearChoicesButton();
            float leftThreshold = drawHelper.CurrentLinePosition + 5f;

            // These are drawn anchored to the right side of the window
            size = DrawSaveAndUploadButton();

            drawHelper.NewLine(size.y/drawHelper.LineHeight + 1);

            // Draw rate box
            Vector2 forumLinkSize;
            Vector2 rateLinkSize;
            float bottomOfSection = drawHelper.TopOfLine() - drawHelper.LineHeight/2f;
            float margin = 5f;
            float leftMargin = 5.5f;
            
            content.text = LEConstants.ForumLinkText;
            drawHelper.TryGetCachedSize(LEConstants.SizeForumLinkTextKey, content, linkStyle, out forumLinkSize);
            
            content.text = LEConstants.RateMeText;
            drawHelper.TryGetCachedSize(LEConstants.SizeRateMeTextKey, content, linkStyle, out rateLinkSize);
            
            float boxWidth = Math.Max(forumLinkSize.x, rateLinkSize.x);
            float leftBoundary = Math.Max(drawHelper.HorizontalMiddleOfLine() - boxWidth/2f, leftThreshold);
            
            content.text = LEConstants.ForumLinkText;
            if (GUI.Button(new Rect(leftBoundary+(boxWidth-forumLinkSize.x)/2f+leftMargin, bottomOfSection-size.y, forumLinkSize.x, forumLinkSize.y), content, linkStyle))
            {
                Application.OpenURL(LEConstants.ForumURL);
            }
            
            content.text = LEConstants.RateMeText;
            if(GUI.Button(new Rect(leftBoundary+(boxWidth-rateLinkSize.x)/2f+leftMargin, topOfSection+margin, rateLinkSize.x, rateLinkSize.y), content, linkStyle))
            {
                Application.OpenURL(LEConstants.RateMeURL);
            }
            
            DrawRateBox(leftBoundary, topOfSection, boxWidth+10, bottomOfSection-topOfSection);


            drawHelper.DrawSectionSeparator();

            drawHelper.DrawSubHeader(LEConstants.SyncDifferencesLbl, currentColor, LEConstants.SizeSyncDiffHeaerKey);
            drawHelper.NewLine(0.5f);

            DrawLeftRightCompare();
        }

        void DrawLeftRightCompare()
        {
            LEDiffDict diffDict;
            LESideOnlyDiffDict newLeftSideLangsDict;
            LESideOnlyDiffDict newRightSideLangsDict;
            LESideOnlyDiffDict leftSideOnlyDict;
            LESideOnlyDiffDict rightSideOnlyDict;

            if (currentFoldoutAllState)
            {
                content.text = LEConstants.CollapseAllLbl;
                sizeKey = LEConstants.SizeCollapseLblKey;
            }
            else
            {
                content.text = LEConstants.ExpandAllLbl;
                sizeKey = LEConstants.SizeExpandLblKey;
            }

            if (!drawHelper.SizeCache.TryGetValue(sizeKey, out size))
            {
                size = EditorStyles.foldout.CalcSize(content);
                drawHelper.SizeCache.Add(sizeKey, new Vector2(size.x, size.y));
            }
            bool newFoldoutAllState = EditorGUI.Foldout(new Rect(drawHelper.CurrentLinePosition, drawHelper.TopOfLine(), size.x, size.y), currentFoldoutAllState, content);

            // If it was set to true from false, then expand everything
            if ((newFoldoutAllState != currentFoldoutAllState) && newFoldoutAllState)
            {
                LESyncHelper.AllLangKeys.ForEach(lang => entryFoldoutState.Add(lang));
                currentFoldoutAllState = true;
            }
            else if ((newFoldoutAllState != currentFoldoutAllState) && !newFoldoutAllState)
            {
                entryFoldoutState.Clear();
                currentFoldoutAllState = false;
            }
            drawHelper.NewLine();

            drawHelper.DrawSubHeader(LEConstants.LeftSide, headerColor, LEConstants.SizeLeftSideHeaderKey, false);

            size = drawHelper.DrawSubHeader(rightSideHeader, headerColor, LEConstants.SizeRightSideHeaderKey, false, true);
            drawHelper.NewLine(size.y/drawHelper.LineHeight+0.5f);

            float currentGroupHeightTotal = CalculateGroupHeight();
            compareScrollViewHeight = drawHelper.HeightToBottomOfWindow();
            compareScrollViewY = drawHelper.TopOfLine();
            isCompareScrollBarVisible = currentGroupHeightTotal > compareScrollViewHeight;

            compareScrollBarPosition = GUI.BeginScrollView(new Rect(drawHelper.CurrentLinePosition, compareScrollViewY, drawHelper.FullWindowWidth(), compareScrollViewHeight),
                                                       compareScrollBarPosition,
                                                       new Rect(drawHelper.CurrentLinePosition, compareScrollViewY, drawHelper.ScrollViewWidth(), currentGroupHeightTotal));

            int count = 0;
            float currentGroupHeight = 0;
            float beginningHeight = 0;
            foreach(var langKey in LESyncHelper.AllLangKeys)
            {
                if (LESyncHelper.LangHasDifferences(langKey))
                {
                    groupHeights.TryGetValue(langKey, out currentGroupHeight);
                    if (currentGroupHeight == 0f ||
                        (currentGroupHeight.NearlyEqual(drawHelper.LineHeight) && entryFoldoutState.Contains(langKey)))
                        currentGroupHeight = drawHelper.LineHeight;

                    if (drawHelper.IsGroupVisible(currentGroupHeight, compareScrollBarPosition, compareScrollViewHeight, compareScrollViewY) ||
                        (count == LESyncHelper.AllLangKeys.Count-1 && compareScrollBarPosition.y.NearlyEqual(currentGroupHeightTotal - drawHelper.LineHeight)))
                    {
                        beginningHeight = drawHelper.CurrentHeight();
                        if (DrawDiffEntryFoldout(langKey))
                        {
                            entryFoldoutState.Add(langKey);

                            if(LESyncHelper.AllNewRightLangs.TryGetValue(langKey, out newRightSideLangsDict))
                            {
                                // Draw as a new language if its only on the right side
                                DrawDiff(langKey, langKey, LEConstants.RemoveLangLbl, LEConstants.KeepNewLangLbl);
                            }
                            else if(LESyncHelper.AllNewLeftLangs.TryGetValue(langKey, out newLeftSideLangsDict))
                            {
                                // Or Draw as a new languages if its only on the left side
                                DrawDiff(langKey, langKey, LEConstants.KeepNewLangLbl, LEConstants.RemoveLangLbl);
                            }
                            else 
                            {
                                // Draw diffs for keys that exist on both sides for the current language
                                if(LESyncHelper.AllDiffs.TryGetValue(langKey, out diffDict))
                                    DrawExistingKeyDiffs(langKey, diffDict);

                                // Draw diffs for keys that exist on left side for the current language
                                if (LESyncHelper.AllLeftDiffs.TryGetValue(langKey, out leftSideOnlyDict))
                                    DrawLeftSideOnlyDiffs(langKey, leftSideOnlyDict);

                                // Draw diffs for keys that exist on right side for the current language
                                if (LESyncHelper.AllRightDiffs.TryGetValue(langKey, out rightSideOnlyDict))
                                    DrawRightSideOnlyDiffs(langKey, rightSideOnlyDict);
                            }
                        }
                        else
                        {
                            entryFoldoutState.Remove(langKey);
                        }

                        currentGroupHeight = drawHelper.CurrentHeight() - beginningHeight;
                        groupHeights.TryAddOrUpdateValue(langKey, currentGroupHeight);
                    }
                    else
                    {
                        drawHelper.NewLine(currentGroupHeight/drawHelper.LineHeight);
                    }

                    count++;
                }
            }
            GUI.EndScrollView();
        }

        void DrawLeftSideOnlyDiffs(string lang, LESideOnlyDiffDict leftSideOnlyDict)
        {
            foreach(var langDiff in leftSideOnlyDict)
            {
                string key = langDiff.Key;
                string leftValue = langDiff.Value;
                string rightValue = LEConstants.RemoveOnlyLeft;

                DrawDiff(lang, key, leftValue, rightValue, true);
            }
        }

        void DrawRightSideOnlyDiffs(string lang, LESideOnlyDiffDict rightSideOnlyDict)
        {
            foreach(var langDiff in rightSideOnlyDict)
            {
                string key = langDiff.Key;
                string leftValue = LEConstants.RemoveOnlyRight;
                string rightValue = langDiff.Value;

                DrawDiff(lang, key, leftValue, rightValue, true);
            }
        }

        void DrawExistingKeyDiffs(string lang, LEDiffDict diffDict)
        {
            foreach(var langDiff in diffDict)
            {
                string key = langDiff.Key;
                string leftValue = langDiff.Value.LeftValue;
                string rightValue = langDiff.Value.RightValue;

                DrawDiff(lang, key, leftValue, rightValue);
            }
        }

        void DrawDiff(string lang, string key, string leftValue, string rightValue, bool lockChoiceAcrossLangs = false)
        {
            DiffChoice currentChoice = DiffChoice.None;
            bool chooseLeft = false;
            bool chooseRight = false;

            // Key label
            if (!lang.Equals(key))
            {
                content.text = LEConstants.KeyLbl + " " + LEStringTableEditor.GetPresentationFromCache(key);

                sizeKey = key + LEConstants.SizeKeySuffix;
                if (!drawHelper.SizeCache.TryGetValue(sizeKey, out size))
                {
                    size = labelStyle.CalcSize(content);
                    drawHelper.SizeCache.Add(sizeKey, new Vector2(size.x, size.y));
                }

                if (drawHelper.IsVisible(compareScrollBarPosition, compareScrollViewHeight, compareScrollViewY))
                    EditorGUI.LabelField(new Rect(drawHelper.CurrentLinePosition, drawHelper.TopOfLine(), size.x, size.y), content, labelStyle);

                drawHelper.NewLine(size.y/drawHelper.LineHeight + 0.1f);
            }

            if (drawHelper.IsVisible(compareScrollBarPosition, compareScrollViewHeight, compareScrollViewY))
            {
                // Left radio button
                size.x = LEConstants.RadioButtonWidth;
                size.y = drawHelper.StandardHeight();

                currentChoice = LESyncHelper.GetChoiceFor(lang, key);
                chooseLeft = EditorGUI.Toggle(new Rect(drawHelper.CurrentLinePosition, drawHelper.TopOfLine()+textFieldStyle.lineHeight/2f, size.x, size.y), currentChoice.Equals(DiffChoice.Left), radioButtonStyle);
                drawHelper.CurrentLinePosition += LEConstants.RadioButtonWidth + 2;
            }

            // Left value
            content.text = LEStringTableEditor.GetPresentationFromCache(leftValue);
            sizeKey = key + LEConstants.SizeLeftValueSuffix + (isCompareScrollBarVisible?"_0":"_1");

            if (!drawHelper.SizeCache.TryGetValue(sizeKey, out size))
            {
                if (isCompareScrollBarVisible)
                    size.x = drawHelper.ScrollViewWidth()-LEConstants.RadioButtonWidth*2-drawHelper.LeftBuffer-drawHelper.RightBuffer-4;
                else
                    size.x = drawHelper.FullWindowWidth()-LEConstants.RadioButtonWidth*2-drawHelper.LeftBuffer-drawHelper.RightBuffer-4;
                size.y = textFieldStyle.CalcHeight(content, size.x);
                drawHelper.SizeCache.Add(sizeKey, new Vector2(size.x, size.y));
            }

            if (drawHelper.IsVisible(compareScrollBarPosition, compareScrollViewHeight, compareScrollViewY))
                EditorGUI.LabelField(new Rect(drawHelper.CurrentLinePosition, drawHelper.TopOfLine(), size.x, size.y), content.text, textFieldStyle);

            drawHelper.NewLine(size.y/drawHelper.LineHeight + 0.1f);

            if (drawHelper.IsVisible(compareScrollBarPosition, compareScrollViewHeight, compareScrollViewY))
            {
                // Right radio button
                size.x = LEConstants.RadioButtonWidth;
                size.y = drawHelper.StandardHeight();

                float position;
                if (isCompareScrollBarVisible)
                    position = drawHelper.ScrollViewWidth()-size.x-drawHelper.LeftBuffer;
                else
                    position = drawHelper.FullWindowWidth()-size.x-drawHelper.LeftBuffer;

                chooseRight = EditorGUI.Toggle(new Rect(position, drawHelper.TopOfLine()+textFieldStyle.lineHeight/2f, size.x, size.y), currentChoice.Equals(DiffChoice.Right), radioButtonStyle);
            }

            if (chooseLeft && !currentChoice.Equals(DiffChoice.Left))
            {
                if (lockChoiceAcrossLangs)
                    LESyncHelper.ChooseLeftForAllLangs(key);
                else
                    LESyncHelper.ChooseLeft(lang, key);
            }
            else if (chooseRight && !currentChoice.Equals(DiffChoice.Right))
            {
                if (lockChoiceAcrossLangs)
                    LESyncHelper.ChooseRightForAllLangs(key);
                else
                    LESyncHelper.ChooseRight(lang, key);
            }
            
            // Right value
            content.text = LEStringTableEditor.GetPresentationFromCache(rightValue);
            sizeKey = key + LEConstants.SizeRightValueSuffix + (isCompareScrollBarVisible?"_0":"_1");

            if (!drawHelper.SizeCache.TryGetValue(sizeKey, out size))
            {
                if (isCompareScrollBarVisible)
                    size.x = drawHelper.ScrollViewWidth()-LEConstants.RadioButtonWidth*2-drawHelper.LeftBuffer-drawHelper.RightBuffer-4;
                else
                    size.x = drawHelper.FullWindowWidth()-LEConstants.RadioButtonWidth*2-drawHelper.LeftBuffer-drawHelper.RightBuffer-4;
                size.y = textFieldStyle.CalcHeight(content, size.x);
                drawHelper.SizeCache.Add(sizeKey, new Vector2(size.x, size.y));
            }

            if (drawHelper.IsVisible(compareScrollBarPosition, compareScrollViewHeight, compareScrollViewY))
                EditorGUI.LabelField(new Rect(drawHelper.CurrentLinePosition+LEConstants.RadioButtonWidth+2, drawHelper.TopOfLine(), size.x, size.y),
                                 content.text, textFieldStyle);

            drawHelper.NewLine(size.y/drawHelper.LineHeight);
            drawHelper.DrawSectionSeparator(isCompareScrollBarVisible);
            drawHelper.NewLine(0.25f);
        }

        bool DrawDiffEntryFoldout(string label)
        {
            content.text = label;

            sizeKey = label + LEConstants.SizeKeySuffix;
            if (!drawHelper.SizeCache.TryGetValue(sizeKey, out size))
            {
                size = EditorStyles.foldout.CalcSize(content);
                drawHelper.SizeCache.Add(sizeKey, new Vector2(size.x, size.y));
            }
            bool isOpen = EditorGUI.Foldout(new Rect(drawHelper.CurrentLinePosition, drawHelper.TopOfLine(), size.x, size.y), entryFoldoutState.Contains(label), content);
            drawHelper.NewLine();

            return isOpen;
        }

        Vector2 DrawClearChoicesButton()
        {
            content.text = LEConstants.ClearChoicesBtn;

            sizeKey = LEConstants.SizeClearChoicesKey;
            if (!drawHelper.SizeCache.TryGetValue(sizeKey, out size))
            {
                size = syncButtonStyle.CalcSize(content);
                drawHelper.SizeCache.Add(sizeKey, new Vector2(size.x, size.y));
            }

            if (GUI.Button(new Rect(drawHelper.CurrentLinePosition, drawHelper.TopOfLine(), size.x, drawHelper.StandardHeight()*buttonHeightMultiplier), content, syncButtonStyle))
                LESyncHelper.ClearChoices();

            drawHelper.CurrentLinePosition += size.x + 4f;

            return size;
        }

        Vector2 DrawAllLeftButton()
        {
            content.text = LEConstants.AllLeftBtn;

            sizeKey = LEConstants.SizeAllLeftKey;
            if (!drawHelper.SizeCache.TryGetValue(sizeKey, out size))
            {
                size = syncButtonStyle.CalcSize(content);
                drawHelper.SizeCache.Add(sizeKey, new Vector2(size.x, size.y));
            }

            if (GUI.Button(new Rect(drawHelper.CurrentLinePosition, drawHelper.TopOfLine(), size.x, drawHelper.StandardHeight()*buttonHeightMultiplier), content, syncButtonStyle))
                LESyncHelper.ChooseAllLeft();

            drawHelper.CurrentLinePosition += size.x + 4f;

            return size;
        }

        Vector2 DrawAllRightButton()
        {
            content.text = LEConstants.AllRightBtn;

            sizeKey = LEConstants.SizeAllRightKey;
            if (!drawHelper.SizeCache.TryGetValue(sizeKey, out size))
            {
                size = syncButtonStyle.CalcSize(content);
                drawHelper.SizeCache.Add(sizeKey, new Vector2(size.x, size.y));
            }

            if (GUI.Button(new Rect(drawHelper.CurrentLinePosition, drawHelper.TopOfLine(), size.x, drawHelper.StandardHeight()*buttonHeightMultiplier), content, syncButtonStyle))
                LESyncHelper.ChooseAllRight();

            drawHelper.CurrentLinePosition += size.x + 4f;

            return size;
        }

        Vector2 DrawSaveAndUploadButton()
        {
            content.text = LEConstants.SaveAndUploadBtn;
            drawHelper.TryGetCachedSize(LEConstants.SizeSaveAndUploadBtnKey, content, syncButtonStyle, out size);

            // Position from the left
            drawHelper.CurrentLinePosition = drawHelper.FullWindowWidth()-drawHelper.RightBuffer-size.x-4f;
            if (GUI.Button(new Rect(drawHelper.CurrentLinePosition, drawHelper.TopOfLine(), size.x, drawHelper.StandardHeight()*buttonHeightMultiplier), content, syncButtonStyle))
            {
                LESyncHelper.SaveAndExport();
            }
            drawHelper.CurrentLinePosition += size.x + 4f;

            return new Vector2(0, drawHelper.StandardHeight()*buttonHeightMultiplier);
        }

        void DrawRateBox(float left, float top, float width, float height)
        {
            GUI.Box(new Rect(left, top, 2f, height), string.Empty, rateBoxStyle);
            GUI.Box(new Rect(left, top, width, 2f), string.Empty, rateBoxStyle);
            GUI.Box(new Rect(left, top+height, width+2, 2f), string.Empty, rateBoxStyle);
            GUI.Box(new Rect(left+width, top, 2f, height), string.Empty, rateBoxStyle);
        }

        float CalculateGroupHeight()
        {
            if (groupHeights.Count == 0)
                return LESyncHelper.AllLangKeys.Count * drawHelper.LineHeight;

            float heightTotal = 0;
            foreach(var kvp in groupHeights)
                heightTotal += kvp.Value;

            return heightTotal;
        }

        void OnEnable()
        {
            LESyncHelper.OnSyncComplete += SyncComplete;
        }

        void OnDisable()
        {
            LESyncHelper.OnSyncComplete = null;
        }
        
        void SyncComplete()
        {
            Debug.Log(LEConstants.SyncCompleteLbl);
            Close();
        }
    }
}
