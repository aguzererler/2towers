using UnityEngine;
using UnityEditor;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace LocalizationEditor
{
    using LELangDict = System.Collections.Generic.Dictionary<string, string>;
    using LELangDictCollection = System.Collections.Generic.Dictionary<string, System.Collections.Generic.Dictionary<string, string>>;

    public class LEImportExcel : EditorWindow
    {
        public enum View
        {
            Default,
            ImportLocalFile,
            LaunchAuthURL,
            Authenticate,
            Download,
            ImportComplete
        }

        GUIStyle headerStyle = null;
        GUIStyle linkStyle = null;
        GUIStyle buttonStyle = null;
        GUIStyle labelStyle = null;
        GUIStyle comboBoxStyle = null;
        GUIStyle textFieldStyle = null;
        string spreadsheetPath = "";

        GUIContent _content;
        GUIContent content 
        {
            get
            {
                if (_content == null)
                    _content = new GUIContent();
                return _content;
            }
        }
        
        Vector2 size;

        string accessCode = "";
        int downloadSelectionIndex = 0;

        static View currentView;
        static View nextView;

        public static string googleSheetImportName;
        public static int googleSheetImportIndex;

        LEDrawHelper _drawHelper;
        LEDrawHelper drawHelper
        {
            get
            {
                if (_drawHelper == null)
                    _drawHelper = new LEDrawHelper(this, 2f, 10f, 10f, 10f, 20f);
                
                return _drawHelper;
            }
        }

        public delegate void ImportDoneAction();
        public static ImportDoneAction OnImportComplete;

        void OnDisable()
        {
            OnImportComplete = null;
        }

        void SetStyles()
        {
            if (headerStyle == null || string.IsNullOrEmpty(headerStyle.name))
            {
                headerStyle = new GUIStyle(GUI.skin.label);
                headerStyle.fontStyle = FontStyle.Bold;
                headerStyle.fontSize = 16;
            }
            
            if (buttonStyle == null || string.IsNullOrEmpty(buttonStyle.name))
            {
                buttonStyle = new GUIStyle(GUI.skin.button);
                buttonStyle.fontSize = 14;
                buttonStyle.padding = new RectOffset(15, 15, 5, 5);
            }
            
            if (labelStyle == null || string.IsNullOrEmpty(labelStyle.name))
            {
                labelStyle = new GUIStyle(GUI.skin.label);
                labelStyle.fontSize = 14;
                labelStyle.padding = new RectOffset(0, 0, 3, 3);
            }
            
            if (linkStyle == null || string.IsNullOrEmpty(linkStyle.name))
            {
                linkStyle = new GUIStyle(GUI.skin.label);
                linkStyle.fontSize = 16;
                linkStyle.alignment = TextAnchor.MiddleCenter;
                linkStyle.normal.textColor = Color.blue;
            }
            
            if (comboBoxStyle == null || string.IsNullOrEmpty(comboBoxStyle.name))
            {
                comboBoxStyle = new GUIStyle(EditorStyles.popup);
                comboBoxStyle.fontSize = 14;
                comboBoxStyle.fixedHeight = 24f;
            }
            
            if (textFieldStyle == null || string.IsNullOrEmpty(textFieldStyle.name))
            {
                textFieldStyle = new GUIStyle(GUI.skin.textField);
                textFieldStyle.fontSize = 14;
                textFieldStyle.fixedHeight = 22f;
                textFieldStyle.alignment = TextAnchor.UpperLeft;
                textFieldStyle.margin = new RectOffset(0, 0, 0, 8);
            }
        }

        public void LoadSettings(View view = View.Default)
        {
            LESettings settings = LESettings.Instance;
            googleSheetImportName = settings.ImportedGoogleSpreadsheetName;
            spreadsheetPath = settings.ImportedLocalSpreadsheetName;

            currentView = view;
            nextView = view;

            this.minSize = new Vector2(420, 250);
            this.maxSize = this.minSize;
        }

        void OnGUI()
        {
            SetStyles();

            size = Vector2.zero;
            
            drawHelper.ResetToTop();

            if (currentView.Equals(View.Default))
                DrawDefaultView();
            else if (currentView.Equals(View.LaunchAuthURL))
                DrawLaunchAuthURLView();
            else if (currentView.Equals(View.Authenticate))
                DrawAuthenticateView();
            else if (currentView.Equals(View.Download))
                DrawDownloadView();
            else if (currentView.Equals(View.ImportLocalFile))
                DrawImportLocalFileView();
            else if (currentView.Equals(View.ImportComplete))
                DrawImportCompleteView();

            currentView = nextView;
        }

        void DrawDefaultView()
        {
            content.text = LEConstants.ChooseImportExcelLbl;
            drawHelper.TryGetCachedSize(LEConstants.SizeChooseImportExcelLblKey, content, headerStyle, out size);
            EditorGUI.LabelField(new Rect(drawHelper.CenteredOnLine(size.x), drawHelper.TopOfLine(), size.x, size.y), content, headerStyle);
            
            drawHelper.NewLine(2);

            content.text = LEConstants.ImportLocalLbl;
            drawHelper.TryGetCachedSize(LEConstants.SizeImportLocalLblKey, content, buttonStyle, out size);
            if (GUI.Button(new Rect(drawHelper.CenteredOnLine(size.x), drawHelper.TopOfLine(), size.x, size.y), content, buttonStyle))
                nextView = View.ImportLocalFile;

            drawHelper.NewLine(2.5f);

            content.text = LEConstants.ImportGoogleSpreadsheetLbl;
            drawHelper.TryGetCachedSize(LEConstants.SizeImportGoogleSpreadsheetLblKey, content, buttonStyle, out size);
            if (GUI.Button(new Rect(drawHelper.CenteredOnLine(size.x), drawHelper.TopOfLine(), size.x, size.y), content, buttonStyle))
            {
                if (HasAuthenticated())
                {
                    nextView = View.Download;
                    GoogleDriveHelper.Instance.GetSpreadsheetList();

                    int index = Array.IndexOf(GoogleDriveHelper.Instance.SpreadSheetNames, googleSheetImportName);
                    if (GoogleDriveHelper.Instance.SpreadSheetNames.IsValidIndex(index))
                        downloadSelectionIndex = index;
                }
                else
                    nextView = View.LaunchAuthURL;
            }
                
            drawHelper.NewLine(2.5f);

            content.text = LEConstants.ReauthenticateGoogleLbl;
            drawHelper.TryGetCachedSize(LEConstants.SizeReauthenticateGoogleLblKey, content, buttonStyle, out size);
            if (GUI.Button(new Rect(drawHelper.CenteredOnLine(size.x), drawHelper.TopOfLine(), size.x, size.y), content, buttonStyle))
                nextView = View.LaunchAuthURL;
        }

        void DrawImportLocalFileView()
        {
            content.text = LEConstants.ImportExcelWBLbl;
            drawHelper.TryGetCachedSize(LEConstants.SizeImportExcelWBLblKey, content, headerStyle, out size);
            EditorGUI.LabelField(new Rect(drawHelper.CenteredOnLine(size.x), drawHelper.TopOfLine(), size.x, size.y), content, headerStyle);
            
            drawHelper.NewLine(2);

            content.text = LEConstants.ExcelFilePathLbl;
            drawHelper.TryGetCachedSize(LEConstants.SizeExcelFilePathLblKey, content, labelStyle, out size);
            EditorGUI.LabelField(new Rect(drawHelper.CurrentLinePosition, drawHelper.TopOfLine(), size.x, size.y), content, labelStyle);
            drawHelper.CurrentLinePosition += size.x + 2f;
            drawHelper.NewLine();

            spreadsheetPath = EditorGUI.TextField(new Rect(drawHelper.CurrentLinePosition, drawHelper.TopOfLine(), drawHelper.FullSeparatorWidth(), 
                                                           textFieldStyle.fixedHeight), spreadsheetPath, textFieldStyle);
            drawHelper.CurrentLinePosition += size.x + 2f;
            drawHelper.NewLine(1.1f);

            content.text = LEConstants.BrowseLbl;
            drawHelper.TryGetCachedSize(LEConstants.SizeBrowseLblKey, content, buttonStyle, out size);
            if (GUI.Button(new Rect(drawHelper.CurrentLinePosition, drawHelper.TopOfLine(), size.x, size.y), content, buttonStyle))
            {
                string newSpreadSheetPath = EditorUtility.OpenFilePanel(LEConstants.OpenWBLbl, spreadsheetPath, string.Empty);
                if (!string.IsNullOrEmpty(newSpreadSheetPath) && !newSpreadSheetPath.Equals(spreadsheetPath))
                    spreadsheetPath = newSpreadSheetPath;
                GUI.FocusControl(string.Empty);
            }

            // Draw Back & Import Buttons
            content.text = LEConstants.BackBtn;
            drawHelper.TryGetCachedSize(LEConstants.SizeBackBtnKey, content, buttonStyle, out size);
            if (GUI.Button(new Rect(drawHelper.CurrentLinePosition, position.height-size.y-drawHelper.BottomBuffer, size.x, size.y), content, buttonStyle))
                nextView = View.Default;
            
            content.text = LEConstants.ImportBtn;
            drawHelper.TryGetCachedSize(LEConstants.SizeImportBtnKey, content, buttonStyle, out size);
            if (GUI.Button(new Rect(position.width-size.x-drawHelper.LeftBuffer, position.height-size.y-drawHelper.BottomBuffer, size.x, size.y), content, buttonStyle))
            {
                LESettings settings = LESettings.Instance;

                settings.ImportedLocalSpreadsheetName = spreadsheetPath;
                settings.SyncLocalSpreadsheetName = spreadsheetPath;
                settings.LastSyncType = LESettings.SyncType.Local;

                settings.Save();
                
                // Clear any data already loaded
                LEStringTableEditor.ClearAll();
                
                // Remove all the language files from disk
                LEStringTableEditor.DeleteLangsFromDisk();
                
                // Process the sheet
                EPPExcelHelper excelHelper = new EPPExcelHelper(spreadsheetPath);
                
                LELangDictCollection allLangs = excelHelper.GetLanguagesFromWorkbook();
                LEStringTableEditor.AllLangsLogical = allLangs;
                
                // Write out lang.txt files
                LEStringTableEditor.Save();

                if (OnImportComplete != null)
                    OnImportComplete();
                
                nextView = View.ImportComplete;
            }
        }

        void DrawLaunchAuthURLView()
        {
            content.text = LEConstants.AuthWithGoogleLbl;
            drawHelper.TryGetCachedSize(LEConstants.SizeAuthWithGoogleLblKey, content, headerStyle, out size);
            EditorGUI.LabelField(new Rect(drawHelper.CenteredOnLine(size.x), drawHelper.TopOfLine(), size.x, size.y), content, headerStyle);
            
            drawHelper.NewLine(2);
            
            content.text = LEConstants.GoogleAuthInstruction1_1;
            drawHelper.TryGetCachedSize(LEConstants.SizeGoogleAuthInstruction1_1Key, content, labelStyle, out size);
            EditorGUI.LabelField(new Rect(drawHelper.CurrentLinePosition, drawHelper.TopOfLine(), size.x, size.y), content, labelStyle);
            
            drawHelper.NewLine();
            
            content.text = LEConstants.GoogleAuthInstruction1_2;
            drawHelper.TryGetCachedSize(LEConstants.SizeGoogleAuthInstruction1_2Key, content, labelStyle, out size);
            EditorGUI.LabelField(new Rect(drawHelper.CurrentLinePosition, drawHelper.TopOfLine(), size.x, size.y), content, labelStyle);
            
            drawHelper.NewLine(2);
            
            content.text = LEConstants.GoogleAuthInstruction2_1;
            drawHelper.TryGetCachedSize(LEConstants.SizeGoogleAuthInstruction2_1Key, content, labelStyle, out size);
            EditorGUI.LabelField(new Rect(drawHelper.CurrentLinePosition, drawHelper.TopOfLine(), size.x, size.y), content, labelStyle);
            
            drawHelper.NewLine();
            
            content.text = LEConstants.GoogleAuthInstruction2_2;
            drawHelper.TryGetCachedSize(LEConstants.SizeGoogleAuthInstruction2_2Key, content, labelStyle, out size);
            EditorGUI.LabelField(new Rect(drawHelper.CurrentLinePosition, drawHelper.TopOfLine(), size.x, size.y), content, labelStyle);
            
            
            // Draw Back & GOTO Auth buttons
            content.text = LEConstants.BackBtn;
            drawHelper.TryGetCachedSize(LEConstants.SizeBackBtnKey, content, buttonStyle, out size);
            if (GUI.Button(new Rect(drawHelper.CurrentLinePosition, position.height-size.y-drawHelper.BottomBuffer, size.x, size.y), content, buttonStyle))
                nextView = View.Default;
            
            content.text = LEConstants.GoToAuthURLBtn;
            drawHelper.TryGetCachedSize(LEConstants.SizeGoToAuthURLBtnKey, content, buttonStyle, out size);
            if (GUI.Button(new Rect(position.width-size.x-drawHelper.LeftBuffer, position.height-size.y-drawHelper.BottomBuffer, size.x, size.y), content, buttonStyle))
            {
                GoogleDriveHelper.Instance.RequestAuthFromUser();
                nextView = View.Authenticate;
            }
        }

        void DrawAuthenticateView()
        {
            content.text = LEConstants.AuthWithGoogleLbl;
            drawHelper.TryGetCachedSize(LEConstants.SizeAuthWithGoogleLblKey, content, headerStyle, out size);
            EditorGUI.LabelField(new Rect(drawHelper.CenteredOnLine(size.x), drawHelper.TopOfLine(), size.x, size.y), content, headerStyle);
            
            drawHelper.NewLine(2);
            
            content.text = LEConstants.EnterAccessCodeLbl;
            drawHelper.TryGetCachedSize(LEConstants.SizeEnterAccessCodeLblKey, content, labelStyle, out size);
            EditorGUI.LabelField(new Rect(drawHelper.CurrentLinePosition, drawHelper.TopOfLine(), size.x, size.y), content, labelStyle);
            
            drawHelper.NewLine(1.1f);
            
            accessCode = EditorGUI.TextField(new Rect(drawHelper.CurrentLinePosition, drawHelper.TopOfLine(), drawHelper.FullSeparatorWidth(), size.y), accessCode, textFieldStyle);
            
            // Draw Back & Set Code Buttons
            content.text = LEConstants.BackBtn;
            drawHelper.TryGetCachedSize(LEConstants.SizeBackBtnKey, content, buttonStyle, out size);
            if (GUI.Button(new Rect(drawHelper.CurrentLinePosition, position.height-size.y-drawHelper.BottomBuffer, size.x, size.y), content, buttonStyle))
                nextView = View.LaunchAuthURL;
            
            content.text = LEConstants.SetCodeBtn;
            drawHelper.TryGetCachedSize(LEConstants.SizeSetCodeBtnKey, content, buttonStyle, out size);
            if (!string.IsNullOrEmpty(accessCode) && GUI.Button(new Rect(position.width-size.x-drawHelper.LeftBuffer, position.height-size.y-drawHelper.BottomBuffer, size.x, size.y), content, buttonStyle))
            {
                GoogleDriveHelper.Instance.SetAccessCode(accessCode);
                GoogleDriveHelper.Instance.GetSpreadsheetList();
                nextView = View.Download;
                
                GUI.FocusControl(string.Empty);
            }
        }

        void DrawDownloadView()
        {
            content.text = LEConstants.DownloadSheetLbl;
            drawHelper.TryGetCachedSize(LEConstants.SizeDownloadSheetLblKey, content, headerStyle, out size);
            EditorGUI.LabelField(new Rect(drawHelper.CenteredOnLine(size.x), drawHelper.TopOfLine(), size.x, size.y), content, headerStyle);
            
            drawHelper.NewLine(2);

            content.text = LEConstants.SelectSpreadSheetLbl;
            drawHelper.TryGetCachedSize(LEConstants.SizeSelectSpreadSheetLblKey, content, labelStyle, out size);
            EditorGUI.LabelField(new Rect(drawHelper.CurrentLinePosition, drawHelper.TopOfLine(), size.x, size.y), content, labelStyle);
            
            drawHelper.NewLine(1.1f);

            downloadSelectionIndex = EditorGUI.Popup(new Rect(drawHelper.CurrentLinePosition, drawHelper.TopOfLine(), drawHelper.FullSeparatorWidth(), size.y),
                                                     downloadSelectionIndex, GoogleDriveHelper.Instance.SpreadSheetNames, comboBoxStyle);

            // Draw Back & Upload Buttons
            content.text = LEConstants.BackBtn;
            drawHelper.TryGetCachedSize(LEConstants.SizeBackBtnKey, content, buttonStyle, out size);
            if (GUI.Button(new Rect(drawHelper.CurrentLinePosition, position.height-size.y-drawHelper.BottomBuffer, size.x, size.y), content, buttonStyle))
                nextView = View.Default;
            
            content.text = LEConstants.DownloadBtn;
            drawHelper.TryGetCachedSize(LEConstants.SizeDownloadBtnKey, content, buttonStyle, out size);
            if (GUI.Button(new Rect(position.width-size.x-drawHelper.LeftBuffer, position.height-size.y-drawHelper.BottomBuffer, size.x, size.y), content, buttonStyle))
            {
                googleSheetImportIndex = downloadSelectionIndex;
                googleSheetImportName = GoogleDriveHelper.Instance.SpreadSheetNames[downloadSelectionIndex];
                
                LESettings settings = LESettings.Instance;

                settings.ImportedGoogleSpreadsheetName = googleSheetImportName;
                settings.SyncGoogleSpreadsheetName = googleSheetImportName;
                settings.LastSyncType = LESettings.SyncType.Google;

                settings.Save();

                GoogleDriveHelper driveHelper = GoogleDriveHelper.Instance;
                spreadsheetPath = driveHelper.DownloadSpreadSheet(driveHelper.SpreadSheetNames[downloadSelectionIndex],  
                                                                  "import_" + googleSheetImportName + ".xlsx");
                
                // Clear any data already loaded into the GDEItemManager
                LEStringTableEditor.ClearAll();
                
                // Remove all the language files from disk
                LEStringTableEditor.DeleteLangsFromDisk();
                
                EPPExcelHelper excelHelper = new EPPExcelHelper(spreadsheetPath);
                LELangDictCollection allLangs = excelHelper.GetLanguagesFromWorkbook();
                LEStringTableEditor.AllLangsLogical = allLangs;
                
                // Write out lang.txt files
                LEStringTableEditor.Save();

                if (OnImportComplete != null)
                    OnImportComplete();
                
                nextView = View.ImportComplete;
            }
        }

        void DrawImportCompleteView()
        {
            content.text = LEConstants.ImportComplete;
            drawHelper.TryGetCachedSize(LEConstants.SizeImportCompleteKey, content, headerStyle, out size);
            EditorGUI.LabelField(new Rect(drawHelper.CenteredOnLine(size.x), drawHelper.TopOfLine(), size.x, size.y), content, headerStyle);
            
            drawHelper.NewLine(2);
            
            content.text = LEConstants.ImportMsg1;
            drawHelper.TryGetCachedSize(LEConstants.SizeImportMsg1, content, labelStyle, out size);
            EditorGUI.LabelField(new Rect(drawHelper.CenteredOnLine(size.x), drawHelper.TopOfLine(), size.x, size.y), content, labelStyle);
            
            drawHelper.NewLine(1.1f);
            
            content.text = LEConstants.ImportMsg2;
            drawHelper.TryGetCachedSize(LEConstants.SizeImportMsg2, content, labelStyle, out size);
            EditorGUI.LabelField(new Rect(drawHelper.CenteredOnLine(size.x), drawHelper.TopOfLine(), size.x, size.y), content, labelStyle);
            
            // Draw rate box
            float heightOfBox = 50f;
            float topOfBox = this.position.height * .5f + 5f;
            float bottomOfBox = topOfBox + heightOfBox;
            
            content.text = LEConstants.ForumLinkText;
            drawHelper.TryGetCachedSize(LEConstants.SizeForumLinkTextKey, content, linkStyle, out size);
            
            float widthOfBox = size.x + 10f;
            float leftOfBox = (this.position.width - widthOfBox) / 2f;
            
            if (GUI.Button(new Rect(leftOfBox + 6f, bottomOfBox - size.y - 2f, size.x, size.y), content, linkStyle))
            {
                Application.OpenURL(LEConstants.ForumURL);
            }

            content.text = LEConstants.RateMeText;
            if (GUI.Button(new Rect(leftOfBox + 6f, topOfBox + 3f, size.x, size.y), content, linkStyle))
            {
                Application.OpenURL(LEConstants.RateMeURL);
            }
            
            // Draw Import Again & Close Buttons
            content.text = LEConstants.ImportAgainBtn;
            drawHelper.TryGetCachedSize(LEConstants.SizeImportAgainBtnKey, content, buttonStyle, out size);
            if (GUI.Button(new Rect(drawHelper.CurrentLinePosition, position.height-size.y-drawHelper.BottomBuffer, size.x, size.y), content, buttonStyle))
                nextView = View.Default;
            
            content.text = LEConstants.CloseBtn;
            drawHelper.TryGetCachedSize(LEConstants.SizeCloseBtnKey, content, buttonStyle, out size);
            if (GUI.Button(new Rect(position.width-size.x-drawHelper.LeftBuffer, position.height-size.y-drawHelper.BottomBuffer, size.x, size.y), content, buttonStyle))
            {
                Close();
            }
        }

        bool HasAuthenticated()
        {
            return GoogleDriveHelper.Instance.HasAuthenticated();
        }
    }
}
